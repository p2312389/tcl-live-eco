# TCL live eco+ (groupe 7)


## Généralités

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://forge.univ-lyon1.fr/p2312389/tcl-live-eco.git
git branch -M main
git push -uf origin main
```

---

## Gestion des branches

### Pourquoi l'utilisation de branche ?

Une branche permet de travailler sur une fonctionnalité sans impacter directement le main. Une fois la branche publié sur le remote toute personne présente dans le projet peut regarder les modifications effectuée et les valider.

### Quand utiliser une branche ?

Lors de modification importante ou lorsque l'on est pas sur de soi.
Inutile de s'embêter avec les branches si j'effectue une petite modification.

### La création d'une branche

```
git pull
git checkout -b <branch> main
```

- `-b` : permet de créer et de se déplacer sur la nouvelle branche en une commande
- `<branch>` : je remplace bien évidemment cette section par le nom de ma branche
- `main` : dans le cadre de se projet la branche principale est la branche main je veux donc créer une nouvelle branche à partir de la branche principale. Cette option n'est pas obligatoire mais il est préférable de tout le temps l'utiliser afin de ne pas créer de branche à partir d'autres branches. On ne veut pas un arbre...

### La vie d'une branche
Le premier commit doit être correctement nommé car c'est généralement lui qui servire de nom de commit lors du merge de la branche (noramlement possible de le changer)
Je peux effectuer autant de commit que veux.

---

## Backend

### Récupérération des dernières modifications du remote

```
git pull
cd backend
composer install
```

la commande `composer install` va installer toutes le dépendances présentes dans le fichier `composer.json`

### Créer la BDD
- Lancer le serveur mysql (xampp, wamp, mamp, ...)
- Dupliquer le fichier .env à la ravine du dossier 'backend' et le renommer en .env.local
- Commenter la ligne `DATABASE_URL="postgresql://app:!ChangeMe!@127.0.0.1:5432/app?serverVersion=15&charset=utf8"` avec un '#'
- Décommenter la ligne `# DATABASE_URL="mysql://<user-bdd>:<mdp-bdd>!@127.0.0.1:3306/<nom-bdd>?serverVersion=10.11.2-MariaDB&charset=utf8mb4"`
	- Modifier `<user-bdd>` : nom d'utilisateur pour se connecter à la BDD
	- Modifier `<mdp-bdd>` : mdp pour se connecter à la BDD
	- Modifier `<nom-bdd>` : nom de la BDD
	La partie `<?serverVersion=10.11.2-MariaDB&charset=utf8mb4>` n'est pas obligatoire elle peut être retirée.

Lancer la suite de commande suivante :

```
cd backend
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

- 'php bin/console doctrine:database:create' : permet de créer la BDD sur le serveur mysql renseigné
- 'php bin/console doctrine:migrations:migrate' : permet de créer les tables

#### En cas d'erreur
En cas d'erreur `could not find driver`

- Se rendre dans le dossier de PHP installer sur le sytème
- Chercher le fichier `php.ini`
	- Si ce dernier n'existe pas copier le fichier `php.ini-development` et renommer le en `php.ini`
- Ouvrir le fichier `php.ini`
- Rechercher `extension=pdo_mysql` dans le fichier 
- Décommenter l'extension en enlevant le ';' devant
- Réessayer la commande qui n'a pas fonctionné

### Peupler la BDD

#### Dans le projet
- Ajouter les fichiers avec les données dans un dossier nommé `filesForDatabase`
- les fichiers avec les transport doivent être renommé de cette façon : transports-`<nom-transport>`.csv Ex : transports-bus.csv
- Le fichier avec les stations doit être nommé `stop-area.csv`

#### Depuis l'interface admin
- Lancer le serveur [symfony](#symfony)
- Lancer le serveur [react](#frontend)
- Se rendre sur le site à l'url : `http://localhost:3000/admin`
- Appuyer sur le bouton `Récupérer les fichiers de mises à jour`
- Appuyer sur le premier bouton `Mettre à jour la base avec les fichiers` **EN PREMIER**
- Une fois le traitement terminé appuyer sur le deuxieme bouton `Mettre à jour la base avec les fichiers`
- Pour voir l'avancée, ouvrir l'inspecteur web > Network
- Il y a environ 650 requêtes à effectuer pour que la BDD soit mise à jour


### Symfony

#### Lancer le serveur
```
cd backend
symfony serve
```

Se rendre sur l'URL : `http://127.0.0.1:8000/api/`
- Suivit de la route pour accéder au point d'entrée voulue
- Si aucune route n'est renseigné l'URL précédente affiche la doc de l'API

---

## Frontend

Le frontend est développé avec [React](https://reactjs.org/).

### Commandes utiles :

- `npm install` :\
	Installe les dépendances nécessaires au bon fonctionnement de l'application.
- `npm start` :\
	Lance l'application en mode développement sur le [port 3000](http://localhost:3000).\
	La page se recharge à chaque modifications et les erreurs sont affichées dans la console.
- `npm test` :\
	Lance les tests.\
	Regardez la section sur [l'exécution de tests](https://facebook.github.io/create-react-app/docs/running-tests) pour plus d'informations.
- `npm run build` :\
	Construit l'application pour la production dans le dossier `build`.\
	Regardez la section sur [le déploiement](https://facebook.github.io/create-react-app/docs/deployment) pour plus d'informations.