import '../../styles/Common/Details.css';

import Header from '../Common/Header';
import StopList from '../Common/StopList';

import accessible from '../../assets/accessible.svg';
import inaccessible from '../../assets/inaccessible.svg';
import { useLocation } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import BannerError from '../Common/BannerError';
import { getTransportWithId } from '../../services/TransportsServices';
import { Transport } from '../../models/Transport';
import ScheduleTable from '../Common/ScheduleTable';
import { getDisturbance } from '../../services/ApiGrandLyonService';
import { Disturbance } from '../../models/Disturbance';
import DisturbanceCard from '../Common/DisturbanceCard';
import TransportImage from '../Common/TransportImage';

/**
 * Represents the main component for displaying transport details.
 */
function Main() {

    const location = useLocation()
    const {transportId} = location.state

    const [errorMessage, setErrorMessage] = useState<string>('');
    const [transport, setTransport] = useState<Transport>()

    const [routeToDisplay, setRouteToDisplay] = useState<React.JSX.Element[]>([])
    const [disturbanceForCurrentTransport, setDisturbanceForCurrentTransport] = useState<Disturbance[]>([])
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {
        getTransport();
    }, [])

    function getTransport() {
        setLoading(true);
        getTransportWithId(transportId)
            .then(response => {
                if (response.status === 200) {
                    setTransport(response.data)
                    const transport: Transport = response.data
                    setRouteToDisplay(transport.routes.map(route => (
                        <div>
                            <h1>{route.complete_name}</h1>
                            <StopList
                                listRouteDetail={route.route_detail}
                                transportId={transport.id}
                                routeId={route.id}
                            />
                            <ScheduleTable
                                handleOnClick={onClickHandler}
                                routeDetailList={route.route_detail}
                            />
                        </div>
                    )))

                    getDisturbanceForTransport(transport.code_line);
                } else {
                    setErrorMessage(response.data.message);
                    setLoading(false);
                }
            })
    }

    /**
     * Retrieves the disturbance information for a given transport.
     * It uses the Grand Lyon API.
     * 
     * @param codeLine The code line to process.
     */
    function getDisturbanceForTransport(codeLine: string) {
        getDisturbance()
            .then(response => {
                setLoading(false);
                if (response.status === 200) {
                    const disturbances: Disturbance[] = response.data
                    setDisturbanceForCurrentTransport(disturbances.filter(disturbance => disturbance.code_line === codeLine))
                } else {
                    setErrorMessage(response.data.message)
                }
            })
    }

    /**
     * Refreshes the page.
     */
    function onClickHandler() {
        getTransport();
    }

    /**
     * Maps the disturbance data for the current transport to a list of JSX elements.
     * 
     * @param disturbanceForCurrentTransport - The disturbance data for the current transport.
     * @returns A list of JSX elements representing the disturbances.
     */
    const disturbanceListToShow = disturbanceForCurrentTransport.map((disturbance, index) => {
        return (
            <div key={index}>
                <DisturbanceCard disturbance={disturbance}/>
            </div>
        );
    });

    return (
        <>
            <Header/>
            {loading ? <h1>Chargement</h1> :
                <main className="transport-details">
                    {errorMessage !== '' &&
                        <BannerError message={errorMessage}></BannerError>}
                    <section>
                        { transport !== undefined &&
                            <TransportImage lineName={transport?.line_name}/>}
                        <img src={transport?.wheelchair_accessible ? accessible : inaccessible}
                             alt={transport?.wheelchair_accessible ? 'Accessible' : 'Inaccessible'}/>
                    </section>
                    <h1>Perturbations</h1>
                    {disturbanceForCurrentTransport.length > 0 ? disturbanceListToShow : 'Aucune perturbation sur cette ligne'}

                    <div>
                        {routeToDisplay}
                    </div>
                </main>
            }
        </>
    );
}

export default Main;