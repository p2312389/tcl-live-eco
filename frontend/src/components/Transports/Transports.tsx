import Header from '../Common/Header';
import Form from './Form';
import '../../styles/AllTransports/Main.css';
import { getTransportListWithParams } from '../../services/TransportsServices';
import React, { useEffect, useState } from 'react';
import { Transport } from '../../models/Transport';
import Card from '../Transports/Card';
import BannerError from '../Common/BannerError';


/**
 * Renders the Transports component.
 */
function Transports() {

    const [errorMessage, setErrorMessage] = useState<string>('');
    const [listCardTypeMetro, setListTransportTypeMetro] = useState<React.JSX.Element[]>([])
    const [listCardTypeBus, setListTransportTypeBus] = useState<React.JSX.Element[]>([])
    const [listCardTypeTramway, setListTransportTypeTramway] = useState<React.JSX.Element[]>([])

    useEffect(() => {
        getTransports('', []);
    }, [])

    /**
     * Retrieves transports based on the transport name and ID types.
     * @param transportName - The name of the transport.
     * @param idTypeTransports - An array of ID types for the transports.
     */
    function getTransports(transportName: string, idTypeTransports: number[]) {
        setErrorMessage('');

        getTransportListWithParams(transportName, idTypeTransports)
            .then(response => {
                if (response.status === 200) {
                    const listTransport: Transport[] = response.data

                    if (listTransport.length > 0) {
                        const listTransportTypeMetro: Transport[] = [];
                        const listTransportTypeBus: Transport[] = [];
                        const listTransportTypeTramway: Transport[] = [];

                        listTransport.forEach(transport => {
                            if (transport.type_transport.label === 'Metro') {
                                listTransportTypeMetro.push(transport);
                            }
                            if (transport.type_transport.label === 'Bus') {
                                listTransportTypeBus.push(transport);
                            }
                            if (transport.type_transport.label === 'Tram') {
                                listTransportTypeTramway.push(transport);
                            }
                        });

                        setListTransportTypeMetro(listTransportTypeMetro.map(transport =>
                            <Card key={transport.id}
                                  {...transport}
                            ></Card>
                        ));

                        setListTransportTypeBus(listTransportTypeBus.map(transport =>
                            <Card key={transport.id}
                                  {...transport}
                            ></Card>
                        ));

                        setListTransportTypeTramway(listTransportTypeTramway.map(transport =>
                            <Card key={transport.id}
                                  {...transport}
                            ></Card>
                        ));
                    }
                    else {
                        setErrorMessage("Aucun transport trouvé");
                    }
                } else {
                    setErrorMessage(response.data.message);
                }
            })
    }

    return (
        <>
            <Header/>
            <Form handleCallback={getTransports}/>
            <main className="transports">

                {errorMessage !== '' &&
                    <BannerError message={errorMessage}></BannerError>}

                <section>
                    {listCardTypeMetro.length > 0 &&
                        <div>
                            <summary>
                                <h3>Métros</h3>
                            </summary>
                            <div>
                                {listCardTypeMetro}
                            </div>
                        </div>
                    }
                    {listCardTypeTramway.length > 0 &&
                        <div>
                            <summary>
                                <h3>Tramways</h3>
                            </summary>
                            <div>
                                {listCardTypeTramway}
                            </div>
                        </div>
                    }
                    {listCardTypeBus.length > 0 &&
                        <div>
                            <summary>
                                <h3>Bus</h3>
                            </summary>
                            <div>
                                {listCardTypeBus}
                            </div>
                        </div>
                    }
                </section>
            </main>
        </>
    );
}

export default Transports;