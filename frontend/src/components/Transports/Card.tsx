import '../../styles/Common/Card.css';

import {Link} from 'react-router-dom';

import accessible from '../../assets/accessible.svg';
import { Transport } from '../../models/Transport';
import TransportImage from '../Common/TransportImage';


/**
 * Renders a card component for a transport.
 * @param transport - The transport object to be displayed.
 */
function Card(transport: Transport) {

	return (
		<Link
			to="/transport-details"
			state={{
				transportId: transport.id
			}}
			className='card'>
			<article>
				<div>
					<TransportImage lineName={ transport.line_name }/>
					{ transport.wheelchair_accessible && <img src={accessible} alt="accessible handicapé" /> }
				</div>
			</article>
		</Link>
	);
}

export default Card;