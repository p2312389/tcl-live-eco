import '../../styles/Common/Form.css';

import SummaryTransportType from '../Common/SummaryTransportType';
import { autocompleteTransportSearch } from '../../services/TransportsServices';
import React, { useState } from 'react';
import Autocomplete from '../Common/Autocomplete';
import search from "../../assets/search.svg";

/**
 * Props for the Form component.
 */
interface FormProps {
	handleCallback: (transportName: string, idTypeTransport: number[]) => void;
}

/**
 * Renders a form component.
 *
 * @param prop - The props for the form component.
 * @returns The rendered form component.
 */
function Form(prop: FormProps) {
	const [transportsNameAutoComplete, setTransportsNameAutoComplete] = useState([]);
	const [listIdTypeTransport, setListIdTypeTransport] = useState<number[]>([]);
	const [transportName, setTransportName] = useState<string>('');

	function handleChange(event: React.FormEvent<HTMLInputElement>) {
		transportsNameAutoComplete.length = 0;
		const keyword: string = event.currentTarget.value;
		if (keyword !== '') {
			autocompleteTransportSearch(keyword)
				.then(response => {
					if (response.status === 200) {
						setTransportsNameAutoComplete(response.data);
					}
				});
		} else {
			transportsNameAutoComplete.length = 0;
		}
	}

	function handleClick() {
		prop.handleCallback(transportName, listIdTypeTransport);
	}

	function handleTypeTransport(listIdTypeTransport: number[]) {
		setListIdTypeTransport(listIdTypeTransport);
	}

	function handleTransportNameInput(transportName: string) {
		setTransportName(transportName);
	}

	return (
		<>
			<nav className='form'>
				<Autocomplete
						handleInputContent={ handleTransportNameInput }
						handleChange= {handleChange}
						elementList= {transportsNameAutoComplete.slice(0, 10)}
						placeholder= 'Nom du transport'
						labelContent= ''
						elementName= 'line_name'
				></Autocomplete>
				<SummaryTransportType handleTypeTransport={handleTypeTransport}/>
				<button className='search-button' onClick={handleClick}>
					<img src={search} alt="Rechercher"/>Rechercher
				</button>
			</nav>
		</>
	);
}

export default Form;