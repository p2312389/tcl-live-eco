import '../styles/var.css';

import React from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import HomePage from './HomePage/HomePage';
import Stops from './Stops/Stops';
import StopDetails from './Stops/Details';
import Transports from './Transports/Transports';
import TransportDetails from './Transports/Details';
import AdminPage from './Admin/AdminPage';
import Velov from './velov/Velov';
import DisturbancePage from './Disturbance/DisturbancePage';


/**
 * Renders the main application component.
 */
function App() {
	return (
		<Router>
			<Routes>
				<Route path="/" element={<HomePage />} />
				<Route path="/stops" element={<Stops />} />
				<Route path="/stop-details" element={<StopDetails />} />
				<Route path="/transports" element={<Transports />} />
				<Route path="/transport-details" element={<TransportDetails />} />
				<Route path="/disturbances" element={<DisturbancePage />} />
				<Route path={ '/stations-velov' } element={ <Velov /> } />
				<Route path="/admin" element={<AdminPage />} />
				<Route path="*" element={<h1>404</h1>} />
			</Routes>
		</Router>
	);
}

export default App;