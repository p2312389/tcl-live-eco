import {MapContainer, Marker, Popup, TileLayer, useMapEvents} from 'react-leaflet'
import 'leaflet/dist/leaflet.css';
import '../../styles/Home/Home.css';
import {LatLngExpression} from "leaflet";
import markerIconURL from '../../assets/station-icon.svg';
import L from 'leaflet';
import {useEffect, useState} from "react";
import { getStationsWithAddress } from '../../services/StationsServices';

/**
 * Represents an address.
 */
interface IAddress {
    street: string;
    municipality: string,
    zip_code: string,
    latitude: number,
    longitude: number
}

/**
 * Represents a station.
 */
interface IStation {
    id: number,
    name: string,
    wheelchair_accessible: boolean,
    lift: boolean,
    escalator: boolean,
    address: IAddress,
    id_stop_area: string
}

/**
 * Renders the location marker component.
 */
function LocationMarker() {
    const [position, setPosition] = useState<L.LatLng>()
    const map = useMapEvents({
        dragend() {
            setPosition(map.getCenter());
        }
        /*
        click() {
            setPosition(map.getCenter());
            console.log(position);
            if (position !== undefined) {
                map.flyTo([position.lat, position.lng], map.getZoom());
            }
        }
         */
    })

    return position === null ? null : (
        <div></div>
    )
}

/**
 * Renders the Map component.
 */
function Map() {
    const [stations, setStations] = useState<IStation[]>([])

    useEffect(() => {
            getStationsWithAddress()
                .then(response => {
                    if (response.status === 200) {
                        const stationsWithAddress = response.data.filter((station: IStation) => {
                            return station.address.longitude !== undefined && !station.id_stop_area.includes('S')
                        })
                        setStations(stationsWithAddress);
                    }
                })
    }, []);


    const position: LatLngExpression | undefined = [45.75959779224713, 4.837873641304261]
    const markerIcon = new L.Icon({
        iconUrl: markerIconURL,
        iconSize: [25, 35],
        iconAnchor: [5, 30]
    });

    return(
        <div className="map-container">
            <MapContainer center={position} zoom={13} scrollWheelZoom={false}>
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {stations.length > 0 &&
                    stations.map((station: IStation) => (
                            <Marker
                                key={station.id}
                                position={[
                                    station.address.latitude,
                                    station.address.longitude
                                ]}
                                icon={markerIcon}
                            >
                                <Popup>{station.name}</Popup>
                            </Marker>
                    ))}
                <LocationMarker />
            </MapContainer>
        </div>
    )

    /* return (
        <>
            <MapContainer center={[45.75959779224713, 4.837873641304261]} zoom={13} scrollWheelZoom={false}>
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={[45.75959779224713, 4.837873641304261]}>
                    <Popup>
                        A pretty CSS3 popup. <br /> Easily customizable.
                    </Popup>
                </Marker>
            </MapContainer>
        </>
    ); */
}

export default Map