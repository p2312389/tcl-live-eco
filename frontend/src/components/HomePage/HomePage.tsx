import Header from "../Common/Header";
import Map from "./Map";

/**
 * Renders the home page component.
 */
function HomePage() {
	return (
		<>
			<Header />
			<h1>HomePage</h1>
			<Map></Map>
		</>
	);
}

export default HomePage