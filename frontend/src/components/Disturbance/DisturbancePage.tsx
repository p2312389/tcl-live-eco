import { getDisturbance } from '../../services/ApiGrandLyonService';
import { useEffect, useState } from 'react';
import { Disturbance } from '../../models/Disturbance';
import DisturbanceCard from '../Common/DisturbanceCard';
import Header from '../Common/Header';
import BannerError from '../Common/BannerError';

/**
 * Renders the DisturbancePage component.
 */
function DisturbancePage() {

    const [disturbances, setDisturbances] = useState<Disturbance[]>([]);
    const [errorMessage, setErrorMessage] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {
        getAllDisturbance();
    }, [])

    /**
     * Retrieves all the disturbances.
     */
    function getAllDisturbance() {
        setLoading(true);
        getDisturbance()
            .then(response => {
                if (response.status === 200) {
                    setDisturbances(response.data);
                    setLoading(false);
                } else {
                    setErrorMessage(response.data.message);
                    setLoading(false);
                }
            })
    }

    /**
     * The disturbances to show.
     */
    const disturbanceToShow = disturbances.map((disturbance, index) => {
        return (
            <div key={index}>
                <DisturbanceCard disturbance={disturbance}/>
            </div>
        );
    });

    return (
        <>
            <Header/>
            <main>
                { loading ? <h1>Chargement</h1> :
                    <div>
                        {errorMessage !== '' &&
                            <BannerError message={errorMessage}></BannerError>}
                        <h1>Perturbations</h1>
                        { disturbanceToShow }
                    </div>
                }
            </main>
        </>
    );
}
export default DisturbancePage;