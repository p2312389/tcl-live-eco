import { VelovStation } from '../../models/VelovStation';
import '../../styles/velov/VelovCard.css';

/**
 * Renders a VelovCard component.
 * @param velovStation - The VelovStation object containing information about the Velov station.
 */
function VelovCard(velovStation: VelovStation) {
    console.log(velovStation)
    return (
        <div className={'velov-card'}>
            <article>
                <div className={'title-element'}>
                    <span className={'title-card'}>{velovStation.nom}</span>
                    <div
                        className={`icon-availability background-${velovStation.availability === undefined ? '0' : velovStation.availability.availability_code}`}></div>
                </div>
                <div>
                    <span>{velovStation.address.street}, {velovStation.address.municipality}</span>
                </div>
                {velovStation.availability !== undefined &&
                    <div>
                        <span>Vélo disponible : {velovStation.availability.available_bike.bikes_available} / {velovStation.availability.available_bike.capacity}</span>
                        <span>Vélo méchanique : {velovStation.availability.available_bike.mechanical_bikes}</span>
                        <span>Vélo électrique : {velovStation.availability.available_bike.electrical_bikes}</span>
                    </div>
                }
            </article>
        </div>
    );
}

export default VelovCard;