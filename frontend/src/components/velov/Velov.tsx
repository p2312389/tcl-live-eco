import Header from '../Common/Header';
import Form from '../Stops/Form';
import { useState } from 'react';
import '../../styles/AroundYou/Main.css';
import BannerError from '../Common/BannerError';
import VelovStationMap from './VelovStationMap';
import { VelovStationsSearch } from '../../models/VelovStationsSearch';
import { getVelovStationNearPostion } from '../../services/VelovStationService';
import { VelovStation } from '../../models/VelovStation';

/**
 * Renders the Velov component.
 */
function Velov() {

    const [velovStationAround, setVelovStationAround] = useState<VelovStationsSearch>(new VelovStationsSearch([], '', ''));
    const [errorMessage, setErrorMessage] = useState<string>('');

    /**
     * Retrieves the Velov station based on the provided parameters.
     * 
     * @param street - The street name of the station.
     * @param municipality - The municipality of the station.
     * @param zipcode - The zipcode of the station.
     * @param idTypeTransport - An array of transportation type IDs.
     */
    function getVelovStation(street: string, municipality: string, zipcode: string, idTypeTransport: number[]) {
        setErrorMessage('');

        if (street !== '' && zipcode !== '' && municipality !== '') {

            getVelovStationNearPostion(
                street,
                municipality,
                zipcode,
                500
            )
                .then(response => {
                    if (response.status === 200) {
                        const velovStationAroundTmp = new VelovStationsSearch(response.data.velov_stations, response.data.latitude, response.data.longitude);

                        velovStationAroundTmp.velov_stations.filter(velovStation => velovStation.ouverte);

                        setVelovStationAround(velovStationAroundTmp);

                        const velovStations: VelovStation[] = velovStationAroundTmp.velov_stations;
                        if (velovStations.length <= 0) {
                            setErrorMessage('Aucune station vélo\'v trouvée pour l\'adresse : ' + street + ', ' + zipcode + ' ' + municipality);
                        }
                    } else {
                        setErrorMessage(response.data.message);
                    }
                })
        } else {
            setErrorMessage('Veuillez saisir l\'adresse complète pour effectuer la recherche.');
        }
    }

    return (
        <>
            <Header/>
            <Form handleCallback={ getVelovStation } showTypeTransportFilter={false} />
            <main className="stops">
                <h2>Autour de vous</h2>
                {errorMessage !== '' &&
                    <BannerError message={errorMessage}></BannerError>}
                <div>
                    <VelovStationMap velovStationsSearch={ velovStationAround }></VelovStationMap>
                </div>
            </main>
        </>
    );
}

export default Velov;