import L, { LatLngExpression } from 'leaflet';
import 'leaflet/dist/leaflet.css';
import '../../styles/AroundYou/StopsMap.css';
import markerIconURL from '../../assets/velov-icon.svg';
import { MapContainer, Marker, Popup, TileLayer, useMapEvents } from 'react-leaflet';
import React, { useEffect, useState } from 'react';
import marker from '../../assets/marker-location.svg';
import { VelovStationsSearch } from '../../models/VelovStationsSearch';
import VelovCard from './VelovCard';

/**
 * Props for the VelovStationMap component.
 */
interface VelovStationMapProp {
    velovStationsSearch: VelovStationsSearch;
}

/**
 * Default position for the VelovStationMap component.
 * @type {LatLngExpression}
 */
const defaultPosition: LatLngExpression = [45.75959779224713, 4.837873641304261]

/**
 * Renders a location marker on the map.
 */
function LocationMarker() {
    const [position, setPosition] = useState<L.LatLng>()
    const map = useMapEvents({
        dragend() {
            setPosition(map.getCenter());
        }
    })

    return position === null ? null : (
        <div></div>
    )
}

/**
 * Sets the view of the map to the specified position and zoom level.
 * 
 * @param map - The Leaflet map instance.
 * @param position - The position to set the view to.
 * @param zoom - The zoom level to set the view to.
 */
function setView(map: L.Map, position: LatLngExpression, zoom: number) {
    map.flyTo(position, zoom);
}

/**
 * Renders a map displaying Velov stations.
 *
 * @param props - The component props.
 */
function VelovStationMap(props: VelovStationMapProp) {

    const [map, setMap] = useState<L.Map | null>(null)

    const [position, setPosition] = useState<LatLngExpression>(defaultPosition);

    useEffect(() => {

        if (props.velovStationsSearch.latitude !== '' || props.velovStationsSearch.longitude !== '') {
            setPosition([Number(props.velovStationsSearch.latitude), Number(props.velovStationsSearch.longitude)]);
            if (map !== null) {
                // Je ne peux pas utiliser le useState juste après un set, donc j'utilise les valeurs
                setView(map, [Number(props.velovStationsSearch.latitude), Number(props.velovStationsSearch.longitude)], 16);
            }
        }
        else {
            setPosition(defaultPosition);
            if (map !== null) {
                setView(map, defaultPosition, 13)
            }
        }
    }, [map, props.velovStationsSearch])

    // const position: LatLngExpression | undefined = [45.75959779224713, 4.837873641304261]
    const markerIcon = new L.Icon({
        iconUrl: markerIconURL,
        iconSize: [35, 45],
        iconAnchor: [5, 30]
    });

    const stopAreaMarler = props.velovStationsSearch.velov_stations.map((velovStation) => (
        <Marker
            key={velovStation.id}
            position={[
                Number(velovStation.address.latitude),
                Number(velovStation.address.longitude)
            ]}
            icon={markerIcon}
        >
            <Popup >
                <div
                    key={velovStation.id}>
                    <VelovCard
                        id={ velovStation.id}
                        id_station={velovStation.id_station}
                        nom={velovStation.nom}
                        nb_bornettes={velovStation.nb_bornettes}
                        ouverte={velovStation.ouverte}
                        address={velovStation.address}
                        availability={velovStation.availability} />
                </div>
            </Popup>
        </Marker>
    ));

    function handleClickDefault() {
        if (map !== null) {
            setView(map, defaultPosition, 13)
        }
    }

    function handleClickAddress() {
        if (map !== null) {
            // Je ne peux pas utiliser le useState juste après un set, donc j'utilise les valeurs
            setView(map, position, 16);
        }
    }

    return(
        <div className="map-component">
            <div className="position-button">
                <button onClick={ handleClickDefault } className='search-button'>
                    <img src={marker} alt="Centrer la carte par défaut" />Centrer par défaut
                </button>
                <button disabled={props.velovStationsSearch.velov_stations.length === 0} onClick={ handleClickAddress } className='search-button'>
                    <img src={marker} alt="Centrer la carte sur l'adresse recherchée" />Centrer sur l'adresse recherchée
                </button>
            </div>
            <div className="map-container">
                <MapContainer center={position} zoom={13} scrollWheelZoom={false} ref={setMap}>
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    {props.velovStationsSearch.velov_stations.length > 0 &&
                        stopAreaMarler}
                    <LocationMarker />
                </MapContainer>
            </div>
        </div>
    )
}

export default VelovStationMap;