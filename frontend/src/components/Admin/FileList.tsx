import { DataFile } from '../../models/DataFile';
import { useState } from 'react';
import {
    uploadAllSchedules,
    uploadAllXmlData,
    uploadBus,
    uploadMetro,
    uploadStopAreaV2,
    uploadTramway, uploadTypeTransport
} from '../../services/databaseHandlerServices';
import '../../styles/Admin/admin.css';

/**
 * Props for the FileList component.
 */
interface FileListeProps {
    csvFiles: DataFile[],
    isXML: boolean
}

/**
 * Renders a list of files.
 * @param fileListeProps - The props for the FileList component.
 */
function FileList (fileListeProps: FileListeProps) {
    const [isSuccess, setIsSuccess] = useState(false);
    const [isError, setIsError] = useState(false);
    const [messageError, setMessageError] = useState('');
    const [isFetching, setIsFetching] = useState(false);
    const [requestCounter, setRequesCounter] = useState(0)
    const [maxRequest, setMaxRequest] = useState(0);
    const [fileNameWithError] = useState([''])

    const csvFiles: DataFile[] = fileListeProps.csvFiles;
    const isXML: boolean = fileListeProps.isXML;

    // React transforme un tableau en tableau associatif...
    // Comme je veux le parcourir j'ai besoin d'un tableau et non pas d'un ****** de tableau associatif
    const listCsvFiles: DataFile[] = [];
    for (const items in csvFiles){
        listCsvFiles.push(csvFiles[items]);
    }

    const numberOfFile = listCsvFiles.length;

    /**
     * Uploads all files.
     */
    const uploadAllFile = async () => {

        setIsFetching(true);
        setIsError(false);
        setIsSuccess(false);

        const res: Promise<any>[] = [];

        for (const file of listCsvFiles) {
            if (file.fileExtension === 'csv') {
                setMaxRequest(listCsvFiles.length)
                setRequesCounter(requestCounter + 1)

                const response = await uploadCurrentCsvFile(file.fileName)
                res.push(response);
            } else {
                // 4 requete par fichier
                setMaxRequest(listCsvFiles.length * 4)

                setRequesCounter(requestCounter + 1);
                const response = await uploadAllXmlData(file.fileName)
                res.push(response);
            }
        }

        let error = '';

        Promise.all(res)
            .then((responses) => {
                responses.forEach((response) => {
                    if (response.status !== 201) {
                        setIsError(true);
                        error += response.message;
                    }
                })
            })

        if (isError) {
            setMessageError(error)
        } else {
            setIsSuccess(true);
        }
        setIsFetching(false);

        setMaxRequest(0)
    }

    /**
     * Uploads the current CSV file with the specified file name.
     * 
     * @param fileName - The name of the file to be uploaded.
     * @returns A Promise that resolves to the current CSV file.
     */
    const uploadCurrentCsvFile = async (fileName: string) => {
        setIsFetching(true);
        let response;

        response = await uploadTypeTransport()

        if (response.status === 201) {

            if (fileName.includes('transports')) {
                if (fileName.includes('bus')) {
                    response = await uploadBus(1, 1000);
                }
                if (fileName.includes('metro')) {
                    response = await uploadMetro(1, 1000);
                }
                if (fileName.includes('tramway')) {
                    response = await uploadTramway(1, 1000);
                }
            } else {
                response = await uploadStopAreaV2(1, 1000);
            }
        } else {
            setIsError(true);
            setMessageError(response.message);
            fileNameWithError.push(fileName);
        }

        if (response.status !== 201) {
            setIsError(true);
            setMessageError(response.message)
        } else {
            setIsSuccess(true);
        }

        setIsFetching(false);

        return response;
    }

    /**
     * Retrieves the current XML file with the specified file name.
     * 
     * @param fileName - The name of the XML file to retrieve.
     * @returns A Promise that resolves to the current XML file.
     */
    const currentXmlFile = async (fileName: string) => {
        setIsFetching(true);

        const response = await uploadAllXmlData(fileName);

        if (response.status !== 201) {
            setIsError(true);
            setMessageError(response.message)
        } else {
            setIsSuccess(true);
        }

        setIsFetching(false);

        return response;
    }

    /**
     * Represents a counter request.
     */
    function CounterRequest() {
        if (maxRequest > 0) {
            return (
                <div className={'count-request'}>Nombre de requête éxécuté : {requestCounter} / {maxRequest}</div>
            );
        }
        return <div>Nombre de fichier : {numberOfFile}</div>;
    }

    /**
     * Represents a component for displaying message information.
     */
    function MessageInformation() {
        if (isError) {
            return (
                <div className={'alert alert-danger'}>
                    {messageError}
                </div>
            );
        }
        if (isSuccess) {
            return (
                <div className={'alert alert-success'}>
                    Tous s'est bien déroulé
                </div>
            );
        }
        return <div></div>;
    }

    /**
     * Uploads schedules asynchronously.
     */
    const uploadSchedules = async () => {
        // uploadAllSchedules
        setIsFetching(true);
        setIsError(false);
        setIsSuccess(false);

        const res: Promise<any>[] = [];

        for (const file of listCsvFiles) {
            // Je veux pas upload les fichier qui ont eu une erreur
            if (!fileNameWithError.includes(file.fileName) && file.fileExtension === "xml") {
                const response = await uploadAllSchedules(1, 100, file.fileName)
                res.push(response);
            }
        }

        let error = '';

        Promise.all(res)
            .then((responses) => {
                responses.forEach((response) => {
                    if (response.status !== 201) {
                        setIsError(true);
                        error += response.message;
                    }
                })
            })

        if (isError) {
            setMessageError(error)
        } else {
            setIsSuccess(true);
        }
        setIsFetching(false);

        setMaxRequest(0)
    }

    /**
     * Maps the list of CSV files to JSX elements.
     *
     * @param {Array<string>} listCsvFiles - The list of CSV files.
     * @returns {Array<JSX.Element>} - The mapped JSX elements.
     */
    const listFile = listCsvFiles.map(file =>

        <div key={file.id}>
            <div className={`xml-card alert ${isSuccess ? 'alert-success' : 'alert-default'}`}>
                <div className={'xml-file-name'}>
                    {file.fileName}
                </div>
                <div className={'xml-loader-button'}>
                    <button disabled={isFetching} className={'button-admin'} onClick={() => {
                        if (file.fileExtension === 'csv') {
                            uploadCurrentCsvFile(file.fileName);
                        } else {
                            currentXmlFile(file.fileName);
                        }
                    }}>Mettre à jour</button>
                </div>
                <div className={'xml-loader-message'}>
                    {file.errorMessage !== '' ? file.errorMessage : file.message}
                </div>
            </div>
        </div>
    );

    return (
        <div>
            <MessageInformation />
            { listCsvFiles.length > 0 ? <button disabled={isFetching} className={'button-all-csv button-admin'} onClick={uploadAllFile}>Mettre à jour la base avec les fichiers</button> : <div></div>}
            { listCsvFiles.length > 0 && isXML ? <button disabled={isFetching} className={'button-all-csv button-admin'} onClick={uploadSchedules}>Mettre à jour les horaires</button> : <div></div>}
            <CounterRequest />
            <div>{listFile}</div>
        </div>
    );
}

export default FileList
