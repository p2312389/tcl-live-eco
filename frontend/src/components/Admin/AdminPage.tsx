import Header from '../Common/Header';
import { useCallback, useState } from 'react';
import { getFileForDatabase } from '../../services/databaseHandlerServices'
import FileList from './FileList';
import { DataFile } from '../../models/DataFile';
import '../../styles/Admin/admin.css';

/**
 * Renders the Admin page component.
 */
function Adminpage() {
    const [listOfFile, setListOfFile] = useState([{
        id: 0,
        fileName: '',
        fileExtension: '',
        completeFileName: '',
        className: '',
        errorMessage: '',
        message: '',
        status: 0
    }]);

    const [error, setError] = useState({
        status: 0,
        message: ''
    });
    const [isSending, setIsSending] = useState(false);

    const sendRequest = useCallback(async () => {
        // don't send again while we are sending
        if (isSending) return
        // update state
        setIsSending(true)
        // send the actual request
        await getFileForDatabase()
            .then(response => {
                setListOfFile(response);
                setError(response)
            })
        // once the request is sent, update state again
        setIsSending(false)
    }, [isSending]) // update the callback if the state changes

    const listCsvFiles: Array<DataFile> = []
    listOfFile.forEach(file => {
        if (file.fileExtension === 'csv') {
            listCsvFiles.push(file);
        }
    })

    const listXmlFiles: Array<DataFile> = []
    listOfFile.forEach(file => {
        if (file.fileExtension === 'xml') {
            listXmlFiles.push(file);
        }
    })

    /**
     * Represents a component for displaying message information.
     */
    function MessageInformation() {
        if (error.status > 200) {
            return (
                <div className={'alert alert-danger'}>
                    {error.message}
                </div>
            );
        }
        return <div></div>;
    }

    return (
        <div>
            <Header></Header>
            <MessageInformation></MessageInformation>
            <button className={'button-fetch-file button-admin'} disabled={isSending} onClick={sendRequest}>Récupérer les fichiers de mises à jour</button>
            <FileList
                csvFiles={listCsvFiles}
                isXML={false}
            />
            <FileList
                csvFiles={listXmlFiles}
                isXML={true}
            />
        </div>
    );
}

export default Adminpage