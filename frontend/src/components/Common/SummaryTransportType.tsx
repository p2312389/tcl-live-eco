import '../../styles/Common/SummaryTransportType.css'
import React, { useEffect, useState } from 'react';
import { TypeTransport } from '../../models/TypeTransport';
import { getTypeTransports } from '../../services/TypeTransportServices';

/**
 * Props for the SummaryTransportType component.
 */
interface SummaryTransportTypeProps {
	handleTypeTransport: (listIdTypeTransport: number[]) => void;
}

/**
 * Renders the summary of the transport type.
 *
 * @param prop - The props for the SummaryTransportType component.
 */
function SummaryTransportType(prop: SummaryTransportTypeProps) {
	const [typeTransports, setTypeTransports] = useState<TypeTransport[]>([]);
	const [listIdTypeTransport] = useState<number[]>([]);
	const getTypeTransport = () => {
		getTypeTransports()
			.then(response => {
				if (response.status === 200) {
					setTypeTransports(response.data);
				}
			})
	}

	/**
	 * Handles the change event of the input element.
	 * 
	 * @param event - The change event.
	 */
	function handleOnChange(event: React.ChangeEvent<HTMLInputElement>) {
		const idFromAttribute = event.target.getAttribute('data-key');
		if (idFromAttribute != null) {
			const idTypeTransport: number = parseInt(idFromAttribute);
			if (event.target.checked) {
				listIdTypeTransport.push(idTypeTransport);
			} else {
				const indexOfCurrentId = listIdTypeTransport.indexOf(idTypeTransport);
				listIdTypeTransport.splice(indexOfCurrentId, 1);
			}
		}
		prop.handleTypeTransport(listIdTypeTransport);
	}

	/**
	 * useEffect hook for handling side effects.
	 */
	useEffect(() => {
		getTypeTransport();
	}, []);

	/**
	 * Renders a list of transport types.
	 *
	 * @param typeTransports - An array of transport types.
	 * @returns The rendered list of transport types.
	 */
	const listTypeTransport = typeTransports.map(typeTransport =>
		<div key={typeTransport.id}>
			<input onChange={handleOnChange} data-key={typeTransport.id} id={typeTransport.label} type="checkbox" />
			<label htmlFor={typeTransport.label}>{typeTransport.label}</label>
		</div>
	);

	return (
		<details className='summary-transport-type'>
			<summary>Type de transport</summary>
			<div className="list-transport-type" >
				{ listTypeTransport }
			</div>
		</details>
	);
}

export default SummaryTransportType;