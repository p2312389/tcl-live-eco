import { Disturbance } from '../../models/Disturbance';
import '../../styles/Common/DisturbanceCard.css';

/**
 * Represents the props for the DisturbanceCard component.
 */
interface DisturbanceCardProp {
    disturbance: Disturbance
}

/**
 * Renders a disturbance card component.
 *
 * @param prop - The props for the disturbance card.
 * @returns The rendered disturbance card component.
 */
function DisturbanceCard(prop: DisturbanceCardProp) {
    return (
        <div className="disturbance-card">
            <article>
                <div>
                    <span>{prop.disturbance.titre}</span>
                </div>
                <div>
                    <span>Date de début : {prop.disturbance.debut}</span>
                </div>
                <div>
                    <span>Date de fin : {prop.disturbance.fin}</span>
                </div>
                <div>
                    <span>{prop.disturbance.message}</span>
                </div>
            </article>
        </div>
    );
}

export default DisturbanceCard;