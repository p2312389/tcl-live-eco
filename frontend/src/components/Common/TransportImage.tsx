/**
 * Represents the props for the TransportImage component.
 */
interface TransportImageProp {
    lineName: string
}

/**
 * Renders an image for a transport.
 *
 * @param prop - The props for the TransportImage component.
 * @returns The rendered TransportImage component.
 */
function TransportImage(prop: TransportImageProp) {
    const transportImagePath = () => {
        return require(`../../assets/lines/${ prop.lineName }.svg`)
    }

    return <img src={ transportImagePath() } alt={ `logo du transport ${ prop.lineName }` } />
}

export default TransportImage;