import '../../styles/Common/Button.css'

import search from '../../assets/search.svg';
import React from "react";

/**
 * Props for the SearchButton component.
 */
interface SearchButtonProps {
	handleClick: React.MouseEventHandler<HTMLImageElement>
}

/**
 * Renders a search button component.
 *
 * @param prop - The props for the SearchButton component.
 * @returns The rendered SearchButton component.
 */
function SearchButton(prop: SearchButtonProps) {
	return (
		<button className='search-button'>
			<img src={search} alt="Rechercher" onClick={prop.handleClick} />Rechercher
		</button>
	);
}

export default SearchButton;