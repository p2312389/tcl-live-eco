import React from 'react';
import RemainingTime from './RemainingTime';
import { RouteDetail } from '../../models/RouteDetail';

/**
 * Props for the ScheduleTable component.
 */
interface ScheduleTableProps {
    handleOnClick: () => void;
    routeDetailList: RouteDetail[];
}

/**
 * Renders a schedule table component.
 *
 * @param prop - The props for the ScheduleTable component.
 * @returns The rendered ScheduleTable component.
 */
function ScheduleTable(prop: ScheduleTableProps) {

    /**
     * Handles the click event.
     */
    function handleOnClick() {
        prop.handleOnClick();
    }

    /**
     * Generates a schedule list for all stop areas based on the given route detail list.
     *
     * @param prop - The prop containing the route detail list.
     * @returns The schedule list for all stop areas.
     */
    const scheduleListForAllStopArea = prop.routeDetailList.map(routeDetail => {
        /**
         * Maps the list of next schedules to the current route detail.
         *
         * @param schedule - The schedule object.
         * @param index - The index of the schedule in the list.
         * @returns The mapped schedule list for the current route detail.
         */
        const scheduleListCurrentRouteDetail = routeDetail.stop_area.list_next_schedules?.map((schedule, index) => {
            return (
                <div key={index}>
                    <RemainingTime time={schedule}/>
                </div>
            );
        })

        return (
            <tr key={routeDetail.id}>
                <td>{routeDetail.stop_area.name}</td>
                <td>
                    {scheduleListCurrentRouteDetail !== undefined ?
                        scheduleListCurrentRouteDetail :
                        'Aucune information'}
                </td>
            </tr>
        );
    });

    return (
        <div className="schedule">
            <button className="search-button" onClick={handleOnClick}>Rafraichir les prochains passages
            </button>
            <table>
                <thead>
                <tr>
                    <th colSpan={2}>Horaires des stations</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>Nom station</th>
                    <th>Prochain(s) passage(s)</th>
                </tr>
                {scheduleListForAllStopArea}
                </tbody>
            </table>
        </div>
    );
}

export default ScheduleTable;