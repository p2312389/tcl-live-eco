import '../../styles/Common/StopList.css';
import { RouteDetail } from '../../models/RouteDetail';
import { Link } from 'react-router-dom';

/**
 * Props for the StopList component.
 */
interface StopListProps {
    transportId: number;
    routeId: number;
    listRouteDetail: RouteDetail[] | undefined;
}

/**
 * Renders a list of stops.
 *
 * @param prop - The props for the StopList component.
 */
function StopList(prop: StopListProps) {
    return (
        <section className="stop-list">
            {prop.listRouteDetail?.map((routeDetail) => (
                <div key={routeDetail.id}>
                    <Link
                        to="/stop-details"
                        state={{
                            stopAreaId: routeDetail.stop_area.id,
                            transportId: prop.transportId,
                            routeId: prop.routeId
                        }}
                        className={'link-stop-area'}>
                        <div key={routeDetail.id}>
                            <p>{routeDetail.stop_area.name}</p>
                        </div>
                    </Link>
                </div>
            ))}
        </section>
    );
};

export default StopList;