import '../../styles/Common/RemainingTime.css'

/**
 * Props for the RemainingTime component.
 */
interface RemainingTimeProps {
	time: string | undefined
}

/**
 * Renders the remaining time based on the provided props.
 *
 * @param prop - The props containing the remaining time information.
 */
function RemainingTime(prop: RemainingTimeProps) {
	return (
		<>
			<div className='remaining-time'>{ prop.time !== undefined ? prop.time : 'aucun passage'}</div>
		</>
	);
}

export default RemainingTime;