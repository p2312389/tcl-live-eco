import '../../styles/Common/Autocomplete.css';

import React, { useEffect, useState } from 'react';


/**
 * Props for the Autocomplete component.
 */
interface AutocompleteProps {
    handleChange: React.ChangeEventHandler<HTMLInputElement>;
    handleInputContent: (inputContent: string) => void;
    elementList: any[];
    placeholder: string;
    labelContent: string;
    elementName: string
}

/**
 * Renders an Autocomplete component.
 *
 * @param autocompleteProps - The props for the Autocomplete component.
 * @returns The rendered Autocomplete component.
 */
function Autocomplete (autocompleteProps: AutocompleteProps) {
    const [display, setDisplay] = useState(false);
    const [search, setSearch] = useState("");

    const elementName = autocompleteProps.elementName;

    /**
     * useEffect hook for performing side effects in the Autocomplete component.
     */
    useEffect(() => {
        autocompleteProps.elementList.length > 0 ? setDisplay(true) : setDisplay(false);

    }, [autocompleteProps.elementList]);

    /**
     * Selects the input based on the provided line name.
     * 
     * @param lineName - The name of the line.
     */
    const selectInput = (lineName: string) => {
        setSearch(lineName);
        setDisplay(false);
        autocompleteProps.handleInputContent(lineName);
    }

    /**
     * Handles the change event of the input element.
     * 
     * @param event - The change event.
     */
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(event.target.value);
        autocompleteProps.handleChange(event);
    }

    // Fonction à retravailler car elle rajoute des caractères en plus.
    // { addStrongToKeyword(element[elementName]) }
    /**
     * Adds a strong tag to the provided element.
     * 
     * @param element - The element to add a strong tag to.
     */
    const addStrongToKeyword = (element: string) => {

        const startIndexOfSearch = element.toUpperCase().indexOf(search.toUpperCase());
        const lengthOfSearch = search.length;
        const endIndexOfSearch = startIndexOfSearch + lengthOfSearch;

        const textBeforeStrong = element.slice(0, startIndexOfSearch);
        const textAfterStrong = element.slice(endIndexOfSearch);

        console.log(textBeforeStrong)
        console.log(search)
        console.log(textAfterStrong)
        return (
            <span>
                {textBeforeStrong}<strong>{search.toUpperCase()}</strong>{textAfterStrong}
            </span>
        );
    }

    /**
     * Handles the click event.
     */
    const handleClick = () => {
        setSearch('');
        setDisplay(false);
    }

    /**
     * Sends the input content.
     */
    function sendInputContent() {
        autocompleteProps.elementList.length = 0;
        setDisplay(false);
        autocompleteProps.handleInputContent(search);
    }

    /**
     * Handles the onFocus event.
     */
    function handleOnFocus() {
        autocompleteProps.elementList.length = 0;
        setDisplay(false);
    }

    return (
        <div className="auto-container">
            <div className="search-box flex-container flex-column pos-rel">
                {autocompleteProps.labelContent !== '' &&
                    <label htmlFor="input-box">{autocompleteProps.labelContent}</label>}
                <div className="row">
                    <input
                        onFocus={ handleOnFocus }
                        onBlur={ sendInputContent }
                        onChange={ handleChange }
                        className="input-autocomplete"
                        type="text"
                        id="input-box"
                        value={search}
                        placeholder={autocompleteProps.placeholder}
                    />
                    <button className="delete-search" onClick={ handleClick }><i className="fa fa-close"></i></button>
                </div>
                {display && (
                    <div className="result-box" hidden={!display}>
                        {autocompleteProps.elementList.map(element => {
                            return (
                                <div
                                    onClick={() => selectInput(element[elementName])}
                                    className="proposal-item"
                                    key={element["id"]}
                                    tabIndex={0}
                                >
                                    { element[elementName] }
                                </div>
                            );
                        })}
                    </div>
                )}
            </div>
        </div>

    );
}

export default Autocomplete