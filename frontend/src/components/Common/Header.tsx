import '../../styles/Common/Header.css';

import { Link } from 'react-router-dom';


/**
 * Renders the header component.
 */
function Header() {
	return (
		<header>
			<h1>TCL Live Eco+</h1>
			<nav>
				<Link to="/" title="Accueil">Accueil</Link>
				<Link to="/stops" title="Chercher les arrêts autour de vous">Arrêts</Link>
				<Link to="/transports" title="Chercher un transport">Transports</Link>
				<Link to={'/disturbances'} title={'Perturbations'}>Perturbations</Link>
				<Link to={ '/stations-velov' } title={ 'Velov' }>Velo'v</Link>
			</nav>
		</header>
	);
}

export default Header;