import '../../styles/Common/BannerError.css';

/**
 * Props for the BannerError component.
 */
interface BannerErrorProps {
    message: string
}

/**
 * Renders a banner error component.
 *
 * @param prop - The props for the BannerError component.
 * @returns The rendered BannerError component.
 */
function BannerError(prop: BannerErrorProps) {
    return (
        <div className={'alert alert-danger'}>
            {prop.message}
        </div>
    );
}

export default BannerError;