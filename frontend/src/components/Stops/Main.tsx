import '../../styles/AroundYou/Main.css';

/**
 * Represents the props for the Main component.
 */
interface MainProps {
	stopAreaTransportRoutes: React.JSX.Element[]
}

/**
 * Renders the main component.
 * @param prop - The props for the Main component.
 */
function Main(prop: MainProps) {
	return (
		<main className='stops'>
			<h2>Autour de vous</h2>
			<section>
				{ prop.stopAreaTransportRoutes.length > 0 ? prop.stopAreaTransportRoutes : 'Aucun résultat' }
			</section>
		</main>
	);
}

export default Main;