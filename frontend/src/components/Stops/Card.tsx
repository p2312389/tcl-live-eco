import '../../styles/Common/Card.css';

import {Link} from 'react-router-dom';

import RemainingTime from '../Common/RemainingTime';

import accessible from '../../assets/accessible.svg';
import {StopAreaTransportRoute} from "../../models/StopAreaTransportRoute";
import TransportImage from '../Common/TransportImage';

/**
 * Renders a card component for a stop area transport route.
 * @param stopAreaTransportRoute - The stop area transport route object.
 */
function Card(stopAreaTransportRoute: StopAreaTransportRoute) {
	return (
		<Link
			to="/stop-details"
			state={{
				stopAreaId: stopAreaTransportRoute.stop_area.id,
				transportId: stopAreaTransportRoute.transport.id,
				routeId: stopAreaTransportRoute.route.id
			}}
			className='card'>
			<article>
				<div>
					<TransportImage lineName={ stopAreaTransportRoute.transport.line_name } />
					<span>{stopAreaTransportRoute.stop_area.name}</span>
				</div>
				<div>
					<span>Direction : {stopAreaTransportRoute.route.complete_name}</span>
				</div>
				<div>
					<RemainingTime time={'5'}/>
					{stopAreaTransportRoute.stop_area.wheelchair_accessible && <img src={accessible} alt="accessible handicapé" />}
				</div>
			</article>
		</Link>
	);
}

export default Card;