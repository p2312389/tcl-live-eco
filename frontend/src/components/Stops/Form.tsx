import '../../styles/Common/Form.css';
import '../../styles/Common/Button.css'
import SummaryTransportType from '../Common/SummaryTransportType';
import React, { useState } from "react";
import search from "../../assets/search.svg";

/**
 * Props for the Form component.
 */
interface FormProps {
    handleCallback: (street: string, municipality: string, zipcode: string, idTypeTransport: number[]) => void;
    showTypeTransportFilter: boolean;
}

/**
 * Renders a form component.
 *
 * @param prop - The props for the form component.
 * @returns The rendered form component.
 */
function Form(prop: FormProps) {

    const [inputAddress, setInputAddress] = useState('');
    const [inputZipcode, setInputZipcode] = useState('');
    const [inputCity, setInputCity] = useState('');

    const [listIdTypeTransport, setListIdTypeTransport] = useState<number[]>([]);

    /**
     * Handles the click event.
     */
    function handleClick() {
        prop.handleCallback(inputAddress, inputCity, inputZipcode, listIdTypeTransport);
    }

    /**
     * Handles the type of transport.
     * 
     * @param listIdTypeTransport - An array of numbers representing the list of type transport.
     */
    function handleTypeTransport(listIdTypeTransport: number[]) {
        setListIdTypeTransport(listIdTypeTransport);
    }

    return (
        <nav className='form'>
            <div>
                <input
                    onChange={event => setInputAddress(event.target.value)}
                    id="address"
                    type="text"
                    placeholder="Numéro et nom de rue"/>
                <input
                    onChange={event => setInputZipcode(event.target.value)}
                    id="zipcode"
                    type="text"
                    placeholder="Code postal"/>
                <input
                    onChange={event => setInputCity(event.target.value)}
                    id="city" type="text"
                    placeholder="Ville"/>
            </div>
            { prop.showTypeTransportFilter &&
                <SummaryTransportType handleTypeTransport={handleTypeTransport}/>
            }
            <button className='search-button' onClick={handleClick}>
                <img src={search} alt="Rechercher"/>Rechercher
            </button>
        </nav>
    );
}

export default Form;