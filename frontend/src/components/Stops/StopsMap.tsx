import L, { LatLngExpression } from 'leaflet';
import 'leaflet/dist/leaflet.css';
import '../../styles/AroundYou/StopsMap.css';
import markerIconURL from '../../assets/station-icon.svg';
import { MapContainer, Marker, Popup, TileLayer, useMapEvents } from 'react-leaflet';
import React, { useEffect, useState } from 'react';
import { StopAreaAround } from '../../models/StopAreaAround';
import Card from './Card';
import marker from '../../assets/marker-location.svg';

/**
 * Represents the props for the StopsMap component.
 */
interface StopsMapProp {
    stopAreaAround: StopAreaAround;
}

const defaultPosition: LatLngExpression = [45.75959779224713, 4.837873641304261]

/**
 * Renders a location marker on the map.
 */
function LocationMarker() {
    const [position, setPosition] = useState<L.LatLng>()
    const map = useMapEvents({
        dragend() {
            setPosition(map.getCenter());
        }
    })

    return position === null ? null : (
        <div></div>
    )
}

/**
 * Sets the view of the map to the specified position and zoom level.
 * 
 * @param map - The Leaflet map instance.
 * @param position - The position to set the view to.
 * @param zoom - The zoom level to set the view to.
 */
function setView(map: L.Map, position: LatLngExpression, zoom: number) {
    map.flyTo(position, zoom);
}

/**
 * Renders a map of stops.
 *
 * @param props - The component props.
 * @returns The rendered StopsMap component.
 */
function StopsMap(props: StopsMapProp) {

    const [map, setMap] = useState<L.Map | null>(null)

    const [position, setPosition] = useState<LatLngExpression>(defaultPosition);

    useEffect(() => {

        if (props.stopAreaAround.latitude !== '' || props.stopAreaAround.longitude !== '') {
            setPosition([Number(props.stopAreaAround.latitude), Number(props.stopAreaAround.longitude)]);
            if (map !== null) {
                // Je ne peux pas utiliser le useState juste après un set, donc j'utilise les valeurs
                setView(map, [Number(props.stopAreaAround.latitude), Number(props.stopAreaAround.longitude)], 16);
            }
        }
        else {
            setPosition(defaultPosition);
            if (map !== null) {
                setView(map, defaultPosition, 13)
            }
        }
    }, [map, props.stopAreaAround])

    // const position: LatLngExpression | undefined = [45.75959779224713, 4.837873641304261]
    const markerIcon = new L.Icon({
        iconUrl: markerIconURL,
        iconSize: [25, 35],
        iconAnchor: [5, 30]
    });

    /**
     * Renders the stop area markers on the map.
     * 
     * @param {StopAreaTransportRoute[]} stopAreaTransportRoutes - The stop area transport routes to render.
     * @returns {JSX.Element[]} - The rendered stop area markers.
     */
    const stopAreaMarler = props.stopAreaAround.stop_area_transport_routes.map((stopAreaTransportRoute) => (
        <Marker
            key={stopAreaTransportRoute.stop_area.id + stopAreaTransportRoute.route.id + stopAreaTransportRoute.transport.id}
            position={[
                Number(stopAreaTransportRoute.stop_area.address.latitude),
                Number(stopAreaTransportRoute.stop_area.address.longitude)
            ]}
            icon={markerIcon}
        >
            <Popup>
                <div
                    key={stopAreaTransportRoute.route.id + '-' + stopAreaTransportRoute.transport.id + '-' + stopAreaTransportRoute.stop_area.id}>
                    <Card
                        {...stopAreaTransportRoute}
                    ></Card>
                </div>
            </Popup>
        </Marker>
    ));

    /**
     * Handles the default click event.
     */
    function handleClickDefault() {
        if (map !== null) {
            setView(map, defaultPosition, 13)
        }
    }

    /**
     * Handles the click event for the address.
     */
    function handleClickAddress() {
        if (map !== null) {
            // Je ne peux pas utiliser le useState juste après un set, donc j'utilise les valeurs
            setView(map, position, 16);
        }
    }

    return(
        <div className="map-component">
            <div className="position-button">
                <button onClick={ handleClickDefault } className='search-button'>
                    <img src={marker} alt="Centrer la carte par défaut" />Centrer par défaut
                </button>
                <button disabled={props.stopAreaAround.stop_area_transport_routes.length === 0} onClick={ handleClickAddress } className='search-button'>
                    <img src={marker} alt="Centrer la carte sur l'adresse recherchée" />Centrer sur l'adresse recherchée
                </button>
            </div>
            <div className="map-container">
                <MapContainer center={position} zoom={13} scrollWheelZoom={false} ref={setMap}>
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    {props.stopAreaAround.stop_area_transport_routes.length > 0 &&
                        stopAreaMarler}
                    <LocationMarker />
                </MapContainer>
            </div>
        </div>
    )
}

export default StopsMap;