import '../../styles/Common/Details.css';

import Header from '../Common/Header';
import RemainingTime from '../Common/RemainingTime';
import StopList from '../Common/StopList';

import accessible from '../../assets/accessible.svg';
import inaccessible from '../../assets/inaccessible.svg';
import elevator from '../../assets/elevator.svg';
import escalator from '../../assets/escalator.svg';
import { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { getStopAreaDetail } from '../../services/StopAreaServices';
import { StopAreaTransportRoute } from '../../models/StopAreaTransportRoute';
import { RouteDetail } from '../../models/RouteDetail';
import ScheduleTable from '../Common/ScheduleTable';
import { getDisturbance } from '../../services/ApiGrandLyonService';
import BannerError from '../Common/BannerError';
import { Disturbance } from '../../models/Disturbance';
import DisturbanceCard from '../Common/DisturbanceCard';
import TransportImage from '../Common/TransportImage';

/**
 * Renders the details component.
 */
function Details() {

    const location = useLocation()
    const {stopAreaId, transportId, routeId} = location.state

    const [stopAreaTransportRoute, setStopAreaTransportRoute] = useState<StopAreaTransportRoute>();

    const [scheduleListForCurrentStopArea, setScheduleListForCurrentStopArea] = useState<string[]>([]);
    const [routeDetailList, setRouteDetailList] = useState<RouteDetail[]>([])

    const [errorMessage, setErrorMessage] = useState<string>('');
    const [disturbanceForCurrentTransport, setDisturbanceForCurrentTransport] = useState<Disturbance[]>([])
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {
        getStationDetail();
    }, []);

    /**
     * Retrieves the station detail.
     * Retrieves the station id, the transport id and the route id of the page.
     */
    function getStationDetail() {
        setLoading(true);
        getStopAreaDetail(stopAreaId, transportId, routeId)
            .then(response => {
                if (response.status === 200) {
                    setStopAreaTransportRoute(response.data)
                    const stopAreaData = response.data;

                    setScheduleListForCurrentStopArea(stopAreaData.next_schedule)
                    setRouteDetailList(stopAreaData.route.route_detail);

                    getDisturbanceForTransport(stopAreaData.transport.code_line);
                } else {
                    setErrorMessage(response.data.message)
                    setLoading(false);
                }
            })
    }

    /**
     * Retrieves the disturbance information for a specific transport.
     * It uses Grand Lyon API.
     * 
     * @param codeLine - The code line representing the transport.
     * @returns The disturbance information for the transport.
     */
    function getDisturbanceForTransport(codeLine: string) {
        getDisturbance()
            .then(response => {
                setLoading(false);
                if (response.status === 200) {
                    const disturbances: Disturbance[] = response.data

                    setDisturbanceForCurrentTransport(disturbances.filter(disturbance => disturbance.code_line === codeLine))
                } else {
                    setErrorMessage(response.data.message)
                }
            })
    }

    /**
     * Rrefresh the page.
     */
    function onClickHandler() {
        getStationDetail();
    }

    /**
     * List of schedules to display.
     */
    const scheduleListToShow = scheduleListForCurrentStopArea?.map((schedule, index) => {
        return (
            <div key={index}>
                <RemainingTime time={schedule}/>
            </div>
        );
    });

    /**
     * List of disturbances to display.
     */
    const disturbanceListToShow = disturbanceForCurrentTransport.map((disturbance, index) => {
        return (
            <div key={index}>
                <DisturbanceCard disturbance={disturbance}/>
            </div>
        );
    });

    return (
        <>
            <Header/>
            {loading ? <h1>Chargement</h1> :
                <main className="transport-details">
                    <section className={'section-detail'}>
                        {errorMessage !== '' &&
                            <BannerError message={errorMessage}></BannerError>}
                        <h1>Station : {stopAreaTransportRoute?.stop_area.name}</h1>

                        {stopAreaTransportRoute !== undefined &&
                            <TransportImage lineName={stopAreaTransportRoute?.transport.line_name}/>}

                        <img src={stopAreaTransportRoute?.stop_area.wheelchair_accessible ? accessible : inaccessible}
                             alt={stopAreaTransportRoute?.stop_area.wheelchair_accessible ? 'Accessible' : 'Inaccessible'}/>

                        {stopAreaTransportRoute?.stop_area.escalator &&
                            <img src={escalator}
                                 alt={'Escalateur'}/>}

                        {stopAreaTransportRoute?.stop_area.lift &&
                            <img src={elevator}
                                 alt={'Ascenseur'}/>}

                        <p>Direction : {stopAreaTransportRoute?.route.complete_name}</p>
                        <p>{stopAreaTransportRoute?.stop_area.address.street}, {stopAreaTransportRoute?.stop_area.address.municipality}</p>
                        {scheduleListToShow !== undefined && scheduleListToShow.length > 0 &&
                            <div><p>Prochain(s) passage(s) : </p>{scheduleListToShow}</div>
                        }
                    </section>
                    <h1>Perturbations</h1>
                    { disturbanceForCurrentTransport.length > 0 ? disturbanceListToShow : 'Aucune perturbation sur la ligne'}
                    {stopAreaTransportRoute !== undefined &&
                        <StopList
                            listRouteDetail={stopAreaTransportRoute.route.route_detail}
                            transportId={stopAreaTransportRoute.transport.id}
                            routeId={stopAreaTransportRoute.route.id}
                        />
                    }
                    <ScheduleTable
                        handleOnClick={onClickHandler}
                        routeDetailList={routeDetailList}
                    />
                </main>
            }
        </>
    );
}

export default Details;