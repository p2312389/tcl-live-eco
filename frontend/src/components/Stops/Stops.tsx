import Header from '../Common/Header';
import Form from './Form';
import { useState } from 'react';
import { StopAreaTransportRoute } from '../../models/StopAreaTransportRoute';
import { getStopAreaNextToAddress } from '../../services/StopAreaServices';
import Card from './Card';
import '../../styles/AroundYou/Main.css';
import BannerError from '../Common/BannerError';
import StopsMap from './StopsMap';
import { StopAreaAround } from '../../models/StopAreaAround';

/**
 * Renders the Stops component.
 */
function Stops() {

    const [listCard, setListCard] = useState<React.JSX.Element[]>([]);
    const [stopAreaAround, setStopAreaAround] = useState<StopAreaAround>(new StopAreaAround([], '', ''));
    const [errorMessage, setErrorMessage] = useState<string>('');

    /**
     * Retrieves stop areas, transports, and routes based on the provided parameters.
     *
     * @param street - The street name.
     * @param municipality - The municipality name.
     * @param zipcode - The zipcode.
     * @param idTypeTransport - An array of transport type IDs.
     */
    function getStopAreasTransportsRoutes(street: string, municipality: string, zipcode: string, idTypeTransport: number[]) {
        setListCard([]);
        setErrorMessage('');

        if (street !== '' && zipcode !== '' && municipality !== '') {

            getStopAreaNextToAddress(
                street,
                municipality,
                zipcode,
                500,
                idTypeTransport
            )
                .then(response => {
                    if (response.status === 200) {

                        const stopAreaAroundTmp = new StopAreaAround(response.data.stop_area_transport_routes, response.data.latitude, response.data.longitude);
                        setStopAreaAround(stopAreaAroundTmp);

                        const stopAreasTransportsRoutes: StopAreaTransportRoute[] = stopAreaAroundTmp.stop_area_transport_routes;
                        if (stopAreasTransportsRoutes.length > 0) {

                            const listStopAreaContent = stopAreasTransportsRoutes.map(stopAreaWithTransportAndRoute =>
                                <div
                                    key={stopAreaWithTransportAndRoute.route.id + '-' + stopAreaWithTransportAndRoute.transport.id + '-' + stopAreaWithTransportAndRoute.stop_area.id}>
                                    <Card
                                        {...stopAreaWithTransportAndRoute}
                                    ></Card>
                                </div>
                            );
                            setListCard(listStopAreaContent);

                        } else {
                            setErrorMessage('Aucune station trouvée pour l\'adresse : ' + street + ', ' + zipcode + ' ' + municipality);
                        }
                    } else {
                        setErrorMessage(response.data.message);
                    }
                })
        } else {
            setErrorMessage('Veuillez saisir l\'adresse complète pour effectuer la recherche.');
        }
    }

    return (
        <>
            <Header/>
            <Form handleCallback={getStopAreasTransportsRoutes} showTypeTransportFilter={true} />
            <main className="stops">
                <h2>Autour de vous</h2>
                {errorMessage !== '' &&
                    <BannerError message={errorMessage}></BannerError>}
                <div>
                    <StopsMap stopAreaAround={stopAreaAround}></StopsMap>
                </div>
                <section>
                    {listCard}
                </section>
            </main>
        </>
    );
}

export default Stops;