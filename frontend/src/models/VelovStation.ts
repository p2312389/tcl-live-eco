import { Address } from './Address';
import { AvailabilityVelov } from './AvailabilityVelov';

export interface VelovStation {
    id: number;
    id_station: number;
    nom: string;
    nb_bornettes: number;
    ouverte: boolean;
    address: Address;
    availability: AvailabilityVelov;
}