import { StopArea } from "./StopArea";
import { Transport } from "./Transport";
import { Route } from "./Route";

export interface StopAreaTransportRoute {
    stop_area: StopArea,
    transport: Transport,
    route: Route,
    next_schedule: string[],
    terminal_stop_area: string
}