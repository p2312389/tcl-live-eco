import { Address } from './Address';

export interface StopArea {
    id: number,
    name: string,
    wheelchair_accessible: boolean,
    lift: boolean,
    escalator: boolean,
    address: Address,
    id_stop_area: string,
    list_next_schedules: string[]
}