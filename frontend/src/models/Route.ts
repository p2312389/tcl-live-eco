import { RouteDetail } from './RouteDetail';

export interface Route {
    id: number,
    name: string,
    complete_name: string,
    route_id: string,
    direction: string,
    route_detail: RouteDetail[]
}