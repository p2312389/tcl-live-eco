import { Transport } from './Transport';

export interface TypeTransport {
    id: number,
    label: string,
    transports: Transport[]
}