import { StopArea } from './StopArea';

export interface RouteDetail {
    id: number,
    route_detail_order: number,
    stop_area: StopArea
}