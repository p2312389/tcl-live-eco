export interface NextSchedule {
    id: number
    ligne: string
    direction: string
    delaipassage: string
    type: string
    heurepassage: Date
    idtarretdestination: number
    coursetheorique: string
    gid: number
    last_update_fme: Date
}