export interface Address {
    id: number;
    street: string;
    municipality: string;
    zipcode: string;
    latitude: string;
    longitude: string;
}