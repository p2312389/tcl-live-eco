import { StopAreaTransportRoute } from './StopAreaTransportRoute';

export interface IStopAreaAround {
    stop_area_transport_routes: StopAreaTransportRoute[];
    latitude: string;
    longitude: string;
}

export class StopAreaAround implements IStopAreaAround {

    stop_area_transport_routes: StopAreaTransportRoute[] = [];
    latitude: string = '';
    longitude: string = '';
    constructor(stop_area_transport_routes: StopAreaTransportRoute[], latitude: string, longitude: string) {
        this.stop_area_transport_routes = stop_area_transport_routes;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}