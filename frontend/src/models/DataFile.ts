interface DataFileInterface {
    id: number;
    fileName: string;
    fileExtension: string;
    completeFileName: string;
    className: string;
    errorMessage: string;
    message: string;
    status: number;
}

export class DataFile implements DataFileInterface {
    id: number;
    fileName: string;
    fileExtension: string;
    completeFileName: string;
    className: string;
    errorMessage: string;
    message: string;
    status: number;

    constructor(dataFile: DataFile) {
        this.id = dataFile.id;
        this.fileName = dataFile.fileName;
        this.fileExtension = dataFile.fileExtension;
        this.completeFileName = dataFile.completeFileName;
        this.className = dataFile.className;
        this.errorMessage = dataFile.errorMessage;
        this.message = dataFile.message;
        this.status = dataFile.status;
    }
}