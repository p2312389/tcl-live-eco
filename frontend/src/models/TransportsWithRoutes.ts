import {Transport} from "./Transport";
import {Route} from "./Route";

export interface TransportsWithRoutes {
    transport: Transport,
    list_routes: Route[]
}