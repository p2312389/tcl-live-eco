import { AvailableBike } from './AvailableBike';


export interface AvailabilityVelov {
    id_station: number;
    availability_code: number;
    availability_color: string;
    status: boolean;
    available_bike: AvailableBike;
}