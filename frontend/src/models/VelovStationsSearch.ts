import { VelovStation } from './VelovStation';

export interface IVelovStationsSearch {
    velov_stations: VelovStation[];
    latitude: string;
    longitude: string;
}

export class VelovStationsSearch implements IVelovStationsSearch {
    velov_stations: VelovStation[] = [];
    latitude: string = '';
    longitude: string = '';
    constructor(velovStations: VelovStation[], latitude: string, longitude: string) {
        this.velov_stations = velovStations;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}