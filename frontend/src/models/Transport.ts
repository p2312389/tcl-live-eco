import { TypeTransport } from './TypeTransport';
import { Route } from './Route';

export interface Transport {
    id: number,
    line_name: string,
    code_trace: string,
    code_line: string,
    wheelchair_accessible: boolean
    type_transport: TypeTransport
    routes: Route[]
}