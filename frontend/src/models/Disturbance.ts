export interface Disturbance {
    type: string
    cause: string
    debut: string
    fin: string
    mode: string
    code_line: string
    line_name: string
    titre: string
    message: string
    last_update_fme: Date
    type_severite: string
    niveau_severite: number
    code_lines: string[]
}