export interface AvailableBike {
    bikes_available: number;
    electrical_bikes: number;
    electrical_internal_battery_bikes: number;
    electrical_removable_battery_bikes: number;
    mechanical_bikes: number;
    capacity: number;
}