export class ApiResponse {
    codeStatus: number;
    errorMessage: string;
    isError: boolean;
    reponseContent?: Promise<any>;


    constructor(apiResponse: ApiResponse) {
        this.codeStatus = apiResponse.codeStatus;
        this.errorMessage = apiResponse.errorMessage;
        this.isError = apiResponse.isError;
        this.reponseContent = apiResponse.reponseContent
    }
}