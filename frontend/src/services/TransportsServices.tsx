import axios from 'axios';
import { REST_SERVICE_URL } from '../utils/constants';

const commonUrl = REST_SERVICE_URL + '/transport';

export async function autocompleteTransportSearch(keyword: string) {
    return await axios.get(commonUrl + `/autocomplete?keyword=${keyword}`)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.toJSON();
        });
}

export async function getTransportListFromTypeTransport() {
    return await axios.get(commonUrl + `/type-transport`)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.toJSON();
        });
}

export async function getTransportListWithParams(transportName: string, idTypeTransports: number[]) {
    let idTypeTransportsParam = '';
    if (idTypeTransports.length > 0) {
        idTypeTransports.forEach(idTypeTransport => {
            idTypeTransportsParam += '&idTypeTransport[]=' + idTypeTransport;
        })
    }

    // Pas de & après la liste d'idTypeTransport car il est déjà rajouté
    const urlGetTransports = idTypeTransportsParam !== '' ?
        commonUrl + `/search-transport?transport-name=${transportName}${idTypeTransportsParam}` :
        commonUrl + `/search-transport?transport-name=${transportName}`

    return await axios.get(urlGetTransports)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.response;
        });
}

export async function getTransportWithId(transportId: number) {
    return await axios.get(commonUrl + `/${transportId}`)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.response;
        });
}
