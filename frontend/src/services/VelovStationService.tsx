import { REST_SERVICE_URL } from '../utils/constants';
import axios from 'axios';

export async function getVelovStationNearPostion(
    street: string,
    municipality: string,
    zipcode: string,
    distance: number
) {
    return await axios.get(REST_SERVICE_URL + `/near-velov-station?street=${street}&municipality=${municipality}&zipcode=${zipcode}&dist=${distance}`)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.response;
        });
}