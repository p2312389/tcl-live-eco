import axios from 'axios';

const commonUrl = 'http://127.0.0.1:8000/api/database-handler/';

export function getFileForDatabase() {
    return fetch(commonUrl + `get-files/`, {method: 'GET'})
        .then((response) => response.json())
        .then((response) => {
            return response;
        })
}

export async function uploadAllXmlData(fileName: string) {
    const urlList: string[] = [
        commonUrl + `create-timetable?file-name=${fileName}`,
        commonUrl + `create-stop-area-address-xml?file-name=${fileName}`,
        commonUrl + `create-routes?file-name=${fileName}`,
        commonUrl + `create-route-detail?file-name=${fileName}`,
    ];

    let lastResponse;

    for (const url of urlList) {
        const response = await uploadCurrentXmlData(url)
        console.log(response)
        if (response.status !== 201) {
            return response.message;
        } else {
            lastResponse = response;
        }
    }

    return lastResponse;
}

function uploadCurrentXmlData(url: string) {
    return axios.get(url)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.toJSON();
        });
}

export function uploadTypeTransport() {
    return axios.get(commonUrl + `create-type-transports`)
        .then(function (response) {
            console.log(response.status);
            return response;
        })
        .catch(function (error) {
            console.log(error.toJSON());
            return error.toJSON();
        });
}

export function uploadBus(startIndex: number, endIndex: number) {
    return axios.get(commonUrl + `create-bus?start-index=${startIndex}&end-index=${endIndex}`)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.toJSON();
        });
}

export function uploadMetro(startIndex: number, endIndex: number) {
    return axios.get(commonUrl + `create-metro?start-index=${startIndex}&end-index=${endIndex}`)
        .then(function (response) {
            console.log(response.status);
            return response;
        })
        .catch(function (error) {
            console.log(error.toJSON());
            return error.toJSON();
        });
}

export function uploadTramway(startIndex: number, endIndex: number) {
    return axios.get(commonUrl + `create-tramway?start-index=${startIndex}&end-index=${endIndex}`)
        .then(function (response) {
            console.log(response.status);
            return response;
        })
        .catch(function (error) {
            console.log(error.toJSON());
            return error.toJSON();
        });
}

export async function uploadStopAreaV2(startIndex: number, endIndex: number): Promise<any> {
    const response = await uploadStopArea(startIndex, endIndex)
    if (response.status !== 201 || (response.data !== undefined && (response.data.nextStartIndex < 1 || response.data.nextEndIndex < 1))) {
        return response;
    }
    return await uploadStopAreaV2(response.data.nextStartIndex, response.data.nextEndIndex);
}

function uploadStopArea(startIndex: number, endIndex: number) {
    return axios.get(commonUrl + `create-stop-area?start-index=${startIndex}&end-index=${endIndex}`)
        .then(function (response) {
            console.log(response.status);
            return response;
        })
        .catch(function (error) {
            console.log(error.toJSON());
            return error.toJSON();
        });
}

export async function uploadAllSchedules(startIndex: number, endIndex: number, fileName: string): Promise<any> {
    const response = await uploadSchedule(startIndex, endIndex, fileName)
    if (response.status !== 201 || (response.data !== undefined && (response.data.nextStartIndex < 1 || response.data.nextEndIndex < 1))) {
        return response;
    }
    return await uploadAllSchedules(response.data.nextStartIndex, response.data.nextEndIndex, fileName);
}

function uploadSchedule(startIndex: number, endIndex: number, fileName: string) {
    return axios.get(commonUrl + `create-schedule?start-index=${startIndex}&end-index=${endIndex}&file-name=${fileName}`)
        .then(function (response) {
            console.log(response.status);
            return response;
        })
        .catch(function (error) {
            console.log(error.toJSON());
            return error.toJSON();
        });
}
