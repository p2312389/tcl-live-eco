import axios from 'axios';
import { REST_SERVICE_URL } from '../utils/constants';

// idTypeTransport = 0 -> pas de filtre sur les types de transport
export async function getStopAreaNextToAddress(
    street: string,
    municipality: string,
    zipcode: string,
    distance: number,
    idTypeTransports: number[]
) {

    let idTypeTransportsParam = '';
    if (idTypeTransports.length > 0) {
        idTypeTransports.forEach(idTypeTransport => {
            idTypeTransportsParam += '&idTransport[]=' + idTypeTransport;
        })
    }

    // Pas de & après la liste d'idTypeTransport car il est déjà rajouté
    const urlGetStopArea = idTypeTransportsParam !== '' ?
        REST_SERVICE_URL + `/stop-area-proches?street=${street}&municipality=${municipality}&zipcode=${zipcode}&dist=${distance}${idTypeTransportsParam}` :
        REST_SERVICE_URL + `/stop-area-proches?street=${street}&municipality=${municipality}&zipcode=${zipcode}&dist=${distance}`

    return await axios.get(urlGetStopArea)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.response;
        });
}

export async function getStopAreaDetail(stopAreaId: number, transportId: number, routeId: number) {
    return await axios.get(REST_SERVICE_URL + `/detail-station?id-stop-area=${stopAreaId}&id-transport=${transportId}&id-routes=${routeId}`)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.response;
        });
}

export async function getNextSchedule() {
    return await axios.get(REST_SERVICE_URL + `/next-schedule`)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.response;
        });
}