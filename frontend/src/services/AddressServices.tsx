import { REST_SERVICE_URL } from '../utils/constants';
import axios from 'axios';

const commonUrl = REST_SERVICE_URL + '/address';
export async function autocompleteStreetSearch(keyword: string) {
    return await axios.get(commonUrl + `/autocomplete-street?keyword=${keyword}`)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.toJSON();
        });
}

export async function autocompleteZipcodeSearch(keyword: string) {
    return await axios.get(commonUrl + `/autocomplete-zipcode?keyword=${keyword}`)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.toJSON();
        });
}

export async function autocompleteCitySearch(keyword: string) {
    return await axios.get(commonUrl + `/autocomplete-city?keyword=${keyword}`)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.toJSON();
        });
}