import { REST_SERVICE_URL } from '../utils/constants';
import axios from 'axios';

export async function getDisturbance() {
    return await axios.get(REST_SERVICE_URL + `/disturbance`)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.response;
        });
}