import axios from 'axios';
import { REST_SERVICE_URL } from '../utils/constants';

const commonUrl = REST_SERVICE_URL + '/type-transport';

export async function getTypeTransports() {
    return await axios.get(commonUrl)
        .then(function (response) {
            return response;
        })
        .catch(function (error) {
            return error.toJSON();
        });
}