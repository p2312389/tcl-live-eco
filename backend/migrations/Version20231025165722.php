<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231025165722 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stop_area ADD stop_area_id INT NOT NULL');
        $this->addSql('ALTER TABLE stop_area ADD CONSTRAINT FK_3D72FF771AD056BA FOREIGN KEY (stop_area_id) REFERENCES stop_area (id)');
        $this->addSql('CREATE INDEX IDX_3D72FF771AD056BA ON stop_area (stop_area_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stop_area DROP FOREIGN KEY FK_3D72FF771AD056BA');
        $this->addSql('DROP INDEX IDX_3D72FF771AD056BA ON stop_area');
        $this->addSql('ALTER TABLE stop_area DROP stop_area_id');
    }
}
