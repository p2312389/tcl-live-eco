<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231024184258 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE schedule ADD available_id INT NOT NULL');
        $this->addSql('ALTER TABLE schedule ADD CONSTRAINT FK_5A3811FB36D3FBA2 FOREIGN KEY (available_id) REFERENCES timetable (id)');
        $this->addSql('CREATE INDEX IDX_5A3811FB36D3FBA2 ON schedule (available_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE schedule DROP FOREIGN KEY FK_5A3811FB36D3FBA2');
        $this->addSql('DROP INDEX IDX_5A3811FB36D3FBA2 ON schedule');
        $this->addSql('ALTER TABLE schedule DROP available_id');
    }
}
