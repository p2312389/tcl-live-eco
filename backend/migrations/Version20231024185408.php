<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231024185408 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE route_detail ADD routes_id INT NOT NULL');
        $this->addSql('ALTER TABLE route_detail ADD CONSTRAINT FK_1FF58E4EAE2C16DC FOREIGN KEY (routes_id) REFERENCES routes (id)');
        $this->addSql('CREATE INDEX IDX_1FF58E4EAE2C16DC ON route_detail (routes_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE route_detail DROP FOREIGN KEY FK_1FF58E4EAE2C16DC');
        $this->addSql('DROP INDEX IDX_1FF58E4EAE2C16DC ON route_detail');
        $this->addSql('ALTER TABLE route_detail DROP routes_id');
    }
}
