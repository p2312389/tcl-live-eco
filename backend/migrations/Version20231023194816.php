<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231023194816 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE transports ADD type_id INT NOT NULL');
        $this->addSql('ALTER TABLE transports ADD CONSTRAINT FK_C7BE69E5C54C8C93 FOREIGN KEY (type_id) REFERENCES type_transports (id)');
        $this->addSql('CREATE INDEX IDX_C7BE69E5C54C8C93 ON transports (type_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE transports DROP FOREIGN KEY FK_C7BE69E5C54C8C93');
        $this->addSql('DROP INDEX IDX_C7BE69E5C54C8C93 ON transports');
        $this->addSql('ALTER TABLE transports DROP type_id');
    }
}
