<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231023200202 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE perturbation ADD transports_id INT NOT NULL');
        $this->addSql('ALTER TABLE perturbation ADD CONSTRAINT FK_B4F2E894518E99D9 FOREIGN KEY (transports_id) REFERENCES transports (id)');
        $this->addSql('CREATE INDEX IDX_B4F2E894518E99D9 ON perturbation (transports_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE perturbation DROP FOREIGN KEY FK_B4F2E894518E99D9');
        $this->addSql('DROP INDEX IDX_B4F2E894518E99D9 ON perturbation');
        $this->addSql('ALTER TABLE perturbation DROP transports_id');
    }
}
