<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231024184633 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE schedule ADD planned_id INT NOT NULL');
        $this->addSql('ALTER TABLE schedule ADD CONSTRAINT FK_5A3811FB2634E2A2 FOREIGN KEY (planned_id) REFERENCES route_detail (id)');
        $this->addSql('CREATE INDEX IDX_5A3811FB2634E2A2 ON schedule (planned_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE schedule DROP FOREIGN KEY FK_5A3811FB2634E2A2');
        $this->addSql('DROP INDEX IDX_5A3811FB2634E2A2 ON schedule');
        $this->addSql('ALTER TABLE schedule DROP planned_id');
    }
}
