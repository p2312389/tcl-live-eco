<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231024183116 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE routes ADD departure_id INT NOT NULL, ADD terminal_id INT NOT NULL');
        $this->addSql('ALTER TABLE routes ADD CONSTRAINT FK_32D5C2B37704ED06 FOREIGN KEY (departure_id) REFERENCES stop_area (id)');
        $this->addSql('ALTER TABLE routes ADD CONSTRAINT FK_32D5C2B3E77B6CE8 FOREIGN KEY (terminal_id) REFERENCES stop_area (id)');
        $this->addSql('CREATE INDEX IDX_32D5C2B37704ED06 ON routes (departure_id)');
        $this->addSql('CREATE INDEX IDX_32D5C2B3E77B6CE8 ON routes (terminal_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE routes DROP FOREIGN KEY FK_32D5C2B37704ED06');
        $this->addSql('ALTER TABLE routes DROP FOREIGN KEY FK_32D5C2B3E77B6CE8');
        $this->addSql('DROP INDEX IDX_32D5C2B37704ED06 ON routes');
        $this->addSql('DROP INDEX IDX_32D5C2B3E77B6CE8 ON routes');
        $this->addSql('ALTER TABLE routes DROP departure_id, DROP terminal_id');
    }
}
