<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231027191204 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stop_area CHANGE wheelchair_accessible wheelchair_accessible TINYINT(1) DEFAULT NULL, CHANGE lift lift TINYINT(1) DEFAULT NULL, CHANGE escalator escalator TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stop_area CHANGE wheelchair_accessible wheelchair_accessible TINYINT(1) NOT NULL, CHANGE lift lift TINYINT(1) NOT NULL, CHANGE escalator escalator TINYINT(1) NOT NULL');
    }
}
