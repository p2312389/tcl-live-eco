<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231024172959 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE routes ADD transports_id INT NOT NULL');
        $this->addSql('ALTER TABLE routes ADD CONSTRAINT FK_32D5C2B3518E99D9 FOREIGN KEY (transports_id) REFERENCES transports (id)');
        $this->addSql('CREATE INDEX IDX_32D5C2B3518E99D9 ON routes (transports_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE routes DROP FOREIGN KEY FK_32D5C2B3518E99D9');
        $this->addSql('DROP INDEX IDX_32D5C2B3518E99D9 ON routes');
        $this->addSql('ALTER TABLE routes DROP transports_id');
    }
}
