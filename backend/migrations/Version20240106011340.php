<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240106011340 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE velov_station (id INT AUTO_INCREMENT NOT NULL, address_id INT NOT NULL, id_station INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, nb_bornettes INT DEFAULT NULL, ouverte TINYINT(1) NOT NULL, INDEX IDX_D25CD93BF5B7AF75 (address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE velov_station ADD CONSTRAINT FK_D25CD93BF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE velov_station DROP FOREIGN KEY FK_D25CD93BF5B7AF75');
        $this->addSql('DROP TABLE velov_station');
    }
}
