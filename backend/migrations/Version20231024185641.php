<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231024185641 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE route_detail ADD stop_area_id INT NOT NULL');
        $this->addSql('ALTER TABLE route_detail ADD CONSTRAINT FK_1FF58E4E1AD056BA FOREIGN KEY (stop_area_id) REFERENCES stop_area (id)');
        $this->addSql('CREATE INDEX IDX_1FF58E4E1AD056BA ON route_detail (stop_area_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE route_detail DROP FOREIGN KEY FK_1FF58E4E1AD056BA');
        $this->addSql('DROP INDEX IDX_1FF58E4E1AD056BA ON route_detail');
        $this->addSql('ALTER TABLE route_detail DROP stop_area_id');
    }
}
