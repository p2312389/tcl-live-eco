<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231101084514 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE schedule_timetable (schedule_id INT NOT NULL, timetable_id INT NOT NULL, INDEX IDX_C9B78800A40BC2D5 (schedule_id), INDEX IDX_C9B78800CC306847 (timetable_id), PRIMARY KEY(schedule_id, timetable_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE schedule_timetable ADD CONSTRAINT FK_C9B78800A40BC2D5 FOREIGN KEY (schedule_id) REFERENCES schedule (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE schedule_timetable ADD CONSTRAINT FK_C9B78800CC306847 FOREIGN KEY (timetable_id) REFERENCES timetable (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE schedule_timetable DROP FOREIGN KEY FK_C9B78800A40BC2D5');
        $this->addSql('ALTER TABLE schedule_timetable DROP FOREIGN KEY FK_C9B78800CC306847');
        $this->addSql('DROP TABLE schedule_timetable');
    }
}
