<?php

namespace App\Models;

use App\Entity\Routes;

class RoutesVO
{
    private int $id;
    private string $name;
    private string $completeName;
    private array $routeDetail;
    private string $direction;

    public function __construct() {}

    public static function createWithRoutes(Routes $routes): RoutesVO
    {
        $newInstance = new self();
        $newInstance->id = $routes->getId();
        $newInstance->name = $routes->getName();
        $newInstance->completeName = $routes->getCompleteName();
        foreach ($routes->getRouteDetails() as $routeDetail) {
            $newInstance->routeDetail[] = RouteDetailVO::createWithRouteDetail($routeDetail);
        }
        $newInstance->direction = $routes->getDirection();
        return $newInstance;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getRouteDetail(): array
    {
        return $this->routeDetail;
    }

    public function setRouteDetail(array $routeDetail): void
    {
        $this->routeDetail = $routeDetail;
    }

    public function getCompleteName(): string
    {
        return $this->completeName;
    }

    public function setCompleteName(string $completeName): void
    {
        $this->completeName = $completeName;
    }

    public function getDirection(): string
    {
        return $this->direction;
    }

    public function setDirection(string $direction): void
    {
        $this->direction = $direction;
    }
}