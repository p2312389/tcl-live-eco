<?php

namespace App\Models;

use App\Entity\RouteDetail;

class RouteDetailVO
{
    private int $id;
    private int $routeDetailOrder;
    private StopAreaVO $stopArea;

    public function __construct() {}

    public static function createWithRouteDetail(RouteDetail $routeDetail): RouteDetailVO
    {
        $newInstance = new self();
        $newInstance->id = $routeDetail->getId();
        $newInstance->routeDetailOrder = $routeDetail->getRouteDetailOrder();
        $newInstance->stopArea = StopAreaVO::createWithStopArea($routeDetail->getStopArea());
        return $newInstance;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getRouteDetailOrder(): int
    {
        return $this->routeDetailOrder;
    }

    public function setRouteDetailOrder(int $routeDetailOrder): void
    {
        $this->routeDetailOrder = $routeDetailOrder;
    }

    public function getStopArea(): StopAreaVO
    {
        return $this->stopArea;
    }

    public function setStopArea(StopAreaVO $stopArea): void
    {
        $this->stopArea = $stopArea;
    }
}