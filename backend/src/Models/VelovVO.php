<?php

namespace App\Models;

use App\Entity\VelovStation;

class VelovVO
{
    private int $id;
    private int $idStation;
    private string $nom;
    private int $nbBornettes;
    private bool $ouverte;
    private AddressVO $address;
    private AvailabilityVelovVO $availability;

    public function __construct() {
        // Constructeur par défaut
    }

    public static function createVelovVO(VelovStation $velovStation): VelovVO
    {
        $newInstance = new self();
        $newInstance->setId($velovStation->getId());
        $newInstance->setIdstation($velovStation->getId());
        $newInstance->setNom($velovStation->getNom());
        $newInstance->setOuverte($velovStation->isOuverte());
        $newInstance->setNbBornettes($velovStation->getNbBornettes());
        $newInstance->setAddress(AddressVO::createWithAddress($velovStation->getAddress()));
        return $newInstance;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getIdStation(): int
    {
        return $this->idStation;
    }

    public function setIdStation(int $idStation): void
    {
        $this->idStation = $idStation;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function getNbBornettes(): int
    {
        return $this->nbBornettes;
    }

    public function setNbBornettes(int $nbBornettes): void
    {
        $this->nbBornettes = $nbBornettes;
    }

    public function isOuverte(): bool
    {
        return $this->ouverte;
    }

    public function setOuverte(bool $ouverte): void
    {
        $this->ouverte = $ouverte;
    }

    public function getAddress(): AddressVO
    {
        return $this->address;
    }

    public function setAddress(AddressVO $address): void
    {
        $this->address = $address;
    }

    public function getAvailability(): AvailabilityVelovVO
    {
        return $this->availability;
    }

    public function setAvailability(AvailabilityVelovVO $availability): void
    {
        $this->availability = $availability;
    }
}