<?php

namespace App\Models;

class VelovStationsSearch
{
    private array $velovStations;
    private string $latitude;
    private string $longitude;

    public function __construct(array $velovStations, string $latitude, string $longitude)
    {
        $this->velovStations = $velovStations;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    public function getVelovStations(): array
    {
        return $this->velovStations;
    }

    public function setVelovStations(array $velovStations): void
    {
        $this->velovStations = $velovStations;
    }

    public function getLatitude(): string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): void
    {
        $this->latitude = $latitude;
    }

    public function getLongitude(): string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): void
    {
        $this->longitude = $longitude;
    }
}