<?php

namespace App\Models;

use App\Entity\Routes;
use App\Entity\StopArea;
use App\Entity\Transports;

class StopAreaDetailVO
{
    private TransportsVO $transport;
    private StopAreaVO $stopArea;
    private RoutesVO $route;
    private array $nextSchedule;
    private string $dateLastUpdate;
    private string $terminalStopArea;

    public function __construct(Transports $transports, StopArea $stopArea, Routes $routes) {
        $this->stopArea = StopAreaVO::createWithStopArea($stopArea);
        $this->transport = TransportsVO::createWithTransports($transports);
        $this->route = RoutesVO::createWithRoutes($routes);
        $this->terminalStopArea = $this->getTerminal($this->route->getCompleteName());
    }

    public function getTransport(): TransportsVO
    {
        return $this->transport;
    }

    public function setTransport(TransportsVO $transport): void
    {
        $this->transport = $transport;
    }

    public function getStopArea(): StopAreaVO
    {
        return $this->stopArea;
    }

    public function setStopArea(StopAreaVO $stopArea): void
    {
        $this->stopArea = $stopArea;
    }

    public function getRoute(): RoutesVO
    {
        return $this->route;
    }

    public function setRoute(RoutesVO $routes): void
    {
        $this->route = $routes;
    }

    public function getNextSchedule(): array
    {
        return $this->nextSchedule;
    }

    public function setNextSchedule(array $nextSchedule): void
    {
        $this->nextSchedule = $nextSchedule;
    }

    public function addSchedule(string $schedule): void
    {
        $this->nextSchedule[] = $schedule;
    }

    public function getDateLastUpdate(): string
    {
        return $this->dateLastUpdate;
    }

    public function setDateLastUpdate(string $dateLastUpdate): void
    {
        $this->dateLastUpdate = $dateLastUpdate;
    }

    public function getTerminalStopArea(): string
    {
        return $this->terminalStopArea;
    }

    public function setTerminalStopArea(string $terminalStopArea): void
    {
        $this->terminalStopArea = $terminalStopArea;
    }

    private function getTerminal(string $completeRouteName): string {
        $terminal = explode(" - ", $completeRouteName);
        return $terminal[1];
    }
}