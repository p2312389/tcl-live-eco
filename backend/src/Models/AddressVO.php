<?php

namespace App\Models;

use App\Entity\Address;

class AddressVO
{
    private int $id;
    private string $street;
    private string $municipality;
    private string $zipcode;
    private string $latitude;
    private string $longitude;

    public function __construct() {}

    public static function createWithAddress(Address $address): AddressVO
    {
        $newInstance = new self();
        $newInstance->id = $address->getId();
        $newInstance->street = $address->getStreet();
        $newInstance->municipality = $address->getMunicipality();
        $newInstance->zipcode = $address->getZipCode();
        $newInstance->latitude = $address->getLatitude();
        $newInstance->longitude = $address->getLongitude();
        return $newInstance;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    public function getMunicipality(): string
    {
        return $this->municipality;
    }

    public function setMunicipality(string $municipality): void
    {
        $this->municipality = $municipality;
    }

    public function getZipcode(): string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): void
    {
        $this->zipcode = $zipcode;
    }

    public function getLatitude(): string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): void
    {
        $this->latitude = $latitude;
    }

    public function getLongitude(): string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): void
    {
        $this->longitude = $longitude;
    }
}