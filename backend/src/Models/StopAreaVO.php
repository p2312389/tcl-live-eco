<?php

namespace App\Models;

use App\Entity\StopArea;

class StopAreaVO
{
    private int $id;
    private string $name;
    private bool $wheelchairAccessible;
    private bool $lift;
    private bool $escalator;
    private AddressVO $address;
    private string $idStopArea;
    private array $listNextSchedules;
    private array $transports;

    public function __construct()
    {

    }

    // Obligé de créer un constructor comme ceci car pas possible de créer plusieurs constructeur en PHP :(
    public static function createWithStopArea(StopArea $stopArea): StopAreaVO
    {
        $newInstance = new self();
        $newInstance->id = $stopArea->getId();
        $newInstance->name = $stopArea->getName();
        $newInstance->wheelchairAccessible = $stopArea->isWheelchairAccessible();
        $newInstance->lift = $stopArea->isLift();
        $newInstance->escalator = $stopArea->isEscalator();
        $newInstance->address = AddressVO::createWithAddress($stopArea->getAddress());
        $newInstance->idStopArea = $stopArea->getIdStopArea();
        foreach ($stopArea->getRoutes() as $route) {
            $newInstance->transports[] = $route->getTransports()->getLineName();
        }
        return $newInstance;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function isWheelchairAccessible(): bool
    {
        return $this->wheelchairAccessible;
    }

    public function setWheelchairAccessible(bool $wheelchairAccessible): void
    {
        $this->wheelchairAccessible = $wheelchairAccessible;
    }

    public function isLift(): bool
    {
        return $this->lift;
    }

    public function setLift(bool $lift): void
    {
        $this->lift = $lift;
    }

    public function isEscalator(): bool
    {
        return $this->escalator;
    }

    public function setEscalator(bool $escalator): void
    {
        $this->escalator = $escalator;
    }

    public function getAddress(): AddressVO
    {
        return $this->address;
    }

    public function setAddress(AddressVO $address): void
    {
        $this->address = $address;
    }

    public function getIdStopArea(): string
    {
        return $this->idStopArea;
    }

    public function setIdStopArea(string $idStopArea): void
    {
        $this->idStopArea = $idStopArea;
    }

    public function getListNextSchedules(): array
    {
        return $this->listNextSchedules;
    }

    public function setListNextSchedules(array $listNextSchedules): void
    {
        $this->listNextSchedules = $listNextSchedules;
    }

    public function addSchedule(string $schedule): void
    {
        $this->listNextSchedules[] = $schedule;
    }

    public function getTransports(): array
    {
        return $this->transports;
    }

    public function setTransports(array $transports): void
    {
        $this->transports = $transports;
    }
}