<?php

namespace App\Models;

use App\Entity\TypeTransports;

class TypeTransportVO
{
    private int $id;
    private string $label;
    private array $transports;

    public function __construct() {}

    public static function createWithTypeTransport(TypeTransports $typeTransports): TypeTransportVO
    {
        $newInstance = new self();
        $newInstance->id = $typeTransports->getId();
        $newInstance->label = $typeTransports->getLabel();
        return $newInstance;
    }

    public static function createWithTypeTransportWithTransport(TypeTransports $typeTransports): TypeTransportVO
    {
        $newInstance = new self();
        $newInstance->id = $typeTransports->getId();
        $newInstance->label = $typeTransports->getLabel();
        foreach ($typeTransports->getTransports() as $transport) {
            $newInstance->transports[] = TransportsVO::createWithTransports($transport);
        }
        return $newInstance;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getTransports(): array
    {
        return $this->transports;
    }

    public function setTransports(array $transports): void
    {
        $this->transports = $transports;
    }
}