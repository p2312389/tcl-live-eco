<?php 

namespace App\Models;

use App\Entity\Transports;
use App\Entity\Routes;

// Définition de la classe TransportAvecRoutesResulat
class TransportAvecRoutesResultat {
    public Transports $transport;
    public $listRoutes;
    public $TARRid;

    public function __construct(Transports $transportParametre) {
        $this->TARRid = $transportParametre->getId();
        $this->transport = $transportParametre;
        $this->listRoutes=[];
    }

    public function setTransportAndId(Transports $transportParametre) {
        $this->TARRid = $transportParametre->getId();
        $this->transport = $transportParametre;
        $this->listRoutes = [];
    }

    public function setListe($ListRoutes) {
        $this->listRoutes = $ListRoutes;
    }

    public function addRoute(Routes $route){
        //tester si pas deja ajouté
        //renvois 1 si ajout effectué 0 sinon
        if(!in_array($route,$this->listRoutes)){   ////////$route->getTransport()->getid() == $TARRid
            array_push($this->listRoutes,$route);
            return 1;
        }
        return 0;

    }

    public function getTARRid(){
        return $this->TARRid;
    }
}