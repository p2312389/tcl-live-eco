<?php

namespace App\Models;

class AvailabilityVelovVO
{
    private int $idStation;
    // 0 -> aucune info
    // 1 -> disponible
    // 2 -> moins de 50%
    // 3 -> Pas de vélo'v disponible
    private int $availabilityCode;
    private string $availabilityColor;
    private bool $status;
    private AvailabilityBikeVO $availableBike;

    public function __construct(array $velovStation)
    {
        $this->idStation = $velovStation['number'];
        // Comme les codes couleur n'ont pas de sens, on créé les propres code couleur de l'application
        $this->availableBike = new AvailabilityBikeVO($velovStation['main_stands']);

        if ($this->availableBike->getCapacity() > 0) {
            $percentageBikeAvailable = ($this->availableBike->getBikesAvailable() * 100 / $this->availableBike->getCapacity());
            if ($percentageBikeAvailable >= 50) {
                $this->availabilityCode = 1;
            }
            if ($percentageBikeAvailable < 50) {
                $this->availabilityCode = 2;
            }
            if ($percentageBikeAvailable === 0) {
                $this->availabilityCode = 3;
            }
        }
        else {
            $this->availabilityCode = 0;
        }

        $this->availabilityColor = $velovStation['availability'];
        $this->status = $velovStation['status'] === "OPEN";
    }

    public function getIdStation(): int
    {
        return $this->idStation;
    }

    public function setIdStation(int $idStation): void
    {
        $this->idStation = $idStation;
    }

    public function getAvailabilityCode(): int
    {
        return $this->availabilityCode;
    }

    public function setAvailabilityCode(int $availabilityCode): void
    {
        $this->availabilityCode = $availabilityCode;
    }

    public function getAvailabilityColor(): string
    {
        return $this->availabilityColor;
    }

    public function setAvailabilityColor(string $availabilityColor): void
    {
        $this->availabilityColor = $availabilityColor;
    }

    public function isStatus(): bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }

    public function getAvailableBike(): AvailabilityBikeVO
    {
        return $this->availableBike;
    }

    public function setAvailableBike(AvailabilityBikeVO $availableBike): void
    {
        $this->availableBike = $availableBike;
    }
}