<?php

namespace App\Models;

class AvailabilityBikeVO
{
    private int $bikesAvailable;
    private int $electricalBikes;
    private int $electricalInternalBatteryBikes;
    private int $electricalRemovableBatteryBikes;
    private int $mechanicalBikes;
    private int $capacity;

    public function __construct(array $availability)
    {
        $this->bikesAvailable = $availability['availabilities']['bikes'];
        $this->electricalBikes = $availability['availabilities']['electricalBikes'];
        $this->electricalInternalBatteryBikes = $availability['availabilities']['electricalInternalBatteryBikes'];
        $this->electricalRemovableBatteryBikes = $availability['availabilities']['electricalRemovableBatteryBikes'];
        $this->mechanicalBikes = $availability['availabilities']['mechanicalBikes'];
        $this->capacity = $availability['capacity'];
    }

    public function getBikesAvailable(): int
    {
        return $this->bikesAvailable;
    }

    public function setBikesAvailable(int $bikesAvailable): void
    {
        $this->bikesAvailable = $bikesAvailable;
    }

    public function getElectricalBikes(): int
    {
        return $this->electricalBikes;
    }

    public function setElectricalBikes(int $electricalBikes): void
    {
        $this->electricalBikes = $electricalBikes;
    }

    public function getElectricalInternalBatteryBikes(): int
    {
        return $this->electricalInternalBatteryBikes;
    }

    public function setElectricalInternalBatteryBikes(int $electricalInternalBatteryBikes): void
    {
        $this->electricalInternalBatteryBikes = $electricalInternalBatteryBikes;
    }

    public function getElectricalRemovableBatteryBikes(): int
    {
        return $this->electricalRemovableBatteryBikes;
    }

    public function setElectricalRemovableBatteryBikes(int $electricalRemovableBatteryBikes): void
    {
        $this->electricalRemovableBatteryBikes = $electricalRemovableBatteryBikes;
    }

    public function getMechanicalBikes(): int
    {
        return $this->mechanicalBikes;
    }

    public function setMechanicalBikes(int $mechanicalBikes): void
    {
        $this->mechanicalBikes = $mechanicalBikes;
    }

    public function getCapacity(): int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): void
    {
        $this->capacity = $capacity;
    }
}