<?php

namespace App\Models;

class DisturbanceVO
{
    private string $type;
    private string $cause;
    private string $debut;
    private string $fin;
    private string $mode;
    private string $codeLine;
    private string $lineName;
    private string $titre;
    private string $message;
    private string $last_update_fme;
    private string $typeSeverite;
    private int $niveauSeverite;
    private string $typeObjet;
    private array $codeLines;

    public function __construct()
    {
        // Constructeur par défaut
    }

    /**
     * Builder pour créer un VO directement avec les informations retournés par l'API du grand Lyon.
     *
     * @param array $disturbanceFromGrandLyon Tableau associatif qui a pour clé le nom de la variable .
     * et pour valeur la valeur de la variable renvoyée par le grand Lyon.
     * @return self L'objet DisturbanceVO créé.
     */
    public static function createDisturbanceVOWithArrayFromGrandLyon(array $disturbanceFromGrandLyon): DisturbanceVO
    {
        $newInstance = new self();

        $newInstance->type = $disturbanceFromGrandLyon['type'];
        $newInstance->cause = $disturbanceFromGrandLyon['cause'];

        $firstWord = 'Le ';
        $timeDebut = strtotime($disturbanceFromGrandLyon['debut']);
        $dateDebut = $firstWord;
        $dateDebut .= date('d/m/Y à H:i', $timeDebut);
        $newInstance->debut = $dateDebut;

        $timeFin = strtotime($disturbanceFromGrandLyon['fin']);
        $dateFin = $firstWord;
        $dateFin .= date('d/m/Y à H:i', $timeFin);
        $newInstance->fin = $dateFin;

        $newInstance->mode = $disturbanceFromGrandLyon['mode'];
        $newInstance->codeLine = $disturbanceFromGrandLyon['ligne_com'];
        $newInstance->lineName = $disturbanceFromGrandLyon['ligne_cli'];
        $newInstance->titre = $disturbanceFromGrandLyon['titre'];
        $newInstance->message = $disturbanceFromGrandLyon['message'];
        $newInstance->last_update_fme = $disturbanceFromGrandLyon['last_update_fme'];
        $newInstance->typeSeverite = $disturbanceFromGrandLyon['typeseverite'];
        $newInstance->niveauSeverite = $disturbanceFromGrandLyon['niveauseverite'];
        if (is_array($disturbanceFromGrandLyon['listeobjet'])) {
            foreach ($disturbanceFromGrandLyon['listeobjet'] as $nomTransport) {
                $newInstance->addCodeLine($nomTransport);
            }
        } else {
            $newInstance->addCodeLine($disturbanceFromGrandLyon['listeobjet']);
        }

        return $newInstance;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getCause(): string
    {
        return $this->cause;
    }

    public function setCause(string $cause): void
    {
        $this->cause = $cause;
    }

    public function getDebut(): string
    {
        return $this->debut;
    }

    public function setDebut(string $debut): void
    {
        $this->debut = $debut;
    }

    public function getFin(): string
    {
        return $this->fin;
    }

    public function setFin(string $fin): void
    {
        $this->fin = $fin;
    }

    public function getMode(): string
    {
        return $this->mode;
    }

    public function setMode(string $mode): void
    {
        $this->mode = $mode;
    }

    public function getCodeLine(): string
    {
        return $this->codeLine;
    }

    public function setCodeLine(string $codeLine): void
    {
        $this->codeLine = $codeLine;
    }

    public function getLineName(): string
    {
        return $this->lineName;
    }

    public function setLineName(string $lineName): void
    {
        $this->lineName = $lineName;
    }

    public function getTitre(): string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): void
    {
        $this->titre = $titre;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function getLastUpdateFme(): string
    {
        return $this->last_update_fme;
    }

    public function setLastUpdateFme(string $last_update_fme): void
    {
        $this->last_update_fme = $last_update_fme;
    }

    public function getTypeSeverite(): string
    {
        return $this->typeSeverite;
    }

    public function setTypeSeverite(string $typeSeverite): void
    {
        $this->typeSeverite = $typeSeverite;
    }

    public function getNiveauSeverite(): int
    {
        return $this->niveauSeverite;
    }

    public function setNiveauSeverite(int $niveauSeverite): void
    {
        $this->niveauSeverite = $niveauSeverite;
    }

    public function getTypeObjet(): string
    {
        return $this->typeObjet;
    }

    public function setTypeObjet(string $typeObjet): void
    {
        $this->typeObjet = $typeObjet;
    }

    public function getCodeLines(): array
    {
        return $this->codeLines;
    }

    public function setCodeLines(array $codeLines): void
    {
        $this->codeLines = $codeLines;
    }

    public function addCodeLine(string $codeLine): void
    {
        $this->codeLines[] = $codeLine;
    }
}