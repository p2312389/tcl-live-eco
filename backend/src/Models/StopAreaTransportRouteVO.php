<?php

namespace App\Models;

class StopAreaTransportRouteVO
{
    private RoutesVO $route;
    private StopAreaVO $stopArea;
    private TransportsVO $transport;

    public function __construct(RoutesVO $route, StopAreaVO $stopAreaVO, TransportsVO $transportsVO) {
        $this->route = $route;
        $this->stopArea = $stopAreaVO;
        $this->transport = $transportsVO;
    }

    public function getRoute(): RoutesVO
    {
        return $this->route;
    }

    public function setRoute(RoutesVO $route): void
    {
        $this->route = $route;
    }

    public function getStopArea(): StopAreaVO
    {
        return $this->stopArea;
    }

    public function setStopArea(StopAreaVO $stopArea): void
    {
        $this->stopArea = $stopArea;
    }

    public function getTransport(): TransportsVO
    {
        return $this->transport;
    }

    public function setTransport(TransportsVO $transport): void
    {
        $this->transport = $transport;
    }


}