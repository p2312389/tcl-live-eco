<?php 

namespace App\Models;

use App\Entity\StopArea;
use App\Models\TransportAvecRoutesResultat;

// Définition de la classe StopAreaResulat
class StopAreaResultat {
    public StopArea $stopArea;
    public $listTransportsAvecRoutes;
    public $SARid;

    public function __construct( StopArea $stopArea) {
        $this->SARid = $stopArea->getId();
        $this->stopArea = $stopArea;
        $this->listTransportsAvecRoutes=[];
    }

    public function setStopAreaAndIdandList(StopArea $stopArea){
        $this->SARid = $stopArea->getId();
        $this->stopArea = $stopArea;
        $this->listTransportsAvecRoutes=[];
    }


    public function ajouterTransport(TransportAvecRoutesResultat $transport){
        //$TARR = new TransportAvecRoutesResulat($transport);
        $TARR = $transport;
        array_push($this->listTransportsAvecRoutes,$TARR);
        // $TARR = $transport;
        // $InList = false;
        // foreach($this->listTransportsAvecRoutes as $elementLTARR){
        //     if($elementLTARR->getTARRid()==$TARR->getTARRid()){
        //         $InList = true;
        //         break;
        //     }
        // }
        // if($InList==false){
        //     array_push($this->listTransportsAvecRoutes,$TARR);
        //     return 1;
        // }
        // return 0;
    }

    public function hasTransport($transportId)
    {
        $TARR = $transportId;
        $InList = false;
        foreach($this->listTransportsAvecRoutes as $elementLTARR){
            if($elementLTARR->getTARRid()==$TARR){
                $InList = true;
                break;
            }
        }
        return $InList;
    }

    public function getTransport($transportId)
    {
        $TARR = $transportId;
        foreach($this->listTransportsAvecRoutes as $elementLTARR){
            if($elementLTARR->getTARRid()==$TARR){
                return $elementLTARR;
            }
        }
        return null;
    }
}