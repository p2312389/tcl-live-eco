<?php

namespace App\Models;

class StopAreaAroundVO
{
    private array $stopAreaTransportRoutes;
    private string $latitude;
    private string $longitude;

    public function __construct(array $stopAreaTransportRoutes, string $latitude, string $longitude) {
        $this->stopAreaTransportRoutes = $stopAreaTransportRoutes;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    public function getStopAreaTransportRoutes(): array
    {
        return $this->stopAreaTransportRoutes;
    }

    public function setStopAreaTransportRoutes(array $stopAreaTransportRoutes): void
    {
        $this->stopAreaTransportRoutes = $stopAreaTransportRoutes;
    }

    public function getLatitude(): string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): void
    {
        $this->latitude = $latitude;
    }

    public function getLongitude(): string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): void
    {
        $this->longitude = $longitude;
    }
}