<?php

namespace App\Models;

use App\Entity\Transports;

class TransportsVO
{
    private int $id;
    private String $lineName;
    private string $codeTrace;
    private string $codeLine;
    private bool $wheelchairAccessible;
    private TypeTransportVO $typeTransport;
    private array $routes;

    public function __construct() {}

    public static function createWithTransports(Transports $transports): TransportsVO
    {
        $newInstance = new self();
        $newInstance->id = $transports->getId();
        $newInstance->lineName = $transports->getlineName();
        $newInstance->codeTrace = $transports->getCodeTrace();
        $newInstance->codeLine = $transports->getCodeLine();
        $newInstance->wheelchairAccessible = $transports->isWheelchairAccessible();
        return $newInstance;
    }

    public static function createWithTransportsWithTypeTransport(Transports $transports): TransportsVO
    {
        $newInstance = new self();
        $newInstance->id = $transports->getId();
        $newInstance->lineName = $transports->getlineName();
        $newInstance->codeTrace = $transports->getCodeTrace();
        $newInstance->codeLine = $transports->getCodeLine();
        $newInstance->wheelchairAccessible = $transports->isWheelchairAccessible();
        $newInstance->typeTransport = TypeTransportVO::createWithTypeTransport($transports->getTypeTransport());
        return $newInstance;
    }

    public static function createTransportWithRoute(Transports $transports): TransportsVO {
        $newInstance = self::createWithTransportsWithTypeTransport($transports);
        foreach ($transports->getRoutes() as $route) {
            $newInstance->routes[] = RoutesVO::createWithRoutes($route);
        }
        return $newInstance;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getLineName(): string
    {
        return $this->lineName;
    }

    public function setLineName(string $lineName): void
    {
        $this->lineName = $lineName;
    }

    public function getCodeTrace(): string
    {
        return $this->codeTrace;
    }

    public function setCodeTrace(string $codeTrace): void
    {
        $this->codeTrace = $codeTrace;
    }

    public function getCodeLine(): string
    {
        return $this->codeLine;
    }

    public function setCodeLine(string $codeLine): void
    {
        $this->codeLine = $codeLine;
    }

    public function isWheelchairAccessible(): bool
    {
        return $this->wheelchairAccessible;
    }

    public function setWheelchairAccessible(bool $wheelchairAccessible): void
    {
        $this->wheelchairAccessible = $wheelchairAccessible;
    }

    public function getTypeTransport(): TypeTransportVO
    {
        return $this->typeTransport;
    }

    public function setTypeTransport(TypeTransportVO $typeTransport): void
    {
        $this->typeTransport = $typeTransport;
    }

    public function getRoutes(): array
    {
        return $this->routes;
    }

    public function setRoutes(array $routes): void
    {
        $this->routes = $routes;
    }
}