<?php

namespace App\Exception;

use Exception;
use Throwable;

class ApiGrandLyonException extends  Exception
{
    public function __construct(string $message, int $code, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}