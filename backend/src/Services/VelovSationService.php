<?php

namespace App\Services;

use App\Exception\ApiGrandLyonException;
use App\Models\AvailabilityVelovVO;
use App\Models\VelovStationsSearch;
use App\Models\VelovVO;
use App\Repository\VelovStationRepository;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Service relatif aux stations de vélo'v.
 */
class VelovSationService
{
    private VelovStationRepository $velovStationRepository;
    private MapServices $mapServices;
    private SerializerInterface $serializer;
    private ConverterAddressService $converterAddressService;
    private ApiGrandLyonService $apiGrandLyonService;

    public function __construct(VelovStationRepository  $velovStationRepository,
                                MapServices             $mapServices,
                                SerializerInterface     $serializer,
                                ConverterAddressService $converterAddressService,
                                ApiGrandLyonService $apiGrandLyonService)
    {
        $this->velovStationRepository = $velovStationRepository;
        $this->mapServices = $mapServices;
        $this->serializer = $serializer;
        $this->converterAddressService = $converterAddressService;
        $this->apiGrandLyonService = $apiGrandLyonService;
    }

    /**
     * Récupère toutes les stations vélo'v qui sont dans la distance de l'adresse renseigné.
     *
     * @param string $street la rue de la position envoyée.
     * @param string $zipCode Le code postal de la position envoyée.
     * @param string $municipality La ville de la position envoyée.
     * @param float $distance Le périmètre dans lequel doit se trouver la station.
     * @return JsonResponse JSON contenant la liste des stations de vélo'v dans le périmètre de la position.
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ApiGrandLyonException
     */
    public function getStationVelovNearToPostion(string $street, string $zipCode, string $municipality, float $distance): JsonResponse
    {
        // Je vais récupérer toutes les stations pour les trier en fonction de leur distance
        $velovStations = $this->velovStationRepository->findAll();

        $velovStationsNearToPosition = array();

        //Appel API pour rechercher les coordonnées GPS de l'adresse renseignée en paramètre
        $resultAddress = $this->converterAddressService->convertAddressToCoordinateWithZipCode($street, $municipality, $zipCode);

        if (is_null($resultAddress)) {
            $error = [
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'Aucune adresse trouvée'
            ];

            return new JsonResponse($this->serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);
        }

        $availabilities = $this->apiGrandLyonService->getAvailabilityVelovStation(false);

        foreach ($velovStations as $velovStation) {
            $latitude = $velovStation->getAddress()->getLatitude();
            $longitude = $velovStation->getAddress()->getLongitude();
            $idStation= $velovStation->getIdStation();

            if ($this->mapServices->haversineDistance($resultAddress->getLatitude(), $resultAddress->getLongitude(), $latitude, $longitude, $distance)) {
                $currentVelovStation = VelovVO::createVelovVO($velovStation);
                if (array_key_exists($idStation, $availabilities)) {
                    $availability = $availabilities[$idStation];
                    $currentVelovStation->setAvailability($availability);
                }
                $velovStationsNearToPosition[] = $currentVelovStation;
            }
        }

        $velovStationSearch = new VelovStationsSearch($velovStationsNearToPosition, $resultAddress->getLatitude(), $resultAddress->getLongitude());

        return new JsonResponse($this->serializer->serialize($velovStationSearch, 'json'), Response::HTTP_OK, [], true);
    }
}