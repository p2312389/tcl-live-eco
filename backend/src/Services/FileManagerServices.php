<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Service pour gérer les fichiers de peuplement de la base de données.
 */
class FileManagerServices
{

    /**
     * Liste tous les fichiers d'un répertoire.
     *
     * @param string $directory Le repertoire sur lequel les fichiers doivent être listés.
     * @return JsonResponse Un JSON contenant tous les fichiers du répertoire renseigné en paramètre.
     */
    public function getFileInformation(string $directory): JsonResponse {
        if (!file_exists($directory)) {
            return new JsonResponse("Le répertoire : " . $directory . " n'existe pas.", Response::HTTP_BAD_REQUEST, [], true);
        }

        $listOfFile = scandir($directory);
        $listOfFileToReturn = array();

        // J'ajoute un ID pour les listes dans react qui nécessite une key unique
        $fileId = 1;

        foreach ($listOfFile as $nameFile) {
            // Je ne veux pas récupérer ce genre d'info
            if ($nameFile !== "." && $nameFile !== ".."){
                $pathParts = pathinfo($nameFile);

                $listOfFileInformation['id'] = $fileId;
                $fileId++;
                $listOfFileInformation['fileName'] = $pathParts['filename'];
                $listOfFileInformation['fileExtension'] = $pathParts['extension'];
                $listOfFileInformation['completeFileName'] = $nameFile;

                $listOfFileToReturn[] = $listOfFileInformation;
            }
        }

        $jsonContent = json_encode($listOfFileToReturn);

        return new JsonResponse($jsonContent, Response::HTTP_OK, [], true);
    }
}