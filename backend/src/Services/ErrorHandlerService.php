<?php

namespace App\Services;

use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Service permettant de renvoyer un json en cas d'erreur.
 */
class ErrorHandlerService
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * Formate un message d'erreur en JSON.
     *
     * @param $errorStatus : Code de l'erreur HTTP.
     * @param $errorMessage : La description de l'erreur.
     * @return JsonResponse L'erreur formatée en JSON.
     */
    public function formattedErrorMessage($errorStatus, $errorMessage): JsonResponse {
        $error = [
            'status' => $errorStatus,
            'message' => $errorMessage
        ];

        return new JsonResponse($this->serializer->serialize($error, 'json'), $errorStatus, [], true);
    }
}