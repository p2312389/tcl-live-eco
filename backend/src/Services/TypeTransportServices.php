<?php

namespace App\Services;

use App\Repository\TypeTransportsRepository;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Service relatif aux types des transports.
 */
class TypeTransportServices
{
    private TypeTransportsRepository $typeTransportsRepository;
    private SerializerInterface $serializer;

    public function __construct(TypeTransportsRepository $typeTransportsRepository, SerializerInterface $serializer) {
        $this->typeTransportsRepository = $typeTransportsRepository;
        $this->serializer = $serializer;
    }

    /**
     * Récupère tous les types de transport trouvés en base de données.
     *
     * @return JsonResponse Un JSON contenant un tableau de tous les type transport trouvés.
     */
    public function getTypeTransport(): JsonResponse
    {
        $typeTransports = $this->typeTransportsRepository->findAll();

        $context = SerializationContext::create()->setGroups(['read:typeTransport:all']);
        $jsonTransportDetail = $this->serializer->serialize($typeTransports, 'json', $context);
        return new JsonResponse($jsonTransportDetail, Response::HTTP_OK, [], true);
    }
}