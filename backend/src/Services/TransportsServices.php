<?php

namespace App\Services;

use App\Entity\Transports;
use App\Models\TransportsVO;
use App\Models\TypeTransportVO;
use App\Repository\TransportsRepository;
use App\Repository\TypeTransportsRepository;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Service relatif aux transports TCL.
 */
class TransportsServices
{
    private TransportsRepository $transportsRepository;
    private TypeTransportsRepository $typeTransportsRepository;
    private SerializerInterface $serializer;
    private ApiGrandLyonService $scheduleService;

    public function __construct(
        TransportsRepository     $transportsRepository,
        SerializerInterface      $serializer,
        TypeTransportsRepository $typeTransportsRepository,
        ApiGrandLyonService $scheduleService
    )
    {
        $this->transportsRepository = $transportsRepository;
        $this->serializer = $serializer;
        $this->typeTransportsRepository = $typeTransportsRepository;
        $this->scheduleService = $scheduleService;
    }

    /**
     * Récupère la liste des transports dont le nom de la ligne commence par le mot-clé renseigné
     *
     * @param string $keyword
     * @return JsonResponse
     */
    public function getTransportsFromKeyword(string $keyword): JsonResponse
    {

        if ($keyword === '') {
            return new JsonResponse("Aucun mot-clé renseigné", Response::HTTP_BAD_REQUEST, [], true);
        }

        $transportsMatchingWithKeyword = $this->transportsRepository->findTransportsNameWhichStartWithGivenKeyword($keyword);

        $context = SerializationContext::create()->setGroups(['read:Transport:autocomplete']);
        $jsonTransports = $this->serializer->serialize($transportsMatchingWithKeyword, 'json', $context);
        return new JsonResponse($jsonTransports, Response::HTTP_OK, [], true);
    }

    /**
     * Récupère le détail d'un transport.
     *
     * @param Transports $transport transport dont on recherche son détail.
     * @return JsonResponse Un JSON avec le détail du transport.
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getTransportDetails(Transports $transport): JsonResponse
    {
        $transportVO = TransportsVO::createTransportWithRoute($transport);

        $arrayOfScheduleFromGrandLyon = $this->scheduleService->getScheduleFromGrandLyon()->toArray();

        $listIdStopArea = [];
        foreach ($transportVO->getRoutes() as $route) {
            foreach ($route->getRouteDetail() as $routeDetail) {
                $listIdStopArea[] = intval($routeDetail->getStopArea()->getIdStopArea());
            }
        }

        foreach ($arrayOfScheduleFromGrandLyon["values"] as $value) {

            $codeLine = $value["ligne"];
            $idStopArea = $value["id"];
            $nextSchedule = $value["delaipassage"];

            if ($transportVO->getCodeLine() === $codeLine && in_array($idStopArea, $listIdStopArea)) {

                foreach ($transportVO->getRoutes() as $route) {
                    foreach ($route->getRouteDetail() as $routeDetail) {
                        if (intval($routeDetail->getStopArea()->getIdStopArea()) === $idStopArea) {
                            $routeDetail->getStopArea()->addSchedule($nextSchedule);
                        }
                    }
                }
            }
        }

        $jsonTransportDetail = $this->serializer->serialize($transportVO, 'json');
        return new JsonResponse($jsonTransportDetail, Response::HTTP_OK, [], true);
    }

    /**
     * Récupère tous les transports.
     *
     * @return JsonResponse Un JSON avec tous les transports trouvés en base de données.
     */
    public function getAllTransports(): JsonResponse
    {
        $transports = $this->transportsRepository->findAll();
        $transportsVO = array();
        foreach ($transports as $transport) {
            $transportsVO[] = TransportsVO::createWithTransportsWithTypeTransport($transport);
        }
        $jsonTransports = $this->serializer->serialize($transportsVO, 'json');
        return new JsonResponse($jsonTransports, Response::HTTP_OK, [], true);
    }

    /**
     * Récupère tous les transports listés par type de transport.
     *
     * @return JsonResponse Un JSON avec tous les transports listés par leur type de transport.
     */
    public function getAllTransportsFromTypeTransport(): JsonResponse
    {
        $typeTransports = $this->typeTransportsRepository->findAll();
        $typeTransportVO = array();

        foreach ($typeTransports as $typeTransport) {
            $typeTransportVO[] = TypeTransportVO::createWithTypeTransportWithTransport($typeTransport);
        }
        $jsonTypeTransports = $this->serializer->serialize($typeTransportVO, 'json');
        return new JsonResponse($jsonTypeTransports, Response::HTTP_OK, [], true);
    }

    /**
     * Recherche un transport via son nom de transport et son type de transport.
     *
     * @param string $transportName Le nom du transport recherché.
     * @param array $idTypeTransport L'id du type du transport recherché.
     * @return JsonResponse Un JSON avec tous les transport trouvés.
     */
    public function searchTransport(string $transportName, array $idTypeTransport): JsonResponse
    {
        $transports = $this->transportsRepository->searchTransportWithNameAndTypeTransport($transportName, $idTypeTransport);
        $transportsVO = array();
        foreach ($transports as $transport) {
            $transportsVO[] = TransportsVO::createWithTransportsWithTypeTransport($transport);
        }

        $jsonTransports = $this->serializer->serialize($transportsVO, 'json');
        return new JsonResponse($jsonTransports, Response::HTTP_OK, [], true);
    }
}