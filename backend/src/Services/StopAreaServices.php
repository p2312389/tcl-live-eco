<?php

namespace App\Services;

use App\Models\StopAreaDetailVO;
use App\Models\TransportsVO;
use App\Repository\RoutesRepository;
use App\Repository\StopAreaRepository;
use App\Repository\TransportsRepository;

use App\Models\AddressVO;
use App\Models\RoutesVO;
use App\Models\StopAreaAroundVO;
use App\Models\StopAreaTransportRouteVO;
use App\Models\StopAreaVO;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;

use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Service relatif aux stations TCL
 */
class StopAreaServices
{
    private SerializerInterface $serializer;
    private ConverterAddressService $converterAddress;
    private EntityManagerInterface $entityManager;
    private TransportsRepository $transportsRepository;
    private StopAreaRepository $stopAreaRepository;
    private RoutesRepository $routesRepository;
    private ErrorHandlerService $errorHandler;
    private ApiGrandLyonService $scheduleService;
    private MapServices $mapServices;

    public function __construct(
        SerializerInterface     $serializer,
        ConverterAddressService $ConverterAddress,
        EntityManagerInterface  $entityManager,
        TransportsRepository    $transportsRepository,
        StopAreaRepository      $stopAreaRepository,
        RoutesRepository        $routesRepository,
        ErrorHandlerService     $errorHandler,
        ApiGrandLyonService     $scheduleService,
        MapServices             $mapServices
    )
    {
        $this->serializer = $serializer;
        $this->converterAddress = $ConverterAddress;
        $this->entityManager = $entityManager;
        $this->transportsRepository = $transportsRepository;
        $this->stopAreaRepository = $stopAreaRepository;
        $this->routesRepository = $routesRepository;
        $this->errorHandler = $errorHandler;
        $this->scheduleService = $scheduleService;
        $this->mapServices = $mapServices;
    }

    /**
     * Récupère le détail d'une station lié à un transport et une route
     *
     * @param int $idStopArea L'id de la station.
     * @param int $idTransport L'id du transport.
     * @param int $idRoutes L'id de la route.
     * @return JsonResponse Un JSON avec le détail d'une station lié à un transport et une route
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getDetailStopArea(int $idStopArea, int $idTransport, int $idRoutes): JsonResponse
    {
        $transport = $this->transportsRepository->find($idTransport);
        $stopArea = $this->stopAreaRepository->find($idStopArea);
        $routes = $this->routesRepository->find($idRoutes);

        if (is_null($transport) || is_null($stopArea) || is_null($routes)) {
            $errorMessage = ' idStopArea : ' . $idStopArea . ', idTransport : ' . $idTransport . ', idRoutes : ' . $idRoutes;
            return $this->errorHandler->formattedErrorMessage(Response::HTTP_INTERNAL_SERVER_ERROR,
                'Impossible de trouver les infos relatives aux ID renseignés' . $errorMessage);
        }

        $stopAreaDetail = new StopAreaDetailVO($transport, $stopArea, $routes);

        $arrayOfScheduleFromGrandLyon = $this->scheduleService->getScheduleFromGrandLyon()->toArray();

        $listIdStopArea = [];
        foreach ($stopAreaDetail->getRoute()->getRouteDetail() as $routeDetail) {
            $listIdStopArea[] = intval($routeDetail->getStopArea()->getIdStopArea());
        }

        foreach ($arrayOfScheduleFromGrandLyon["values"] as $value) {

            $codeLine = $value["ligne"];
            $idStopArea = $value["id"];
            $nextSchedule = $value["delaipassage"];

            if ($stopAreaDetail->getTransport()->getCodeLine() === $codeLine && in_array($idStopArea, $listIdStopArea)) {

                // Je veux seulement remplir le tableau du point d'arret courant
                if ($idStopArea === intval($stopAreaDetail->getStopArea()->getIdStopArea())) {
                    $stopAreaDetail->addSchedule($nextSchedule);
                }
                $stopAreaDetail->setDateLastUpdate($value["last_update_fme"]);

                foreach ($stopAreaDetail->getRoute()->getRouteDetail() as $routeDetail) {
                    if (intval($routeDetail->getStopArea()->getIdStopArea()) === $idStopArea) {
                        $routeDetail->getStopArea()->addSchedule($nextSchedule);
                    }
                }
            }
        }

        $jsonDetailStopArea = $this->serializer->serialize($stopAreaDetail, 'json');
        return new JsonResponse($jsonDetailStopArea, Response::HTTP_OK, [], true);
    }

    /**
     * Donne les stopArea qui sont au maximum à dist metres de distance de l'adresse donnée en parametres
     * Et qui sont du type renseigné (ou pas)
     *
     * @param $streetRequest
     * @param $municipalityRequest
     * @param $zipCodeRequest
     * @param $distanceRequest
     * @param $typeTransportsRequest
     * @return JsonResponse Contenant soit une liste des stop avec les tranports et routesassociés soit une messahe d'erreur
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getStopAreaProches(
        $streetRequest,
        $municipalityRequest,
        $zipCodeRequest,
        $distanceRequest,
        $typeTransportsRequest
    ): JsonResponse
    {
        //Appel API pour convertir les champs d'addresse en coordonnées
        $resultatAddress = $this->converterAddress->convertAddressToCoordinateWithZipCode($streetRequest, $municipalityRequest, $zipCodeRequest);
        if (is_null($resultatAddress)) {
            $error = [
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'Aucune adresse trouvée'
            ];

            return new JsonResponse($this->serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);

        }

        $whereConditionIdTypeTransports = "";
        if (count($typeTransportsRequest) > 0) {
            $whereConditionIdTypeTransports = 'AND ( T.type_id = ' . $typeTransportsRequest[0];
            for ($indexArrayIdTypeTransports = 1; $indexArrayIdTypeTransports < count($typeTransportsRequest); $indexArrayIdTypeTransports++) {
                $whereConditionIdTypeTransports .= ' OR T.type_id = ' . $typeTransportsRequest[$indexArrayIdTypeTransports];
            }
            $whereConditionIdTypeTransports .= ' )';
        }

        $distanceRequete = number_format((float)$distanceRequest, 10, '.', '');

        $sql = 'SELECT S.id as Sid, S.name as Sname, S.wheelchair_accessible as Swheelchair_accessible, ' .
            'S.lift as Slift, S.escalator as Sescalator,S.lift as Slift,S.address_id as Saddress_id, ' .
            'S.stop_area_id as Sstop_area_id, S.id_stop_area as Sid_stop_area, '
            . 'A.latitude as Alat ,A.longitude as Along ,'

            . 'R.id as Rid, R.name as Rname, R.complete_name as Rcomplete_name, ' .
            'R.transports_id as Rtransports_id, R.departure_id as Rdeparture_id, R.terminal_id as Rterminal_id, ' .
            'R.direction as Rdirection, '

            . 'T.id as Tid, T.line_name as Tline_name, T.code_trace as Tcode_trace, '
            . 'T.code_line as Tcode_line, T.wheelchair_accessible as Twheelchair_accessible, '
            . 'T.type_id as Ttype_id '

            . 'FROM stop_area S JOIN address A ON S.address_id = A.id '
            . ' JOIN route_detail RD ON RD.stop_area_id = S.id '
            . ' JOIN routes R ON R.id = RD.routes_id '
            . ' JOIN transports T ON T.id = R.transports_id '
            . ' WHERE  S.id_stop_area NOT LIKE \'S%\' AND A.longitude IS NOT NULL AND A.latitude IS NOT NULL ' . $whereConditionIdTypeTransports
            . '  ORDER BY S.id,T.id';

        //Pour récuperer les colonnes qu'on veut
        $resultsetmapping = new ResultSetMapping();
        $aliases = [
            'Sid', 'Sname', 'Swheelchair_accessible', 'Slift', 'Sescalator', 'Saddress_id', 'Sstop_area_id', 'Sid_stop_area',
            'Alat', 'Along',
            'Rid', 'Rname', 'Rcomplete_name', 'Rtransports_id', 'Rdeparture_id', 'Rterminal_id', 'Rdirection',
            'Tid', 'Tline_name', 'Tcode_trace', 'Tcode_line', 'Twheelchair_accessible', 'Ttype_id'
        ];
        foreach ($aliases as $alias) {
            $resultsetmapping->addScalarResult($alias, $alias);
        }

        $query = $this->entityManager->createNativeQuery($sql, $resultsetmapping);

        $result = $query->getArrayResult();

        // tableau associatif pour stocker les StopAreaResultat par ID StopArea
        $stopAreas = [];
        $latRequete = $resultatAddress->getLatitude();
        $longRequete = $resultatAddress->getLongitude();

        $stopAreaTransportRouteVO = [];

        $transportIdStopAreaID = [];

        foreach ($result as $row) {
            $stopAreaId = $row['Sid'];
            $transportId = $row['Tid'];

            //calcul distance et filtrage
            $dansRayon = $this->mapServices->haversineDistance($latRequete, $longRequete, $row['Alat'], $row['Along'], $distanceRequete);
            if ($dansRayon) {

                // Si le lieux correspond au terminus alors pas besoin d'afficher la ligne car l'utilisateur veux allé à un autre endroit
                if ($row['Rterminal_id'] != $stopAreaId) {
                    // Comme il existe des duplicatas de route (complete_name et direction) dû aux horaires, je veux pas avoir des doublons
                    if (!isset($transportIdStopAreaID[$stopAreaId . '-' . $transportId]) || ($stopAreaTransportRouteVO[$transportIdStopAreaID[$stopAreaId . '-' . $transportId]]->getRoute()->getCompleteName() !== $row['Rcomplete_name'] && $stopAreaTransportRouteVO[$transportIdStopAreaID[$stopAreaId . '-' . $transportId]]->getRoute()->getDirection() !== $row['Rdirection'])) {

                        $stopAreaVO = new StopAreaVO();
                        $addressVO = new AddressVO();

                        $addressVO->setLatitude($row['Alat']);
                        $addressVO->setLongitude($row['Along']);

                        $stopAreaVO->setId($stopAreaId);
                        $stopAreaVO->setName($row['Sname']);
                        $stopAreaVO->setWheelchairAccessible($row['Swheelchair_accessible']);
                        $stopAreaVO->setLift($row['Slift']);
                        $stopAreaVO->setEscalator($row['Sescalator']);
                        $stopAreaVO->setAddress($addressVO);

                        $transportVO = new TransportsVO();

                        $transportVO->setId($transportId);
                        $transportVO->setLineName($row['Tline_name']);
                        $transportVO->setCodeTrace($row['Tcode_trace']);
                        $transportVO->setCodeLine($row['Tcode_line']);
                        $transportVO->setWheelchairAccessible($row['Twheelchair_accessible']);

                        $routeVO = new RoutesVO();

                        $routeVO->setId($row['Rid']);
                        $routeVO->setName($row['Rname']);
                        $routeVO->setCompleteName($row['Rcomplete_name']);
                        $routeVO->setDirection($row['Rdirection']);

                        $currentStopAreaTransportRouteVO = new StopAreaTransportRouteVO($routeVO, $stopAreaVO, $transportVO);
                        $currentIndex = array_push($stopAreaTransportRouteVO, $currentStopAreaTransportRouteVO);
                        $transportIdStopAreaID[$stopAreaId . '-' . $transportId] = $currentIndex - 1;

                    }
                }
            }
        }

        $stopAreaAroundVO = new StopAreaAroundVO($stopAreaTransportRouteVO, $latRequete, $longRequete);

        return new JsonResponse(
            $this->serializer->serialize($stopAreaAroundVO, 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }
}