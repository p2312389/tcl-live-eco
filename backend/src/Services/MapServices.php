<?php

namespace App\Services;

use App\Models\Location;

/**
 * Service pour la gestion des informations relatives aux cartes affichées dans le frontend
 */
class MapServices
{

    /**
     * Récupère la distance en KM entre deux points GPS.
     *
     * @param Location $fromLocation Le point GPS de départ.
     * @param Location $toLocation Le point GPS d'arrivé.
     * @return float La distance en KM entre les deux points GPS.
     */
    public function getDistanceBetweenGPSCoordinate(Location $fromLocation, Location $toLocation): float {
        $longFrom = deg2rad($fromLocation->getLongitude());
        $longTo = deg2rad($toLocation->getLongitude());
        $latFrom = deg2rad($fromLocation->getLatitude());
        $latTo = deg2rad($toLocation->getLatitude());

        $val = acos(sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($longTo-$longFrom)) * 6371;

        return ($val);
    }

    /**
     * Calcule la distance entre deux coordonnées données.
     *
     * @param string $latA : Latitude du point GPS de départ.
     * @param string $lonA : Longitude du point GPS de départ.
     * @param string $latB : Latitude du point GPS d'arrivé.
     * @param string $lonB : Longitude du point GPS d'arrivé.
     * @param float $distanceM : Distance à respecter entre les deux points GPS.
     * @return bool vrai si les 2 coordonnées sont inferieures ou égales à la distance renseignée.
     */
    public function haversineDistance(string $latA, string $lonA, string $latB, string $lonB, float $distanceM): bool
    {
        $lat1 = number_format((float)$latA, 6, '.', '');
        $lon1 = number_format((float)$lonA, 6, '.', '');
        $lat2 = number_format((float)$latB, 6, '.', '');
        $lon2 = number_format((float)$lonB, 6, '.', '');
        // Convertir les degrés en radians
        $lat1c = deg2rad($lat1);
        $lon1c = deg2rad($lon1);
        $lat2c = deg2rad($lat2);
        $lon2c = deg2rad($lon2);
        // Calculer la différence de latitude et de longitude
        $dlat = $lat2c - $lat1c;
        $dlon = $lon2c - $lon1c;

        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1c) * cos($lat2c) * sin($dlon / 2) * sin($dlon / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        // Rayon de la Terre en mètres (environ 6371 km)
        $radius = 6367445; //6371000;

        // Calculer la distance puis on vérifie qu'elle est inférieure ou égale à la distance passée en parametres
        return $radius * $c <= $distanceM;
    }
}