<?php

namespace App\Services;

use App\Exception\ApiGrandLyonException;
use App\Models\AvailabilityVelovVO;
use App\Models\DisturbanceVO;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Service pour faire les appels à l'API du grand Lyon.
 */
class ApiGrandLyonService
{
    private HttpClientInterface $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * Récupère tous les prochain passage de tous les transports TCL.
     *
     * @return ResponseInterface La réponse renvoyée par l'API TCL.
     * @throws TransportExceptionInterface
     */
    public function getScheduleFromGrandLyon(): ResponseInterface
    {
        $URL = 'https://data.grandlyon.com/fr/datapusher/ws/rdata/tcl_sytral.tclpassagearret/all.json?maxfeatures=-1&start=1';

        $authorizationHeader = $this->getAuthorizationHeader();

        return $this->httpClient->request(
            'GET',
            $URL,
            ['headers' => ['Authorization' => $authorizationHeader]]
        );
    }

    /**
     * Récupère toutes les perturbations de tous les transports sur le réseau TCL.
     *
     * @return array Un tableau de perturbations.
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws ApiGrandLyonException
     */
    public function getDisturbanceFromGrandLyon(bool $throwExceptionIfError): array
    {
        $URL = 'https://data.grandlyon.com/fr/datapusher/ws/rdata/tcl_sytral.tclalertetrafic_2/all.json?maxfeatures=-1&start=1';

        $response = $this->sendHTTPRequestTOGrandLyon($URL, $throwExceptionIfError);

        $arrayResponse = $response->toArray();
        $disturbanceVOs = array();

        foreach ($arrayResponse['values'] as $value) {
            $disturbanceVO = DisturbanceVO::createDisturbanceVOWithArrayFromGrandLyon($value);
            $disturbanceVOs[] = $disturbanceVO;
        }

        return $disturbanceVOs;
    }

    /**
     * Récupère toutes les disponibilités de toutes les stations de vélo'v.
     *
     * @param bool $throwExceptionIfError
     * @return array Un tableau de disponibilité des stations de vélo'v.
     * @throws ApiGrandLyonException
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getAvailabilityVelovStation(bool $throwExceptionIfError): array
    {
        $URL = 'https://data.grandlyon.com/fr/datapusher/ws/rdata/jcd_jcdecaux.jcdvelov/all.json?maxfeatures=-1&start=1';

        $response = $this->sendHTTPRequestTOGrandLyon($URL, $throwExceptionIfError);

        $arrayResponse = $response->toArray();
        $availibilityVelovVOs = array();

        foreach ($arrayResponse['values'] as $value) {
            $idStation = $value['number'];
            $availibilityVelovVO = new AvailabilityVelovVO($value);
            if (!array_key_exists($idStation, $availibilityVelovVOs)) {
                $availibilityVelovVOs[$idStation] = $availibilityVelovVO;
            }
        }

        return $availibilityVelovVOs;
    }

    /**
     * Fabrique le header authorization nécessaire à l'authentification de l'API du grand Lyon
     *
     * @return string Le header authorization nécessaire pour s'authentifier et utiliser l'API du grand Lyon
     */
    private function getAuthorizationHeader(): string
    {
        $username = $_ENV['USERNAME_GRAND_LYON'];
        $password = $_ENV['PASSWORD_GRAND_LYON'];
        $loginAndPasswordBase64Encode = base64_encode($username . ':' . $password);
        return 'Basic ' . $loginAndPasswordBase64Encode;
    }

    /**
     * Envoi une requête HTTP à l'API du grand Lyon.
     *
     * @param string $url L'url de la requête HTTP.
     * @param bool $throwExceptionIfError Précise si une exception doit être levé en cas d'erreur provenant de l'API du grand Lyon.
     * @return ResponseInterface La réponse renvoyée par l'API du grand Lyon.
     * @throws ApiGrandLyonException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function sendHTTPRequestTOGrandLyon(string $url, bool $throwExceptionIfError): ResponseInterface
    {
        $authorizationHeader = $this->getAuthorizationHeader();

        $response = $this->httpClient->request(
            'GET',
            $url,
            ['headers' => ['Authorization' => $authorizationHeader]]
        );

        // S'il y a eu une erreur alors un tableau vide est renvoyé sauf si booléen qui demande de renvoyer une erreur est à vrai.
        if ($response->getStatusCode() !== 200) {
            if ($throwExceptionIfError) {
                throw new ApiGrandLyonException($response->getContent(), $response->getStatusCode());
            } else {
                return $response;
            }
        }

        return $response;
    }
}