<?php

namespace App\Services;

use App\Repository\AddressRepository;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Service pour traiter les informations relatives aux adresses.
 */
class AddressService
{
    private SerializerInterface $serializer;
    private AddressRepository $addressRepository;

    public function __construct(
        SerializerInterface $serializer,
        AddressRepository $addressRepository
    ) {
        $this->serializer = $serializer;
        $this->addressRepository = $addressRepository;
    }

    /**
     * Recherche des noms de rue depuis un mot-clé.
     *
     * @param string $keyword Le mot-clé pour rechercher des rues.
     * @return JsonResponse Un json composé d'un tableau d'adresse trouvée.
     */
    public function getStreetFromKeyword(string $keyword): JsonResponse
    {
        if ($keyword === '') {
            return new JsonResponse("Aucun mot-clé renseigné", Response::HTTP_BAD_REQUEST, [], true);
        }

        $streetMatchingWithKeyword = $this->addressRepository->findStreetWhichStartWithGivenKeyword($keyword);

        $context = SerializationContext::create()->setGroups(['read:Address:autocompleteStreet']);
        $jsonAddress = $this->serializer->serialize($streetMatchingWithKeyword, 'json', $context);
        return new JsonResponse($jsonAddress, Response::HTTP_OK, [], true);
    }

    /**
     * Recherche des codes postaux depuis un mot-clé.
     *
     * @param string $keyword Le mot-clé pour rechercher des codes postaux.
     * @return JsonResponse Un json composé d'un tableau d'adresse trouvée.
     */
    public function getZipcodeFromKeyword(string $keyword): JsonResponse
    {
        if ($keyword === '') {
            return new JsonResponse("Aucun mot-clé renseigné", Response::HTTP_BAD_REQUEST, [], true);
        }

        $addressMatchingWithKeyword = $this->addressRepository->findZipcodeWhichStartWithGivenKeyword($keyword);

        $context = SerializationContext::create()->setGroups(['read:Address:autocompleteZipcode']);
        $jsonAddress = $this->serializer->serialize($addressMatchingWithKeyword, 'json', $context);
        return new JsonResponse($jsonAddress, Response::HTTP_OK, [], true);
    }

    /**
     * Recherche une ville depuis un mot-clé.
     *
     * @param string $keyword Le mot-clé pour rechercher des villes.
     * @return JsonResponse Un json composé d'un tableau d'adresse trouvée.
     */
    public function getCityFromKeyword(string $keyword): JsonResponse
    {
        if ($keyword === '') {
            return new JsonResponse("Aucun mot-clé renseigné", Response::HTTP_BAD_REQUEST, [], true);
        }

        $addressMatchingWithKeyword = $this->addressRepository->findCityWhichStartWithGivenKeyword($keyword);

        $context = SerializationContext::create()->setGroups(['read:Address:autocompleteCity']);
        $jsonAddress = $this->serializer->serialize($addressMatchingWithKeyword, 'json', $context);
        return new JsonResponse($jsonAddress, Response::HTTP_OK, [], true);
    }
}