<?php

namespace App\Services\xmlObject;

class StopAreaXml extends xmlObject
{
    private string $name;
    private array $listStopPointId;
    private string $areaCentroidId;
    private bool $wheelchairAccess;

    public function __construct(array $xmlData)
    {
        parent::__construct($xmlData);

        $this->name = $xmlData["name"];
        $this->areaCentroidId = $this->extractID($xmlData["centroidOfArea"]);

        // La section "contains" dans le XML comporte plusieurs occurrence alors j'ai un tableau sinon j'ai une chaine
        // Si j'ai une chaine alors je vais la convertir en tableau pour simplifier le traitement
        foreach ($this->handleListOfItem("contains", $xmlData) as $contains) {
            $this->listStopPointId[] = $this->extractID($contains);
        }

        $this->wheelchairAccess = $xmlData["StopAreaExtension"]["mobilityRestrictedSuitability"];

    }

    public function isParent(): bool {
        // Rappel : les parents ont des 'S' dans leur ID.
        // cf fichier XML
        if (str_contains($this->getId(), 'S')) {
            return true;
        }

        return false;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getListStopPointId(): array
    {
        return $this->listStopPointId;
    }

    public function setListStopPointId(array $listStopPointId): void
    {
        $this->listStopPointId = $listStopPointId;
    }

    public function addItemToListStopPoint(string $stopPointId): void {
        if (!in_array($stopPointId, $this->listStopPointId)) {
            $this->listStopPointId[] = $stopPointId;
        }
    }

    public function getAreaCentroidId(): string
    {
        return $this->areaCentroidId;
    }

    public function setAreaCentroidId(string $areaCentroid): void
    {
        $this->areaCentroidId = $areaCentroid;
    }

    public function isWheelchairAccess(): bool
    {
        return $this->wheelchairAccess;
    }

    public function setWheelchairAccess(bool $wheelchairAccess): void
    {
        $this->wheelchairAccess = $wheelchairAccess;
    }


}