<?php

namespace App\Services\xmlObject;

class LineXml extends xmlObject
{
    private string $name;
    private string $number;
    private string $transportModeName;
    private array $listRouteId;
    private bool $wheelchairAccess;

    public function __construct($xmlData)
    {
        parent::__construct($xmlData);
        $this->name = $xmlData["name"];
        $this->number = $xmlData["number"];
        $this->transportModeName = $xmlData["transportModeName"];

        foreach ($this->handleListOfItem("routeId", $xmlData) as $routeId) {
            $this->listRouteId[] = $this->extractID($routeId);
        }
        $this->wheelchairAccess = $xmlData["LineExtension"]["mobilityRestrictedSuitability"];
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getTransportModeName(): string
    {
        return $this->transportModeName;
    }

    public function getListRouteId(): array
    {
        return $this->listRouteId;
    }

    public function isWheelchairAccess(): bool
    {
        return $this->wheelchairAccess;
    }

}