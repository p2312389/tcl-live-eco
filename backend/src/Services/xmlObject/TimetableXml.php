<?php

namespace App\Services\xmlObject;

class TimetableXml extends xmlObject
{
    private ?\DateTimeInterface $startOfPeriod;
    private ?\DateTimeInterface $endOfPeriod;
    private array $listOfDay;
    private array $listVehicleJourneyId;

    public function __construct($xmlData)
    {
        parent::__construct($xmlData);
        if ($xmlData["period"]["startOfPeriod"] === "") {
            $this->startOfPeriod = null;
        } else {
            $startDate = new \DateTime($xmlData["period"]["startOfPeriod"]);
            $this->startOfPeriod = $startDate;
        }
        if ($xmlData["period"]["endOfPeriod"] === "") {
            $this->endOfPeriod = null;
        } else {
            $startDate = new \DateTime($xmlData["period"]["endOfPeriod"]);
            $this->endOfPeriod = $startDate;
        }

        foreach ($this->handleListOfItem("dayType", $xmlData) as $item) {
            $this->listOfDay[] = $item;
        }

        foreach ($this->handleListOfItem("vehicleJourneyId", $xmlData) as $item) {
            $this->listVehicleJourneyId[] = $this->extractID($item);
        }

    }

    public function getStartOfPeriod(): ?\DateTimeInterface
    {
        return $this->startOfPeriod;
    }

    public function getEndOfPeriod(): ?\DateTimeInterface
    {
        return $this->endOfPeriod;
    }

    public function getListOfDay(): array
    {
        return $this->listOfDay;
    }

    public function getListVehicleJourneyId(): array
    {
        return $this->listVehicleJourneyId;
    }
}