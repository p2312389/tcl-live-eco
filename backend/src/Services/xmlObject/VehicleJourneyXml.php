<?php

namespace App\Services\xmlObject;

class VehicleJourneyXml extends xmlObject
{
    private string $routeId;
    private string $journeyPatternId;
     private array $listVehicleJourneyAtStop;

    public function __construct($xmlData)
    {
        parent::__construct($xmlData);
        $this->journeyPatternId = $xmlData["journeyPatternId"];
        $this->routeId = $this->extractID($xmlData["routeId"]);

        foreach ($this->handleListOfItem("vehicleJourneyAtStop", $xmlData) as $vehicleJourneyAtStop) {
            $vehicleJourneyAtStop = new VehicleJourneyAtStopXml($vehicleJourneyAtStop);
            $this->listVehicleJourneyAtStop[] = $vehicleJourneyAtStop;
        }
    }

    public function getRouteId(): string
    {
        return $this->routeId;
    }

    public function getJourneyPatternId(): string
    {
        return $this->journeyPatternId;
    }

    public function getListVehicleJourneyAtStop(): array
    {
        return $this->listVehicleJourneyAtStop;
    }
}