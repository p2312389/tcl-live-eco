<?php

namespace App\Services\xmlObject;

class JourneyPatternXml extends xmlObject
{
    private string $name;
    private string $routeId;
    private array $listStopPointId;

    public function __construct($xmlData)
    {
        parent::__construct($xmlData);
        $this->name = $xmlData["name"];
        $this->routeId = $xmlData["routeId"];

        foreach ($this->handleListOfItem("stopPointList", $xmlData) as $item) {
            $this->listStopPointId[] = $this->extractID($item);
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getRouteId(): string
    {
        return $this->routeId;
    }

    public function getListStopPointId(): array
    {
        return $this->listStopPointId;
    }

}