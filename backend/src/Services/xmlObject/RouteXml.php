<?php

namespace App\Services\xmlObject;

class RouteXml extends xmlObject
{
    private string $name;
    private string $publishedName;
    private string $direction;
    private array $listPtLinkId;
    private array $journeyPatternId;

    public function __construct($xmlData)
    {
        parent::__construct($xmlData);
        $this->name = $xmlData["name"];
        $this->publishedName = $xmlData["publishedName"];
        $this->direction = $xmlData["direction"];

        foreach ($this->handleListOfItem("ptLinkId", $xmlData) as $item) {
            $this->listPtLinkId[] = $this->extractID($item);
        }

        foreach ($this->handleListOfItem("journeyPatternId", $xmlData) as $item) {
            $this->journeyPatternId[] = $this->extractID($item);
        }
    }

    public function extractLineCodeFromIdRoute(): string {
        // Si la direction est l'allé alors je recherche le caractère a
        if ($this->getDirection() === 'A') {
            $characterToFind = 'a';
        } else {
            // sinon c'est r pour retour
            $characterToFind = 'r';
        }
        $positionOfSeparator = strpos($this->getId(), $characterToFind);

        // Pour rappel l'idRoute est composé comme ceci C9Ar2 (code de la ligne + sens a=aller ou r=retour + numéro de route)
        // Donc je commence au début jusqu'a la direction
        return substr($this->getId(), 0, $positionOfSeparator);
    }

    public function extractStartAndEndStopArea(): array
    {
        // un ID PtLink se compose de la sorte : 302Aa16_30472A46022
        // 302Aa16 correspond à l'id de la route
        // 30472 l'id du premier arret
        // 46022 l'ID du deuxième arret du trajet
        // Attention une route est composé de plusieurs trajet !!!!!!!

        // Le premier arrêt de la route se trouve dans le premier PtLink
        $firstPtLinkId = $this->listPtLinkId[0];
        $lastPtLinkId = end($this->listPtLinkId);

        //Comme il peut y a voir des A dans l'ID de la route je skip l'ID de la route
        $recudeCharacterFirstLink = strpos($firstPtLinkId, '_') + 1;
        $firstPtLinkId = substr($firstPtLinkId, $recudeCharacterFirstLink);

        //Comme il peut y a voir des A dans l'ID de la route je skip l'ID de la route
        $recudeCharacterLastLink = strpos($lastPtLinkId, '_') + 1;
        $lastPtLinkId = substr($lastPtLinkId, $recudeCharacterLastLink);

        // +1 car je veux pas récupérer le caractère '_'
        $startCharacterFirstLink = 0;
        $endCharacterFirstLink = strpos($firstPtLinkId, 'A');

        // +1 pour par récupérer le caractère 'A'
        $startCharacterLastLink = strpos($lastPtLinkId, 'A') + 1;

        // il faut soustraire sinon je récupère plus de caractère que prévu
        $startAndEndStopArea["start"] = substr($firstPtLinkId, $startCharacterFirstLink, $endCharacterFirstLink - $startCharacterFirstLink);
        // Pas de length pour avoir le reste de la chaine
        $startAndEndStopArea["end"] = substr($lastPtLinkId, $startCharacterLastLink);

        return $startAndEndStopArea;
    }

    public function extractFirstAreaFromGivenLinkId(string $linkId): string
    {
        // +1 car je veux pas récupérer le caractère '_'
        $startCharacterOfLinkId = strpos($linkId, '_') + 1;
        $linkId = substr($linkId, $startCharacterOfLinkId);
        $endCharacterOfLinkId = strpos($linkId, 'A');

        // il faut soustraire sinon je récupère plus de caractère que prévu
        return substr($linkId, 0, $endCharacterOfLinkId);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPublishedName(): string
    {
        return $this->publishedName;
    }

    public function getDirection(): string
    {
        return $this->direction;
    }

    public function getListPtLinkId(): array
    {
        return $this->listPtLinkId;
    }

    public function getJourneyPatternId(): string
    {
        return $this->journeyPatternId;
    }

}