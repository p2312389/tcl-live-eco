<?php

namespace App\Services\xmlObject;

class AreaCentroidXml extends xmlObject
{
    private string $longitude;
    private string $latitude;
    private string $stopAreaId;
    private string $name;

    public function __construct($xmlData)
    {
        parent::__construct($xmlData);
        $this->name = $xmlData["name"];
        $this->longitude = $xmlData["longitude"];
        $this->latitude = $xmlData["latitude"];
        $this->stopAreaId = $this->extractID($xmlData["containedIn"]);
    }

    public function getLongitude(): string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): void
    {
        $this->longitude = $longitude;
    }

    public function getLatitude(): string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): void
    {
        $this->latitude = $latitude;
    }

    public function getStopAreaId(): string
    {
        return $this->stopAreaId;
    }

    public function setStopAreaId(string $stopAreaId): void
    {
        $this->stopAreaId = $stopAreaId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }


}