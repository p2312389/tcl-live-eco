<?php

namespace App\Services\xmlObject;

class XmlObjectHandler
{
    private array $listAreaCentroid;
    private array $listJouerneyPattern;
    private LineXml $lineXml;
    private array $listPtLink;
    private array $listRoute;
    private array $listStopArea;
    private array $listStopPoint;
    private array $listTimetable;
    private array $listVehicleJourney;

    public function getListAreaCentroid(): array
    {
        return $this->listAreaCentroid;
    }

    public function getListJouerneyPattern(): array
    {
        return $this->listJouerneyPattern;
    }

    public function getLineXml(): LineXml
    {
        return $this->lineXml;
    }

    public function setLineXml(LineXml $lineXml): void
    {
        $this->lineXml = $lineXml;
    }

    public function getListPtLink(): array
    {
        return $this->listPtLink;
    }

    public function getListRoute(): array
    {
        return $this->listRoute;
    }

    public function getListStopArea(): array
    {
        return $this->listStopArea;
    }

    public function getListStopPoint(): array
    {
        return $this->listStopPoint;
    }

    public function getListTimetable(): array
    {
        return $this->listTimetable;
    }

    public function getListVehicleJourney(): array
    {
        return $this->listVehicleJourney;
    }

    public function addToListAreaCentroid(AreaCentroidXml $areaCentroidXml): void {
        $this->listAreaCentroid[$areaCentroidXml->getId()] = $areaCentroidXml;
    }
    public function addToListJourneyPattern(JourneyPatternXml $journeyPatternXml): void {
        $this->listJouerneyPattern[$journeyPatternXml->getId()] = $journeyPatternXml;
    }
    public function addToListPtLink(PtLinkXml $linkXml): void {
        $this->listPtLink[$linkXml->getId()] = $linkXml;
    }
    public function addToListRoute(RouteXml $routeXml): void {
        $this->listRoute[$routeXml->getId()] = $routeXml;
    }
    public function addToListStopArea(StopAreaXml $stopAreaXml): void {
        $this->listStopArea[$stopAreaXml->getId()] = $stopAreaXml;
    }
    public function addToListStopPoint(StopPointXml $stopPointXml): void {
        $this->listStopPoint[$stopPointXml->getId()] = $stopPointXml;
    }
    public function addToListTimetable(TimetableXml $timetableXml): void {
        $this->listTimetable[$timetableXml->getId()] = $timetableXml;
    }
    public function addToListVehicleJourney(VehicleJourneyXml $vehicleJourneyXml): void {
        $this->listVehicleJourney[$vehicleJourneyXml->getId()] = $vehicleJourneyXml;
    }
}