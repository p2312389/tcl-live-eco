<?php

namespace App\Services\xmlObject;

class StopPointXml extends xmlObject
{
    private string $longitude;
    private string $latitude;
    private string $stopAreaId;
    private string $name;

    public function __construct($xmlData)
    {
        parent::__construct($xmlData);
        $this->name = $xmlData["name"];
        $this->longitude = $xmlData["longitude"];
        $this->latitude = $xmlData["latitude"];
        $this->stopAreaId = $xmlData["containedIn"];
    }

    public function getLongitude(): string
    {
        return $this->longitude;
    }

    public function getLatitude(): string
    {
        return $this->latitude;
    }

    public function getStopAreaId(): string
    {
        return $this->stopAreaId;
    }

    public function getName(): string
    {
        return $this->name;
    }

}