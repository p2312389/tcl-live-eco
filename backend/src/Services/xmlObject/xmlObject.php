<?php

namespace App\Services\xmlObject;

class xmlObject
{
    protected string $id;

    public function __construct(array $xmlData) {
        $this->extractIDAndSetIt($xmlData["objectId"]);
    }

    /**
     * Dans le XML si j'ai plusieurs tag alors c'est regroupé dans un tableau
     * Sinon une chaine, je veux pouvoir convertir en tableau la chaine pour simplifier le traitement
     * @param string $nameOfField
     * @param $arrayConcern
     * @return array
     */
    public function handleListOfItem(string $nameOfField, $arrayConcern): array {
        if (is_array($arrayConcern[$nameOfField])) {
            return $arrayConcern[$nameOfField];
        } else {
            $newArray[] = $arrayConcern[$nameOfField];
            return $newArray;
        }
    }

    /**
     * Permet de set directement l'ID depuis une chaine du type TCL:StopArea:35256
     * @param string $stringContainsId
     * @return void
     */
    protected function extractIDAndSetIt(string $stringContainsId): void {
        // $stringContainsId est sous le format "TCL:StopArea:35256"

        $idSubstring = explode(':', $stringContainsId);
        $this->setId($idSubstring[2]);
    }

    /**
     * Permet de récupérer un ID depuis une chaine du type TCL:StopArea:35256
     * @param string $stringContainsId
     * @return string
     */
    protected function extractID(string $stringContainsId): string {
        // $stringContainsId est sous le format "TCL:StopArea:35256"

        $idSubstring = explode(':', $stringContainsId);
        return $idSubstring[2];
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }


}