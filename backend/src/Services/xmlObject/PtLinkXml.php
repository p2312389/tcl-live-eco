<?php

namespace App\Services\xmlObject;

class PtLinkXml extends xmlObject
{
    private string $startStopPointId;
    private string $endStopPointId;

    public function __construct($xmlData)
    {
        parent::__construct($xmlData);
        $this->startStopPointId = $xmlData["startOfLink"];
        $this->endStopPointId = $xmlData["endOfLink"];
    }

    public function getStartStopPointId(): string
    {
        return $this->startStopPointId;
    }

    public function getEndStopPointId(): string
    {
        return $this->endStopPointId;
    }

}