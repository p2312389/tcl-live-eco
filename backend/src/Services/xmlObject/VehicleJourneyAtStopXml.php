<?php

namespace App\Services\xmlObject;

class VehicleJourneyAtStopXml
{
    private string $stopPointId;
    private \DateTimeInterface $arrivalTime;
    private \DateTimeInterface $departureTime;
    private int $order;

    public function __construct($xmlData)
    {
        $this->stopPointId = $xmlData["stopPointId"];

        $arrivalTime = date_create_from_format('H:i:s', $xmlData["arrivalTime"]);
        $this->arrivalTime = $arrivalTime;

        $departureTime = date_create_from_format('H:i:s', $xmlData["departureTime"]);
        $this->departureTime = $departureTime;

        $this->order = $xmlData["order"];
    }

    public function extractIdStopAreaFromStopPoint(): string {

        $idStopPoint = $this->getStopPointId();

        // +1 pour ne pas avoir le séparateur
        $positionOfSeparator = strpos($idStopPoint, '_') + 1;
        return substr($idStopPoint, $positionOfSeparator);
    }

    public function getStopPointId(): string
    {
        return $this->stopPointId;
    }

    public function getArrivalTime(): \DateTimeInterface
    {
        return $this->arrivalTime;
    }

    public function getDepartureTime(): \DateTimeInterface
    {
        return $this->departureTime;
    }

    public function getOrder(): int
    {
        return $this->order;
    }
}