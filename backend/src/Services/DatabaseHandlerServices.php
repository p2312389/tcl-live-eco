<?php

namespace App\Services;

use App\Entity\Address;
use App\Entity\RouteDetail;
use App\Entity\Routes;
use App\Entity\Schedule;
use App\Entity\StopArea;
use App\Entity\Timetable;
use App\Entity\Transports;
use App\Entity\TypeTransports;
use App\Entity\VelovStation;
use App\Repository\AddressRepository;
use App\Repository\RouteDetailRepository;
use App\Repository\RoutesRepository;
use App\Repository\ScheduleRepository;
use App\Repository\StopAreaRepository;
use App\Repository\TimetableRepository;
use App\Repository\TransportsRepository;
use App\Repository\TypeTransportsRepository;
use App\Repository\VelovStationRepository;
use App\Services\xmlObject\AreaCentroidXml;
use App\Services\xmlObject\JourneyPatternXml;
use App\Services\xmlObject\LineXml;
use App\Services\xmlObject\PtLinkXml;
use App\Services\xmlObject\RouteXml;
use App\Services\xmlObject\StopAreaXml;
use App\Services\xmlObject\StopPointXml;
use App\Services\xmlObject\TimetableXml;
use App\Services\xmlObject\VehicleJourneyXml;
use App\Services\xmlObject\XmlObjectHandler;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Json;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DatabaseHandlerServices
{
    private SerializerInterface $serializer;
    // Permet de renvoyer une erreur lors d'un traitement qui a echoué
    private string $errorMessage = "";
    private EntityManagerInterface $entityManager;
    private bool $reachEndOfFile = false;
    // Permet de savoir quand je recherche les horaires pour limiter le nombre d'enregistrement car dans certain fichier
    // il y en a bcp trop
    private bool $scheduleHandlerCall = false;
    private ErrorHandlerService $errorHandler;
    private TypeTransportsRepository $typeTransportsRepository;
    private TransportsRepository $transportsRepository;
    private StopAreaRepository $stopAreaRepository;
    private AddressRepository $addressRepository;
    private TimetableRepository $timetableRepository;
    private RoutesRepository $routesRepository;
    private RouteDetailRepository $routeDetailRepository;
    private ScheduleRepository $scheduleRepository;
    private VelovStationRepository $velovStationRepository;

    public function __construct(
        SerializerInterface      $serializer,
        EntityManagerInterface   $entityManager,
        ErrorHandlerService      $errorHandler,
        TypeTransportsRepository $typeTransportsRepository,
        TransportsRepository     $transportsRepository,
        StopAreaRepository       $stopAreaRepository,
        AddressRepository        $addressRepository,
        TimetableRepository      $timetableRepository,
        RoutesRepository         $routesRepository,
        RouteDetailRepository    $routeDetailRepository,
        ScheduleRepository       $scheduleRepository,
        VelovStationRepository   $velovStationRepository
    )
    {
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
        $this->errorHandler = $errorHandler;
        $this->typeTransportsRepository = $typeTransportsRepository;
        $this->transportsRepository = $transportsRepository;
        $this->stopAreaRepository = $stopAreaRepository;
        $this->addressRepository = $addressRepository;
        $this->timetableRepository = $timetableRepository;
        $this->routesRepository = $routesRepository;
        $this->routeDetailRepository = $routeDetailRepository;
        $this->scheduleRepository = $scheduleRepository;
        $this->velovStationRepository = $velovStationRepository;
    }

    public function transportHandler($labelTypeTransport, string $filePath, int $startIndex, int $endIndex): JsonResponse
    {
        $jsonResponseForCheckingFile = $this->checkFile($filePath);
        if ($jsonResponseForCheckingFile->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseForCheckingFile;

        $typeTransport = $this->typeTransportsRepository->findOneBy(['label' => $labelTypeTransport]);

        if (is_null($typeTransport)) {
            return $this->errorHandler->formattedErrorMessage(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                "Le type de transport avec le label : " . $labelTypeTransport . " n'existe pas en base."
            );
        }

        $transportData = $this->extractDataToCsv("code_ligne", $filePath, $startIndex, $endIndex);
        if ($this->errorMessage !== "") {
            return $this->errorHandler->formattedErrorMessage($this->serializer,
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $this->errorMessage);
        }

        $transportList = $this->convertCsvDataToTransport($transportData, $typeTransport);
        $this->insertOrUpdateTransport($transportList);

        return $this->generateResponseWithStartAndEndIndexWhenSuccess($startIndex, $endIndex);
    }

    public function stopAreaHandler(string $filePath, int $startIndex, int $endIndex): JsonResponse
    {
        $jsonResponseForCheckingFile = $this->checkFile($filePath);
        if ($jsonResponseForCheckingFile->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseForCheckingFile;

        $stopAreaAndAddress = $this->extractDataToCsv("id", $filePath, $startIndex, $endIndex);
        $jsonResponseForCheckingError = $this->checkError();
        if ($jsonResponseForCheckingError->getStatusCode() === Response::HTTP_INTERNAL_SERVER_ERROR) return $jsonResponseForCheckingError;

        // Je veux d'abord insérer les adresses
        // Je vais extraire les adresses et sauvergarder l'ID de la station

        $addressList = $this->convertCsvDataToAddress($stopAreaAndAddress);
        $stopAreaList = $this->convertCsvDataToStopArea($stopAreaAndAddress);

        $listIdStationAddressUpdate = $this->insertOrUpdateAddress($addressList);
        $jsonResponseForCheckingError = $this->checkError();
        if ($jsonResponseForCheckingError->getStatusCode() === Response::HTTP_INTERNAL_SERVER_ERROR) return $jsonResponseForCheckingError;

        $this->insertOrUpdateStopArea($stopAreaList, $listIdStationAddressUpdate);

        return $this->generateResponseWithStartAndEndIndexWhenSuccess($startIndex, $endIndex);
    }

    public function timetableHandler(string $filePath): JsonResponse
    {
        $jsonResponseForCheckingFile = $this->checkFile($filePath);
        if ($jsonResponseForCheckingFile->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseForCheckingFile;

        $xmlObjectHandler = $this->getDataFromXml($filePath);

        $this->insertOrUpdateTimetable($xmlObjectHandler->getListTimetable());
        return new JsonResponse($this->serializer->serialize("Les infos relatives au calendrier ont bien été créées", 'json'), Response::HTTP_CREATED, [], true);
    }

    public function stopAreaAddressFromXmlHandler(string $filePath): JsonResponse
    {
        $jsonResponseForCheckingFile = $this->checkFile($filePath);
        if ($jsonResponseForCheckingFile->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseForCheckingFile;

        $xmlObjectHandler = $this->getDataFromXml($filePath);
        //return new JsonResponse($this->serializer->serialize($xmlObjectHandler->getListAreaCentroid(), 'json'), Response::HTTP_OK, [], true);
        $this->insertOrUpdateStopAreaAddressFromXml($xmlObjectHandler->getListStopArea(), $xmlObjectHandler->getListAreaCentroid());
        $jsonResponseForCheckingError = $this->checkError();
        if ($jsonResponseForCheckingError->getStatusCode() === Response::HTTP_INTERNAL_SERVER_ERROR) return $jsonResponseForCheckingError;

        return new JsonResponse($this->serializer->serialize("Les infos relatives aux stations ont bien été créées", 'json'), Response::HTTP_CREATED, [], true);
    }

    public function routesHandler(string $filePath): JsonResponse
    {
        $jsonResponseForCheckingFile = $this->checkFile($filePath);
        if ($jsonResponseForCheckingFile->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseForCheckingFile;

        $xmlObjectHandler = $this->getDataFromXml($filePath);

        $this->insertOrUpdateRoutes($xmlObjectHandler->getListRoute());

        $jsonResponseForCheckingError = $this->checkError();
        if ($jsonResponseForCheckingError->getStatusCode() === Response::HTTP_INTERNAL_SERVER_ERROR) return $jsonResponseForCheckingError;

        return new JsonResponse($this->serializer->serialize("Les infos relatives aux routes ont bien été créées", 'json'), Response::HTTP_CREATED, [], true);
    }

    public function routeDetailHandler(string $filePath): JsonResponse
    {
        $jsonResponseForCheckingFile = $this->checkFile($filePath);
        if ($jsonResponseForCheckingFile->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseForCheckingFile;

        $xmlObjectHandler = $this->getDataFromXml($filePath);

        $this->insertOrUpdaterouteDetail($xmlObjectHandler->getListRoute());
        $jsonResponseForCheckingError = $this->checkError();
        if ($jsonResponseForCheckingError->getStatusCode() === Response::HTTP_INTERNAL_SERVER_ERROR) return $jsonResponseForCheckingError;

        return new JsonResponse($this->serializer->serialize("Les infos relatives aux détails des routes ont bien été créées", 'json'), Response::HTTP_CREATED, [], true);
    }

    public function scheduleHandler(string $filePath, int $startIndex, int $endIndex): JsonResponse
    {
        $jsonResponseForCheckingFile = $this->checkFile($filePath);
        if ($jsonResponseForCheckingFile->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseForCheckingFile;

        $this->scheduleHandlerCall = true;

        $xmlObjectHandler = $this->getDataFromXml($filePath, $startIndex, $endIndex);

        $this->insertOrUpdateSchedule($xmlObjectHandler->getListVehicleJourney(), $xmlObjectHandler->getListTimetable());
        $jsonResponseForCheckingError = $this->checkError();
        if ($jsonResponseForCheckingError->getStatusCode() === Response::HTTP_INTERNAL_SERVER_ERROR) return $jsonResponseForCheckingError;

        // return new JsonResponse($this->serializer->serialize("Les infos relatives aux horaires ont bien été créées", 'json'), Response::HTTP_CREATED, [], true);

        return $this->generateResponseWithStartAndEndIndexWhenSuccess($startIndex, $endIndex);
    }

    public function checkFile(string $filePath): JsonResponse
    {
        if (!file_exists($filePath)) {
            return $this->errorHandler->formattedErrorMessage(
                Response::HTTP_BAD_REQUEST,
                "Le fichier " . $filePath . " n'existe pas.");
        }

        return new JsonResponse("", Response::HTTP_OK, [], true);
    }

    public function checkError(): JsonResponse
    {
        if ($this->errorMessage !== "") {
            return $this->errorHandler->formattedErrorMessage(
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $this->errorMessage);
        }

        return new JsonResponse("", Response::HTTP_OK, [], true);
    }

    public function generateResponseWithStartAndEndIndexWhenSuccess(int $startIndex, int $endIndex): JsonResponse
    {
        // Si j'ai parcouru tous mon fichier alors je renvoi 0 pour les deux index pour notifier qu'on a tout parcouru
        $nextStartIndex = 0;
        $nextEndIndex = 0;

        if (!$this->reachEndOfFile) {
            $nextStartIndex = $endIndex + 1;
            // Je récupère mon pas + le prochain index de début pour avoir mon index de fin
            $nextEndIndex = $endIndex - $startIndex + $nextStartIndex;
        }

        $responseToReturn['nextStartIndex'] = $nextStartIndex;
        $responseToReturn['nextEndIndex'] = $nextEndIndex;

        return new JsonResponse($this->serializer->serialize($responseToReturn, 'json'), Response::HTTP_CREATED, [], true);
    }

    public function insertVelovStationInDatabase($filePath): JsonResponse
    {
        $dataJSON = file_get_contents($filePath);
        $velovStations = (array) json_decode($dataJSON);
        foreach ($velovStations['values'] as $velovStation) {
            $currentAddress = new Address();
            $currentAddress->setStreet($velovStation->{'adresse1'});
            $currentAddress->setZipCode($velovStation->{'code_insee'});
            $currentAddress->setMunicipality($velovStation->{'commune'});
            $currentAddress->setLatitude($velovStation->{'lat'});
            $currentAddress->setLongitude($velovStation->{'lon'});

            $addressExist = $this->addressRepository->findAddressWithLongitudeLatitude($currentAddress->getLongitude(), $currentAddress->getLatitude());

            $idStationVelov = $velovStation->{'idstation'};

            $velovStationExist = $this->velovStationRepository->findOneBy(['idStation' => $idStationVelov]);

            if (is_null($velovStationExist)) {
                $currentVelovStation = new VelovStation();
                $currentVelovStation->setNom($velovStation->{'nom'});
                $currentVelovStation->setOuverte($velovStation->{'ouverte'});
                $currentVelovStation->setNbBornettes($velovStation->{'nbbornettes'});
                $currentVelovStation->setIdStation($velovStation->{'idstation'});
                if (count($addressExist) > 0) {
                    // Je récupère la première car si j'en ai plusieurs elles ont toutes les même coordonnées GPS
                    $currentVelovStation->setAddress($addressExist[0]);
                } else {
                    $currentVelovStation->setAddress($currentAddress);
                }

                $this->entityManager->persist($currentVelovStation);
            }

            $this->entityManager->flush();
        }

        return new JsonResponse("Les stations des velo'v ont bien été enregistrées.", Response::HTTP_CREATED, [], true);
    }

    private function extractDataToCsv(string $nameOfIdField, string $filePath, int $startIndex, int $endIndex): array
    {
        $listOfData = $fields = array();
        $handle = @fopen($filePath, "r");
        $fileIterator = 0;

        if ($handle) {
            // je définit la length à 0 pour ne pas avoir de restriction sur la longueur et ainsi manquer des données
            while (($row = fgetcsv($handle, 0, ";")) !== false && $fileIterator < $endIndex) {

                // Comme la première ligne du csv comporte les rubriques si mon tableau de field est vide alors je n'ai
                // pas encore parcouru la première ligne du csv
                if (empty($fields)) {
                    // La première ligne du fichier comporte le caractère : \ufeff qui est un byte order mark (BOM)
                    // Je le retire pour avoir une clé valable
                    $row = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row);

                    // Pour rappel la fonction "fgetcsv" retourne un tableau indexé
                    // Je récupère donc ce tableau
                    $fields = $row;
                    continue;
                }

                // je récupère l'index de mon champ qui sert d'ID
                $indexWithIdField = array_search($nameOfIdField, $fields);
                $currentFieldId = $row[$indexWithIdField];

                $fileIterator++;

                if ($fileIterator < $startIndex) {
                    continue;
                }

                // je parcours mon tableau avec l'index et la valeur associé
                foreach ($row as $indexRow => $value) {

                    $listOfData[$currentFieldId][$fields[$indexRow]] = $value;
                }
            }

            // Si on est arrivé à la fin alors j'enregistre pour ajuster ma réponse
            if (feof($handle)) {
                $this->reachEndOfFile = true;
            }

            // Si on est pas à la fin du fichier alors il y a eu une erreur durant le parcours
            // et qu'on a pas dépassé l'index de fin
            if (!feof($handle) && $fileIterator < $endIndex) {
                $this->errorMessage = "Error: unexpected fgets() fail";
                return [];
            }
            fclose($handle);
        }
        return $listOfData;
    }

    private function convertCsvDataToTransport($transportData, $typeTransport): ArrayCollection
    {
        $listTransport = new ArrayCollection();

        if (count($transportData) > 0) {

            //foreach($arrayTransport as $ligne => $transport)
            foreach ($transportData as $codeLigne => $transport) {
                $currentTransport = new Transports();

                $currentTransport->setLineName($transport["ligne"]);
                $currentTransport->setCodeTrace($transport["code_trace"]);
                $currentTransport->setCodeLine($codeLigne);
                $currentTransport->setWheelchairAccessible($transport["pmr"]);

                $currentTransport->setTypeTransport($typeTransport);

                $listTransport->add($currentTransport);
            }
        }

        return $listTransport;
    }

    private function convertCsvDataToStopArea($listStation): ArrayCollection
    {
        $listStationObject = new arrayCollection();

        if (count($listStation) > 0) {
            foreach ($listStation as $idStation => $station) {
                $currentStation = new StopArea();

                $currentStation->setIdStopArea($idStation);
                $currentStation->setName($station["nom"]);
                $currentStation->setLift($station["ascenseur"]);
                $currentStation->setEscalator($station["escalator"]);
                $currentStation->setWheelchairAccessible($station["pmr"]);

                $listStationObject->add($currentStation);
            }
        }
        return $listStationObject;
    }

    // Je renvoi un array car j'ai besoin d'un tableau associatif
    private function convertCsvDataToAddress($addressData): array
    {
        $listAddress = array();

        foreach ($addressData as $idStation => $address) {
            $currentAddress = new Address();
            $currentAddress->setStreet($address["adresse"]);
            $currentAddress->setMunicipality($address["commune"]);
            $currentAddress->setZipCode($address["insee"]);

            $listAddress[$idStation] = $currentAddress;

        }

        return $listAddress;
    }

    private function insertOrUpdateTransport($transportCollection): void
    {

        // Comme j'effectue le flush une fois la boucle terminé pour des raisons de performance
        // Je vais enregistrer tous les transports déjà créé dans un tableau
        $listTransportAlreadyInsert = array(); // $listTransportAlreadyInsert[CodeLine]
        foreach ($transportCollection as $transport) {
            $codeLine = $transport->getCodeLine();
            // Doublon présent dans le fichier car on a les routes avec les transports donc aller et retour
            if (!array_key_exists($codeLine, $listTransportAlreadyInsert)) {
                $findTransport = $this->transportsRepository->findOneBy(['codeLine' => $codeLine]);
                if (is_null($findTransport)) {
                    $this->entityManager->persist($transport);
                    $listTransportAlreadyInsert[$codeLine] = $transport;
                }
            }
        }

        $this->entityManager->flush();
    }

    private function insertOrUpdateStopArea($listStopArea, $listIdStationAddressUpdate): void
    {
        foreach ($listStopArea as $stopArea) {
            $idStopArea = $stopArea->getIdStopArea();
            $idAddress = $listIdStationAddressUpdate[$idStopArea];
            $addressForStation = $this->addressRepository->find($idAddress);
            $stopArea->setAddress($addressForStation);

            $findStopArea = $this->stopAreaRepository->findOneBy(['idStopArea' => $idStopArea]);

            if (is_null($findStopArea)) {
                $this->entityManager->persist($stopArea);
            }
        }

        $this->entityManager->flush();
    }

    private function insertOrUpdateAddress($listAddress): array
    {
        $listIdStationAddressUpdate = array();

        foreach ($listAddress as $idStation => $address) {
            $street = $address->getStreet();
            $zipCode = $address->getZipCode();
            $municipality = $address->getMunicipality();
            $findListAddress = $this->addressRepository->findAddressWithStreetZipCodeMunicipality($street, $zipCode, $municipality);

            $numberOfSameAddress = count($findListAddress);

            if (empty($findListAddress)) {

                // --> Prend beaucoup trop de temps on les ajouteras plus tard
                /*
                // Je le fais ici car ci l'adresse existe déjà pas besoin de faire un appel API pour rien
                $addressConverter = new ConverterAddressService($httpClient, $this->serializer);
                $addressResult = $addressConverter->convertAddressToCoordinate($address->getStreet(),$address->getMunicipality());

                if(!is_null($addressResult)){
                    $address->setLatitude($addressResult->getLatitude());
                    $address->setLongitude($addressResult->getLongitude());
                }
                 */

                $this->entityManager->persist($address);
                $this->entityManager->flush();
                // A voir si je suis obligé de flush pour récupérer l'ID
                $listIdStationAddressUpdate[$idStation] = $address->getId();
            } elseif ($numberOfSameAddress === 1) {
                // Si j'ai une occurrence alors pas de soucis mais je n'ajoute pas mon adresse
                $listIdStationAddressUpdate[$idStation] = $findListAddress[0]->getId();
            } else {
                // Si j'ai plus d'un enregistrement alors j'ai un soucis et je renvoie une erreur pour prévenir et ainsi modifier la base
                $this->errorMessage = "Il existe " . $numberOfSameAddress
                    . " enregistrements avec la rue : " . $street
                    . " le code postal : " . $zipCode
                    . " et la ville : " . $municipality . " veuillez supprimer des enregistrements avant de relancer le traitement";
                return [];
            }
        }


        return $listIdStationAddressUpdate;
    }

    private function getDataFromXml(string $filePath, int $startIndex = -1, int $endIndex = -1): XmlObjectHandler
    {

        $xmlObjectHandler = new XmlObjectHandler();

        // Comme j'ai certain fichier xml qui sont très lourd je ne peux pas me permettre de stocker des infos dans des variables
        // Soit je vide les variables une fois qu'elles ne sont plus utilisé
        // soit je fais tout en un traitement
        $xmlArray = json_decode(json_encode(simplexml_load_string(file_get_contents($filePath))), true);

        foreach ($xmlArray as $xmlContentkey => $content) {
            switch ($xmlContentkey) {

                case "PTNetwork":
                    break;

                // Stations
                case "ChouetteArea":
                    if (array_key_exists("StopArea", $content)) {
                        foreach ($content["StopArea"] as $currentStation) {
                            $newStopArea = new StopAreaXml($currentStation);
                            $xmlObjectHandler->addToListStopArea($newStopArea);

                        }
                    }
                    if (array_key_exists("AreaCentroid", $content)) {
                        foreach ($content["AreaCentroid"] as $currentAddress) {

                            $newAreaCentroid = new AreaCentroidXml($currentAddress);
                            $xmlObjectHandler->addToListAreaCentroid($newAreaCentroid);

                        }
                    }
                    break;

                // Calendrier
                case "Timetable":
                    foreach ($content as $currentTimetable) {

                        $newTimetable = new TimetableXml($currentTimetable);
                        $xmlObjectHandler->addToListTimetable($newTimetable);

                    }
                    break;


                // Info relative à la ligne
                case "ChouetteLineDescription":
                    foreach ($content as $lineDescriptionKey => $lineDescription) {
                        switch ($lineDescriptionKey) {

                            case "Line":

                                // Obliger de passer par un tableau temporaire sinon si je n'ai qu'un seul élément dans mon tableau
                                // le foreach va parcourir toutes les clés une à une
                                $tmpArray = array();
                                $tmpArray[] = $content["Line"];
                                foreach ($tmpArray as $currentTransport) {

                                    $newLine = new LineXml($currentTransport);
                                    $xmlObjectHandler->setLineXml($newLine);

                                }
                                break;

                            case "ChouetteRoute":
                                foreach ($lineDescription as $currentRoute) {

                                    $newRoute = new RouteXml($currentRoute);
                                    $xmlObjectHandler->addToListRoute($newRoute);

                                }
                                break;

                            case "StopPoint":
                                foreach ($lineDescription as $currentStopPoint) {

                                    $newStopPoint = new StopPointXml($currentStopPoint);
                                    $xmlObjectHandler->addToListStopPoint($newStopPoint);

                                }
                                break;

                            case "PtLink":
                                foreach ($lineDescription as $currentConnection) {

                                    $newPtLink = new PtLinkXml($currentConnection);
                                    $xmlObjectHandler->addToListPtLink($newPtLink);

                                }
                                break;

                            case "JourneyPattern":
                                foreach ($lineDescription as $currentJourneyPattern) {

                                    $newJourneyPattern = new JourneyPatternXml($currentJourneyPattern);
                                    $xmlObjectHandler->addToListJourneyPattern($newJourneyPattern);

                                }
                                break;

                            case "VehicleJourney":
                                // 12142 max
                                // 1600

                                if ($this->scheduleHandlerCall) {
                                    $scheduleIterator = 0;
                                    foreach ($lineDescription as $currentSchedule) {
                                        $scheduleIterator++;

                                        if ($scheduleIterator < $startIndex) {
                                            continue;
                                        }
                                        if ($scheduleIterator > $endIndex) {
                                            break;
                                        }
                                        $newVehicleJourney = new VehicleJourneyXml($currentSchedule);
                                        $xmlObjectHandler->addToListVehicleJourney($newVehicleJourney);

                                    }

                                    if ($scheduleIterator <= $endIndex) {
                                        $this->reachEndOfFile = true;
                                    }

                                    break;
                                }
                        }
                    }
                    break;
            }
        }

        return $xmlObjectHandler;
    }

    private function insertOrUpdateTimetable(array $listTimetableXml): void
    {
        foreach ($listTimetableXml as $idTimetable => $timetableXml) {

            $findExistingTimetable = $this->timetableRepository->findOneBy(['idTimetable' => $timetableXml->getId()]);

            if (is_null($findExistingTimetable)) {

                $newTimetable = new Timetable();
                $newTimetable->setStartOfPeriod($timetableXml->getStartOfPeriod());
                $newTimetable->setEndOfPeriod($timetableXml->getEndOfPeriod());
                $newTimetable->setIdTimetable($timetableXml->getId());

                $listOfDay = $timetableXml->getListOfDay();

                // Attention les colonnes de jours ne peuvent pas être null !!! donc il faut mettre à false si pas dans la liste
                in_array("Monday", $listOfDay,) ? $newTimetable->setMonday(true) : $newTimetable->setMonday(false);
                in_array("Tuesday", $listOfDay,) ? $newTimetable->setTuesday(true) : $newTimetable->setTuesday(false);
                in_array("Wednesday", $listOfDay,) ? $newTimetable->setWednesday(true) : $newTimetable->setWednesday(false);
                in_array("Thursday", $listOfDay,) ? $newTimetable->setThursday(true) : $newTimetable->setThursday(false);
                in_array("Friday", $listOfDay,) ? $newTimetable->setFriday(true) : $newTimetable->setFriday(false);
                in_array("Saturday", $listOfDay,) ? $newTimetable->setSaturday(true) : $newTimetable->setSaturday(false);
                in_array("Sunday", $listOfDay,) ? $newTimetable->setSunday(true) : $newTimetable->setSunday(false);

                $this->entityManager->persist($newTimetable);
            }
        }

        $this->entityManager->flush();
    }

    private function insertOrUpdateStopAreaAddressFromXml(array $listStopAreaXml, array $listAreaCentroidXml): void
    {
        // $listIdParentToListChild[$idParentStopArea] = [$idChildStopArea1, $idChildStopArea2, ..., $idChildStopAreaN]
        $listIdParentToListChild = array();

        // j'insere seulement les parents pour le moment
        foreach ($listStopAreaXml as $idStopArea => $stopAreaXml) {
            if ($stopAreaXml->isParent()) {
                $listIdParentToListChild[$idStopArea] = $stopAreaXml->getListStopPointId();

                $findExistingParentStopArea = $this->stopAreaRepository->findOneBy(['idStopArea' => $idStopArea]);
                if (is_null($findExistingParentStopArea)) {

                    $newStopAreaParent = new StopArea();

                    $areaCentroidXml = $listAreaCentroidXml[$stopAreaXml->getAreaCentroidId()];
                    $longitude = $areaCentroidXml->getLongitude();
                    $latitude = $areaCentroidXml->getLatitude();

                    $findExistingAddress = $this->addressRepository->findAddressWithLongitudeLatitude($longitude, $latitude);

                    if (count($findExistingAddress) === 0) {
                        $newAddress = new Address();
                        $newAddress->setLongitude($longitude);
                        $newAddress->setLatitude($latitude);

                        $this->entityManager->persist($newAddress);
                        $this->entityManager->flush();
                        $newStopAreaParent->setAddress($newAddress);

                    } elseif (count($findExistingAddress) === 1) {
                        $newStopAreaParent->setAddress($findExistingAddress[0]);
                    } else {
                        $this->errorMessage = "Impossible d'ajouter la sation n° " . $idStopArea .
                            " car il y a des doublons d'adresse pour la longitude : " .
                            $longitude . " et la latitude : " . $latitude .
                            ". Veuillez corriger la BDD";
                        return;
                    }

                    $newStopAreaParent->setName($stopAreaXml->getName());
                    $newStopAreaParent->setWheelchairAccessible($stopAreaXml->isWheelchairAccess());
                    $newStopAreaParent->setIdStopArea($idStopArea);

                    $this->entityManager->persist($newStopAreaParent);
                }
            }
        }

        $this->entityManager->flush();

        // Une fois les parents insérer il va falloir mettre à jour les enfants pour 2 points :
        // - Ajouter un parent
        // - Ajouter les coordonnées GPS à leur l'adresse
        foreach ($listIdParentToListChild as $idStopAreaParent => $listIdstopAreaChild) {

            $stopAreaParent = $this->stopAreaRepository->findOneBy(['idStopArea' => $idStopAreaParent]);

            foreach ($listIdstopAreaChild as $idStopAreaChild) {
                $stopAreaChild = $this->stopAreaRepository->findOneBy(['idStopArea' => $idStopAreaChild]);


                if (is_null($stopAreaChild->getStopArea())) {
                    $stopAreaChild->setStopArea($stopAreaParent);
                    $this->entityManager->persist($stopAreaChild);
                }

                $areaCentroidXml = $listAreaCentroidXml[$listStopAreaXml[$idStopAreaChild]->getAreaCentroidId()];
                $longitudeXml = $areaCentroidXml->getLongitude();
                $latitudeXml = $areaCentroidXml->getLatitude();

                $address = $this->addressRepository->find($stopAreaChild->getAddress()->getId());
                // Si pas les mêmes coordonnées GPS en base alors j'update
                if (is_null($address->getLongitude()) || is_null($address->getLatitude()) || $address->getLongitude() !== $longitudeXml || $address->getLatitude() !== $latitudeXml) {
                    $address->setLongitude($longitudeXml);
                    $address->setLatitude($latitudeXml);

                    $this->entityManager->persist($address);
                }
            }
        }

        $this->entityManager->flush();
    }

    private function insertOrUpdateRoutes(array $listRoutesXml): void
    {
        foreach ($listRoutesXml as $idRoute => $routeXml) {

            $findExistingRoute = $this->routesRepository->findOneBy(['routeId' => $idRoute]);

            if (is_null($findExistingRoute)) {
                $lineCode = $routeXml->extractLineCodeFromIdRoute();
                $startAndEndStopArea = $routeXml->extractStartAndEndStopArea();

                $idStartStopArea = $startAndEndStopArea["start"];
                $idEndStopArea = $startAndEndStopArea["end"];

                $transport = $this->transportsRepository->findOneBy(['codeLine' => $lineCode]);

                // Je bloque pas mais je passe au suivant
                if (is_null($transport)) {
                    if ($this->errorMessage !== "") $this->errorMessage .= "\n";
                    $this->errorMessage .= "Impossible d'enregister la route : " . $idRoute . " car le transport : " .
                        $lineCode . " n'existe pas en base.";
                    continue;
                }

                $departureStopArea = $this->stopAreaRepository->findOneBy(['idStopArea' => $idStartStopArea]);

                // Je bloque pas mais je passe au suivant
                if (is_null($departureStopArea)) {
                    if ($this->errorMessage !== "") $this->errorMessage .= " ";
                    $this->errorMessage .= "Impossible d'enregister la route : " . $idRoute . " car le point d'arrêt de départ : " .
                        $idStartStopArea . " n'existe pas en base.";
                    continue;
                }

                $terminalStopArea = $this->stopAreaRepository->findOneBy(['idStopArea' => $idEndStopArea]);

                // Je bloque pas mais je passe au suivant
                if (is_null($terminalStopArea)) {
                    if ($this->errorMessage !== "") $this->errorMessage .= " ";
                    $this->errorMessage .= "Impossible d'enregister la route : " . $idRoute . " car le point d'arrêt de retour : " .
                        $idEndStopArea . " n'existe pas en base.";
                    continue;
                }

                $newRoute = new Routes();

                $newRoute->setName($routeXml->getName());
                $newRoute->setCompleteName($routeXml->getPublishedName());
                $newRoute->setDeparture($departureStopArea);
                $newRoute->setTerminal($terminalStopArea);
                $newRoute->setDirection($routeXml->getDirection());
                $newRoute->setRouteId($idRoute);
                $newRoute->setTransports($transport);

                $this->entityManager->persist($newRoute);
            }
        }

        $this->entityManager->flush();
    }

    private function insertOrUpdaterouteDetail(array $listRoutesXml): void
    {
        foreach ($listRoutesXml as $idRoute => $routeXml) {
            $route = $this->routesRepository->findOneBy(['routeId' => $idRoute]);

            if (is_null($route)) {
                if ($this->errorMessage !== "") $this->errorMessage .= " ";
                $this->errorMessage .= "La route : " . $idRoute . " n'existe pas en base.";
                continue;
            }

            // juste besoin du terminus car avec la fonction extractFirstAreaFromGivenLinkId
            // Je récupère seulement le premier point d'arrêt de chaque trajet et donc pas le terminus
            $arrivalStopArea = $route->getTerminal();

            $routeDetailOrder = 1;
            foreach ($routeXml->getListPtLinkId() as $linkId) {
                $idStopArea = $routeXml->extractFirstAreaFromGivenLinkId($linkId);
                $stopArea = $this->stopAreaRepository->findOneBy(['idStopArea' => $idStopArea]);

                if (is_null($stopArea)) {
                    if ($this->errorMessage !== "") $this->errorMessage .= " ";
                    $this->errorMessage .= "La station : " . $idStopArea . " n'existe pas en base.";
                    break;
                }

                $newRouteDetail = new RouteDetail();
                $newRouteDetail->setRoutes($route);
                $newRouteDetail->setRouteDetailOrder($routeDetailOrder);
                $newRouteDetail->setStopArea($stopArea);

                $this->entityManager->persist($newRouteDetail);
                $routeDetailOrder++;
            }

            // J'ajoute le terminus
            $newRouteDetail = new RouteDetail();
            $newRouteDetail->setRoutes($route);
            $newRouteDetail->setRouteDetailOrder($routeDetailOrder);
            $newRouteDetail->setStopArea($arrivalStopArea);
            $this->entityManager->persist($newRouteDetail);

            $this->entityManager->flush();
        }
    }

    private function insertOrUpdateSchedule(array $listVehicleJourney, array $listTimetable): void
    {

        // On va d'abord récupérer pour chaque ID de vehicleJourney la liste des timetableId
        $listIdVBehicleJourneyWithtimetableId = array();

        foreach ($listTimetable as $idTimetable => $timetable) {
            foreach ($timetable->getListVehicleJourneyId() as $idVehicleJourney) {
                $listIdVBehicleJourneyWithtimetableId[$idVehicleJourney][] = $idTimetable;
            }
        }

        foreach ($listVehicleJourney as $idVehicleJourney => $vehicleJourneyXml) {

            $routeId = $vehicleJourneyXml->getRouteId();

            $route = $this->routesRepository->findOneBy(['routeId' => $routeId]);

            if (is_null($route)) {
                if ($this->errorMessage !== "") $this->errorMessage .= " ";
                $this->errorMessage .= "La route avec l'id : " . $routeId . " n'existe pas en base.";
                continue;
            }

            foreach ($vehicleJourneyXml->getListVehicleJourneyAtStop() as $vehicleJourneyAtStop) {

                // On va d'abord chercher si l'enregistrement existe avec l'ID VehicleJourney + order pour avoir un seul ID
                $idVehicleJourneyWithOrder = $idVehicleJourney . $vehicleJourneyAtStop->getOrder();

                $schedule = $this->scheduleRepository->findOneBy(['idScheduleWithOrder' => $idVehicleJourneyWithOrder]);

                if (is_null($schedule)) {

                    $idStopPoint = $vehicleJourneyAtStop->extractIdStopAreaFromStopPoint();
                    $stopArea = $this->stopAreaRepository->findOneBy(['idStopArea' => $idStopPoint]);

                    if (is_null($stopArea)) {
                        if ($this->errorMessage !== "") $this->errorMessage .= " ";
                        $this->errorMessage .= "Le point d'arrêt avec l'ID : " . $idStopPoint . " n'existe pas en base.";
                        continue;
                    }

                    $routeDetail = $this->routeDetailRepository->findOneBy([
                        'routes' => $route,
                        'stopArea' => $stopArea
                    ]);

                    if (is_null($routeDetail)) {
                        if ($this->errorMessage !== "") $this->errorMessage .= " ";
                        $this->errorMessage .= "La route détail avec l'id route : " . $route->getRouteId() .
                            " et le point d'arrêt avec l'ID : " . $stopArea->getIdStopArea() . " n'existe pas en base.";
                        continue;
                    }

                    $listTimetableFromDatabase = $this->timetableRepository->getMultipleTimetableFromArray($listIdVBehicleJourneyWithtimetableId[$idVehicleJourney]);

                    $newSchedule = new Schedule();

                    foreach ($listTimetableFromDatabase as $timetable) {
                        $newSchedule->addAvailable($timetable);
                    }

                    $newSchedule->setDepartureTime($vehicleJourneyAtStop->getDepartureTime());
                    $newSchedule->setArrivalTime($vehicleJourneyAtStop->getArrivalTime());
                    $newSchedule->setRoute($route);
                    $newSchedule->setPlanned($routeDetail);
                    $newSchedule->setIdScheduleWithOrder($idVehicleJourneyWithOrder);

                    $this->entityManager->persist($newSchedule);
                }
            }
        }
        $this->entityManager->flush();
    }
}