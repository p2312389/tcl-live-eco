<?php

namespace App\Services;

use App\Entity\Address;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Service pour convertir les adresses en coordonées GPS et inversement.
 */
class ConverterAddressService
{
    private string $urlToNominativApi;
    private string $country;
    private HttpClientInterface $httpClient;
    private SerializerInterface $serializer;

    public function __construct(HttpClientInterface $httpClient, SerializerInterface $serializer) {
        $this->country = "france";
        $this->urlToNominativApi = "https://nominatim.openstreetmap.org";
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
    }

    /**
     * Convertit une adresse postale en coordonées GPS.
     *
     * @param string $address L'adresse (numéro et nom de rue) à convertir en coordonnées GPS.
     * @param string $city La ville à convertir en coordonnées GPS.
     * @return JsonResponse Un JSON avec un objet Address contenant les coordonnées GPS correspondant à l'adresse et à la ville.
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function callConverterAddressToCoordinate(string $address, string $city):JsonResponse
    {
        $addressToReturn = $this->convertAddressToCoordinate($address, $city);

        if (is_null($addressToReturn)) {
            $error = [
                'status' => Response::HTTP_NOT_FOUND,
                'message' => 'L\'adresse n\'a pas été trouvée.'
            ];

            return new JsonResponse($this->serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);
        }

        return new JsonResponse($this->serializer->serialize($addressToReturn, 'json'), Response::HTTP_OK, [], true);
    }

    /**
     * Convertit les coordonnées GPS en adresse postale.
     *
     * @param string $latitude
     * @param string $longitude
     * @return JsonResponse Un JSON avec un objet Address contenant l'adresse postale correspondant aux coordonnées GPS.
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function convertCoordinateToAddress(string $latitude, string $longitude): jsonResponse
    {
        $completeUrl = $this->urlToNominativApi . "/reverse?format=json&lat=" . $latitude . "&lon=" . $longitude;

        $response = $this->sendGetRequest($completeUrl);

        $infoFromCoordinate = $response->toArray();
        $addressInfo = array();

        if (count($infoFromCoordinate) > 0)
        {
            // $response->toArray() renvoie un tableau double dimensionnel de longueur 1 pour le premier tableau
            // et toutes les infos dans le deuxième tableau accessible via des clés
            $addressInfo["house_number"] = $infoFromCoordinate["address"]["house_number"];
            $addressInfo["road"] = $infoFromCoordinate["address"]["road"];
            $addressInfo["suburb"] = $infoFromCoordinate["address"]["suburb"];
            $addressInfo["city"] = $infoFromCoordinate["address"]["city"];
            $addressInfo["municipality"] = $infoFromCoordinate["address"]["municipality"];
            $addressInfo["postcode"] = $infoFromCoordinate["address"]["postcode"];
        } else
        {
            $error = [
                'status' => Response::HTTP_NOT_FOUND,
                'message' => 'Les coordonnées GPS sont incorrectes'
            ];

            return new JsonResponse($this->serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);
        }

        return new JsonResponse($addressInfo, $response->getStatusCode(), [], false);
    }

    /**
     * Convertit une adresse postale en coordonées GPS.
     *
     * @param string $address L'adresse (numéro et nom de rue) à convertir en coordonnées GPS.
     * @param string $city La ville à convertir en coordonnées GPS.
     * @return Address|null Un objet Address contenant les coordonnées GPS correspondant à l'adresse et à la ville.
     * Renvoi null si l'adresse n'existe pas.
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function convertAddressToCoordinate(string $address, string $city): ?Address {
        $completeUrl = $this->urlToNominativApi . "/search?q=" . $address . "+" . $city . "+" . $this->country . "&format=json" . "&limit=1";
        $formatedUrl = str_replace(" ", "+", $completeUrl);

        $response = $this->httpClient->request(
            'GET',
            $formatedUrl
        );

        $responseAddressInfo = $response->toArray();
        $addressToReturn = new Address();

        if (count($responseAddressInfo) > 0) {
            // $response->toArray() renvoie un tableau double dimensionnel de longueur 1 pour le premier tableau
            // et toutes les infos dans le deuxième tableau accessible via des clés
            $addressToReturn->setStreet($address);
            $addressToReturn->setMunicipality($city);
            $addressToReturn->setLongitude($responseAddressInfo[0]["lon"]);  ///! 
            $addressToReturn->setLatitude($responseAddressInfo[0]["lat"]);

            return $addressToReturn;
        } else {
            return null;
        }
    }

    /**
     * Convertit une adresse postale en coordonées GPS.
     *
     * @param string $address L'adresse (numéro et nom de rue) à convertir en coordonnées GPS.
     * @param string $city La ville à convertir en coordonnées GPS.
     * @param string $ZipCode Le code postal à convertir en coordonnées GPS.
     * @return Address|null Un JSON avec un objet Address contenant les coordonnées GPS correspondant aux coordonnées GPS.
     * Renvoi null si l'adresse n'existe pas.
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function convertAddressToCoordinateWithZipCode(string $address, string $city,string $ZipCode): ?Address {
        $completeUrl = $this->urlToNominativApi . "/search?q=" . $address . "+" . $city . "+" . $this->country . "+" . $ZipCode . "&format=json" . "&limit=1";
        $formatedUrl = str_replace(" ", "+", $completeUrl);

        $response = $this->httpClient->request(
            'GET',
            $formatedUrl
        );

        // $response->toArray() renvoie un tableau double dimensionnel de longueur 1 pour le premier tableau
        $responseAddressInfo = $response->toArray();
        $addressToReturn = new Address();

        if (count($responseAddressInfo) > 0) {
            
            // et toutes les infos dans le deuxième tableau accessible via des clés
            $addressToReturn->setStreet($address);
            $addressToReturn->setMunicipality($city);
            $addressToReturn->setZipCode($ZipCode);
            $addressToReturn->setLongitude($responseAddressInfo[0]["lon"]);
            $addressToReturn->setLatitude($responseAddressInfo[0]["lat"]);

            return $addressToReturn;
        } else {
            return null;
        }
    }

    /**
     * Envoi une requête HTTP à une application externe.
     *
     * @throws TransportExceptionInterface
     */
    private function sendGetRequest($url): ResponseInterface
    {
        return $this->httpClient->request(
            'GET',
            $url
        );
    }
}