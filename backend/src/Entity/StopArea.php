<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\StopAreaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: StopAreaRepository::class)]
#[ApiResource(normalizationContext: ['groups' => ['read:StopArea:stationWithAddress']])]
class StopArea
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read:StopArea:stationWithAddress', 'read:StopArea:stopAreaDetail'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read:Transport:detail', 'read:StopArea:stationWithAddress', 'read:StopArea:stopAreaDetail'])]
    private ?string $name = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['read:Transport:detail', 'read:StopArea:stationWithAddress', 'read:StopArea:stopAreaDetail'])]
    private ?bool $wheelchairAccessible = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['read:Transport:detail', 'read:StopArea:stationWithAddress'])]
    private ?bool $lift = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['read:Transport:detail', 'read:StopArea:stationWithAddress'])]
    private ?bool $escalator = null;

    #[ORM\OneToMany(mappedBy: 'departure', targetEntity: Routes::class)]
    private Collection $routes;

    #[ORM\OneToMany(mappedBy: 'stopArea', targetEntity: RouteDetail::class)]
    private Collection $routeDetails;

    #[ORM\ManyToOne(inversedBy: 'stopAreas')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['read:StopArea:stationWithAddress'])]
    private ?Address $address = null;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'divStopArea')]
    private ?self $stopArea = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['read:StopArea:stationWithAddress'])]
    private ?string $idStopArea = null;

    public function __construct()
    {
        $this->routes = new ArrayCollection();
        $this->routeDetails = new ArrayCollection();
        $this->stopAreas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function isWheelchairAccessible(): ?bool
    {
        return $this->wheelchairAccessible;
    }

    public function setWheelchairAccessible(bool $wheelchairAccessible): static
    {
        $this->wheelchairAccessible = $wheelchairAccessible;

        return $this;
    }

    public function isLift(): ?bool
    {
        return $this->lift;
    }

    public function setLift(bool $lift): static
    {
        $this->lift = $lift;

        return $this;
    }

    public function isEscalator(): ?bool
    {
        return $this->escalator;
    }

    public function setEscalator(bool $escalator): static
    {
        $this->escalator = $escalator;

        return $this;
    }

    /**
     * @return Collection<int, Routes>
     */
    public function getRoutes(): Collection
    {
        return $this->routes;
    }

    public function addRoute(Routes $route): static
    {
        if (!$this->routes->contains($route)) {
            $this->routes->add($route);
            $route->setDeparture($this);
        }

        return $this;
    }

    public function removeRoute(Routes $route): static
    {
        if ($this->routes->removeElement($route)) {
            // set the owning side to null (unless already changed)
            if ($route->getDeparture() === $this) {
                $route->setDeparture(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RouteDetail>
     */
    public function getRouteDetails(): Collection
    {
        return $this->routeDetails;
    }

    public function addRouteDetail(RouteDetail $routeDetail): static
    {
        if (!$this->routeDetails->contains($routeDetail)) {
            $this->routeDetails->add($routeDetail);
            $routeDetail->setStopArea($this);
        }

        return $this;
    }

    public function removeRouteDetail(RouteDetail $routeDetail): static
    {
        if ($this->routeDetails->removeElement($routeDetail)) {
            // set the owning side to null (unless already changed)
            if ($routeDetail->getStopArea() === $this) {
                $routeDetail->setStopArea(null);
            }
        }

        return $this;
    }


    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): static
    {
        $this->address = $address;

        return $this;
    }

    public function getStopArea(): ?self
    {
        return $this->stopArea;
    }

    public function setStopArea(?self $stopArea): static
    {
        $this->stopArea = $stopArea;

        return $this;
    }

    public function getIdStopArea(): ?string
    {
        return $this->idStopArea;
    }

    public function setIdStopArea(?string $idStopArea): static
    {
        $this->idStopArea = $idStopArea;

        return $this;
    }
    /////fait a la main
    public function setId(?string $id): static
    {
        $this->id = $id;

        return $this;
    }
}
