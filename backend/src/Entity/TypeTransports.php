<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\TypeTransportsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: TypeTransportsRepository::class)]
#[ApiResource]
class TypeTransports
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read:typeTransport:all'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read:typeTransport:all'])]
    private ?string $label = null;

    #[ORM\OneToMany(mappedBy: 'type', targetEntity: Transports::class)]
    private Collection $transports;

    public function __construct()
    {
        $this->transports = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection<int, Transports>
     */
    public function getTransports(): Collection
    {
        return $this->transports;
    }

    public function addTransport(Transports $transport): static
    {
        if (!$this->transports->contains($transport)) {
            $this->transports->add($transport);
            $transport->setTypeTransport($this);
        }

        return $this;
    }

    public function removeTransport(Transports $transport): static
    {
        if ($this->transports->removeElement($transport)) {
            // set the owning side to null (unless already changed)
            if ($transport->getTypeTransport() === $this) {
                $transport->setTypeTransport(null);
            }
        }

        return $this;
    }
}
