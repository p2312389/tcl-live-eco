<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\RouteDetailRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RouteDetailRepository::class)]
#[ApiResource]
class RouteDetail
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read:Transport:detail'])]
    private ?int $id = null;

    #[ORM\Column]
    #[Groups(['read:Transport:detail'])]
    private ?int $routeDetailOrder = null;

    #[ORM\OneToMany(mappedBy: 'planned', targetEntity: Schedule::class)]
    #[Groups(['read:Transport:detail'])]
    private Collection $schedules;

    #[ORM\ManyToOne(inversedBy: 'routeDetails')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Routes $routes = null;

    #[ORM\ManyToOne(inversedBy: 'routeDetails')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['read:Transport:detail'])]
    private ?StopArea $stopArea = null;

    public function __construct()
    {
        $this->schedules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRouteDetailOrder(): ?int
    {
        return $this->routeDetailOrder;
    }

    public function setRouteDetailOrder(int $routeDetailOrder): static
    {
        $this->routeDetailOrder = $routeDetailOrder;

        return $this;
    }

    /**
     * @return Collection<int, Schedule>
     */
    public function getSchedules(): Collection
    {
        return $this->schedules;
    }

    public function addSchedule(Schedule $schedule): static
    {
        if (!$this->schedules->contains($schedule)) {
            $this->schedules->add($schedule);
            $schedule->setPlanned($this);
        }

        return $this;
    }

    public function removeSchedule(Schedule $schedule): static
    {
        if ($this->schedules->removeElement($schedule)) {
            // set the owning side to null (unless already changed)
            if ($schedule->getPlanned() === $this) {
                $schedule->setPlanned(null);
            }
        }

        return $this;
    }

    public function getRoutes(): ?Routes
    {
        return $this->routes;
    }

    public function setRoutes(?Routes $routes): static
    {
        $this->routes = $routes;

        return $this;
    }

    public function getStopArea(): ?StopArea
    {
        return $this->stopArea;
    }

    public function setStopArea(?StopArea $stopArea): static
    {
        $this->stopArea = $stopArea;

        return $this;
    }
}
