<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\RoutesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RoutesRepository::class)]
#[ApiResource]
class Routes
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read:Transport:detail', 'read:Routes:stopAreaDetail'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read:Transport:detail', 'read:Routes:stopAreaDetail'])]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['read:Transport:detail', 'read:Routes:stopAreaDetail'])]
    private ?string $completeName = null;

    #[ORM\ManyToOne(inversedBy: 'routes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Transports $transports = null;

    #[ORM\ManyToOne(inversedBy: 'routes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?StopArea $departure = null;

    #[ORM\ManyToOne(inversedBy: 'routes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?StopArea $terminal = null;

    #[ORM\OneToMany(mappedBy: 'routes', targetEntity: RouteDetail::class)]
    #[Groups(['read:Transport:detail'])]
    private Collection $routeDetails;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $routeId = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read:Transport:detail'])]
    private ?string $direction = null;

    #[ORM\OneToMany(mappedBy: 'route', targetEntity: Schedule::class)]
    private Collection $schedules;

    public function __construct()
    {
        $this->routeDetails = new ArrayCollection();
        $this->schedules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getCompleteName(): ?string
    {
        return $this->completeName;
    }

    public function setCompleteName(?string $completeName): static
    {
        $this->completeName = $completeName;

        return $this;
    }

    public function getTransports(): ?Transports
    {
        return $this->transports;
    }

    public function setTransports(?Transports $transports): static
    {
        $this->transports = $transports;

        return $this;
    }

    public function getDeparture(): ?StopArea
    {
        return $this->departure;
    }

    public function setDeparture(?StopArea $departure): static
    {
        $this->departure = $departure;

        return $this;
    }

    public function getTerminal(): ?StopArea
    {
        return $this->terminal;
    }

    public function setTerminal(?StopArea $terminal): static
    {
        $this->terminal = $terminal;

        return $this;
    }

    /**
     * @return Collection<int, RouteDetail>
     */
    public function getRouteDetails(): Collection
    {
        return $this->routeDetails;
    }

    public function addRouteDetail(RouteDetail $routeDetail): static
    {
        if (!$this->routeDetails->contains($routeDetail)) {
            $this->routeDetails->add($routeDetail);
            $routeDetail->setRoutes($this);
        }

        return $this;
    }

    public function removeRouteDetail(RouteDetail $routeDetail): static
    {
        if ($this->routeDetails->removeElement($routeDetail)) {
            // set the owning side to null (unless already changed)
            if ($routeDetail->getRoutes() === $this) {
                $routeDetail->setRoutes(null);
            }
        }

        return $this;
    }

    public function getRouteId(): ?string
    {
        return $this->routeId;
    }

    public function setRouteId(?string $routeId): static
    {
        $this->routeId = $routeId;

        return $this;
    }

    public function getDirection(): ?string
    {
        return $this->direction;
    }

    public function setDirection(string $direction): static
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * @return Collection<int, Schedule>
     */
    public function getSchedules(): Collection
    {
        return $this->schedules;
    }

    public function addSchedule(Schedule $schedule): static
    {
        if (!$this->schedules->contains($schedule)) {
            $this->schedules->add($schedule);
            $schedule->setRoute($this);
        }

        return $this;
    }

    public function removeSchedule(Schedule $schedule): static
    {
        if ($this->schedules->removeElement($schedule)) {
            // set the owning side to null (unless already changed)
            if ($schedule->getRoute() === $this) {
                $schedule->setRoute(null);
            }
        }

        return $this;
    }

    //fait a la main
    public function setId(?string $id): static
    {
        $this->id = $id;

        return $this;

    }
}
