<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\VelovStationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VelovStationRepository::class)]
#[ApiResource]
class VelovStation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?int $idStation = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(nullable: true)]
    private ?int $nbBornettes = null;

    #[ORM\Column]
    private ?bool $ouverte = null;

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'velovStations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Address $address = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdStation(): ?int
    {
        return $this->idStation;
    }

    public function setIdStation(?int $idStation): static
    {
        $this->idStation = $idStation;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getNbBornettes(): ?int
    {
        return $this->nbBornettes;
    }

    public function setNbBornettes(?int $nbBornettes): static
    {
        $this->nbBornettes = $nbBornettes;

        return $this;
    }

    public function isOuverte(): ?bool
    {
        return $this->ouverte;
    }

    public function setOuverte(bool $ouverte): static
    {
        $this->ouverte = $ouverte;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): static
    {
        $this->address = $address;

        return $this;
    }
}
