<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ScheduleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ScheduleRepository::class)]
#[ApiResource]
class Schedule
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private ?\DateTimeInterface $arrivalTime = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private ?\DateTimeInterface $departureTime = null;

    #[ORM\ManyToOne(inversedBy: 'schedules')]
    #[ORM\JoinColumn(nullable: false)]
    private ?RouteDetail $planned = null;

    #[ORM\ManyToMany(targetEntity: Timetable::class, inversedBy: 'schedules')]
    private Collection $available;

    #[ORM\ManyToOne(inversedBy: 'schedules')]
    private ?Routes $route = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $idScheduleWithOrder = null;

    public function __construct()
    {
        $this->available = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArrivalTime(): ?\DateTimeInterface
    {
        return $this->arrivalTime;
    }

    public function setArrivalTime(\DateTimeInterface $arrivalTime): static
    {
        $this->arrivalTime = $arrivalTime;

        return $this;
    }

    public function getDepartureTime(): ?\DateTimeInterface
    {
        return $this->departureTime;
    }

    public function setDepartureTime(\DateTimeInterface $departureTime): static
    {
        $this->departureTime = $departureTime;

        return $this;
    }

    public function getPlanned(): ?RouteDetail
    {
        return $this->planned;
    }

    public function setPlanned(?RouteDetail $planned): static
    {
        $this->planned = $planned;

        return $this;
    }

    /**
     * @return Collection<int, Timetable>
     */
    public function getAvailable(): Collection
    {
        return $this->available;
    }

    public function addAvailable(Timetable $available): static
    {
        if (!$this->available->contains($available)) {
            $this->available->add($available);
        }

        return $this;
    }

    public function removeAvailable(Timetable $available): static
    {
        $this->available->removeElement($available);

        return $this;
    }

    public function getRoute(): ?Routes
    {
        return $this->route;
    }

    public function setRoute(?Routes $route): static
    {
        $this->route = $route;

        return $this;
    }

    public function getIdScheduleWithOrder(): ?string
    {
        return $this->idScheduleWithOrder;
    }

    public function setIdScheduleWithOrder(?string $idScheduleWithOrder): static
    {
        $this->idScheduleWithOrder = $idScheduleWithOrder;

        return $this;
    }
}
