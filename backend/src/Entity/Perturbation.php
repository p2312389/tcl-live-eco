<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\PerturbationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PerturbationRepository::class)]
#[ApiResource]
class Perturbation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $type = null;

    #[ORM\Column(length: 255)]
    private ?string $cause = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $debut = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $fin = null;

    #[ORM\Column(length: 255)]
    private ?string $mode = null;

    #[ORM\Column(length: 255)]
    private ?string $titre = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $message = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $lastUpdateFme = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $listeobjet = null;

    #[ORM\ManyToOne(inversedBy: 'perturbations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Transports $Transports = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getCause(): ?string
    {
        return $this->cause;
    }

    public function setCause(string $cause): static
    {
        $this->cause = $cause;

        return $this;
    }

    public function getDebut(): ?\DateTimeInterface
    {
        return $this->debut;
    }

    public function setDebut(\DateTimeInterface $debut): static
    {
        $this->debut = $debut;

        return $this;
    }

    public function getFin(): ?\DateTimeInterface
    {
        return $this->fin;
    }

    public function setFin(\DateTimeInterface $fin): static
    {
        $this->fin = $fin;

        return $this;
    }

    public function getMode(): ?string
    {
        return $this->mode;
    }

    public function setMode(string $mode): static
    {
        $this->mode = $mode;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): static
    {
        $this->titre = $titre;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): static
    {
        $this->message = $message;

        return $this;
    }

    public function getLastUpdateFme(): ?\DateTimeInterface
    {
        return $this->lastUpdateFme;
    }

    public function setLastUpdateFme(\DateTimeInterface $lastUpdateFme): static
    {
        $this->lastUpdateFme = $lastUpdateFme;

        return $this;
    }

    public function getListeobjet(): ?string
    {
        return $this->listeobjet;
    }

    public function setListeobjet(?string $listeobjet): static
    {
        $this->listeobjet = $listeobjet;

        return $this;
    }

    public function getTransports(): ?Transports
    {
        return $this->Transports;
    }

    public function setTransports(?Transports $Transports): static
    {
        $this->Transports = $Transports;

        return $this;
    }
}
