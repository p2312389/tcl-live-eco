<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\TimetableRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TimetableRepository::class)]
#[ApiResource]
class Timetable
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $startOfPeriod = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $endOfPeriod = null;

    #[ORM\Column]
    private ?bool $monday = null;

    #[ORM\Column]
    private ?bool $tuesday = null;

    #[ORM\Column]
    private ?bool $wednesday = null;

    #[ORM\Column]
    private ?bool $thursday = null;

    #[ORM\Column]
    private ?bool $friday = null;

    #[ORM\Column]
    private ?bool $saturday = null;

    #[ORM\Column]
    private ?bool $sunday = null;

    #[ORM\ManyToMany(targetEntity: Schedule::class, mappedBy: 'available')]
    private Collection $schedules;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $idTimetable = null;

    public function __construct()
    {
        $this->schedules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartOfPeriod(): ?\DateTimeInterface
    {
        return $this->startOfPeriod;
    }

    public function setStartOfPeriod(\DateTimeInterface $startOfPeriod): static
    {
        $this->startOfPeriod = $startOfPeriod;

        return $this;
    }

    public function getEndOfPeriod(): ?\DateTimeInterface
    {
        return $this->endOfPeriod;
    }

    public function setEndOfPeriod(\DateTimeInterface $endOfPeriod): static
    {
        $this->endOfPeriod = $endOfPeriod;

        return $this;
    }

    public function isMonday(): ?bool
    {
        return $this->monday;
    }

    public function setMonday(bool $monday): static
    {
        $this->monday = $monday;

        return $this;
    }

    public function isTuesday(): ?bool
    {
        return $this->tuesday;
    }

    public function setTuesday(bool $tuesday): static
    {
        $this->tuesday = $tuesday;

        return $this;
    }

    public function isWednesday(): ?bool
    {
        return $this->wednesday;
    }

    public function setWednesday(bool $wednesday): static
    {
        $this->wednesday = $wednesday;

        return $this;
    }

    public function isThursday(): ?bool
    {
        return $this->thursday;
    }

    public function setThursday(bool $thursday): static
    {
        $this->thursday = $thursday;

        return $this;
    }

    public function isFriday(): ?bool
    {
        return $this->friday;
    }

    public function setFriday(bool $friday): static
    {
        $this->friday = $friday;

        return $this;
    }

    public function isSaturday(): ?bool
    {
        return $this->saturday;
    }

    public function setSaturday(bool $saturday): static
    {
        $this->saturday = $saturday;

        return $this;
    }

    public function isSunday(): ?bool
    {
        return $this->sunday;
    }

    public function setSunday(bool $sunday): static
    {
        $this->sunday = $sunday;

        return $this;
    }

    /**
     * @return Collection<int, Schedule>
     */
    public function getSchedules(): Collection
    {
        return $this->schedules;
    }

    public function addSchedule(Schedule $schedule): static
    {
        if (!$this->schedules->contains($schedule)) {
            $this->schedules->add($schedule);
            $schedule->addAvailable($this);
        }

        return $this;
    }

    public function removeSchedule(Schedule $schedule): static
    {
        if ($this->schedules->removeElement($schedule)) {
            $schedule->removeAvailable($this);
        }

        return $this;
    }

    public function getIdTimetable(): ?string
    {
        return $this->idTimetable;
    }

    public function setIdTimetable(?string $idTimetable): static
    {
        $this->idTimetable = $idTimetable;

        return $this;
    }
}
