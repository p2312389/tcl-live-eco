<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\TransportsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: TransportsRepository::class)]
#[ApiResource]
class Transports
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['read:Transport:autocomplete', 'read:Transport:detail', 'read:Transport:stopAreaDetail'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read:Transport:autocomplete', 'read:Transport:detail', 'read:Transport:stopAreaDetail'])]
    private ?string $lineName = null;

    #[ORM\Column(length: 255)]
    private ?string $codeTrace = null;

    #[ORM\Column(length: 255)]
    #[Groups(['read:Transport:stopAreaDetail'])]
    private ?string $codeLine = null;

    #[ORM\Column]
    #[Groups(['read:Transport:detail', 'read:Transport:stopAreaDetail'])]
    private ?bool $wheelchairAccessible = null;

    #[ORM\ManyToOne(inversedBy: 'transports')]
    #[ORM\JoinColumn(nullable: false)]
    private ?TypeTransports $type = null;

    #[ORM\OneToMany(mappedBy: 'Transports', targetEntity: Perturbation::class)]
    private Collection $perturbations;

    #[ORM\OneToMany(mappedBy: 'transports', targetEntity: Routes::class)]
    #[Groups(['read:Transport:detail'])]
    private Collection $routes;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $transportModeName = null;

    public function __construct()
    {
        $this->perturbations = new ArrayCollection();
        $this->routes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $n)
    {
        $this->id = $n;
    }

    public function getLineName(): ?string
    {
        return $this->lineName;
    }

    public function setLineName(string $lineName): static
    {
        $this->lineName = $lineName;

        return $this;
    }

    public function getCodeTrace(): ?string
    {
        return $this->codeTrace;
    }

    public function setCodeTrace(string $codeTrace): static
    {
        $this->codeTrace = $codeTrace;

        return $this;
    }

    public function getCodeLine(): ?string
    {
        return $this->codeLine;
    }

    public function setCodeLine(string $codeLine): static
    {
        $this->codeLine = $codeLine;

        return $this;
    }

    public function isWheelchairAccessible(): ?bool
    {
        return $this->wheelchairAccessible;
    }

    public function setWheelchairAccessible(bool $wheelchairAccessible): static
    {
        $this->wheelchairAccessible = $wheelchairAccessible;

        return $this;
    }

    public function getTypeTransport(): ?TypeTransports
    {
        return $this->type;
    }

    public function setTypeTransport(?TypeTransports $typeTransport): static
    {
        $this->type = $typeTransport;

        return $this;
    }

    /**
     * @return Collection<int, Perturbation>
     */
    public function getPerturbations(): Collection
    {
        return $this->perturbations;
    }

    public function addPerturbation(Perturbation $perturbation): static
    {
        if (!$this->perturbations->contains($perturbation)) {
            $this->perturbations->add($perturbation);
            $perturbation->setTransports($this);
        }

        return $this;
    }

    public function removePerturbation(Perturbation $perturbation): static
    {
        if ($this->perturbations->removeElement($perturbation)) {
            // set the owning side to null (unless already changed)
            if ($perturbation->getTransports() === $this) {
                $perturbation->setTransports(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Routes>
     */
    public function getRoutes(): Collection
    {
        return $this->routes;
    }

    public function addRoute(Routes $route): static
    {
        if (!$this->routes->contains($route)) {
            $this->routes->add($route);
            $route->setTransports($this);
        }

        return $this;
    }

    public function removeRoute(Routes $route): static
    {
        if ($this->routes->removeElement($route)) {
            // set the owning side to null (unless already changed)
            if ($route->getTransports() === $this) {
                $route->setTransports(null);
            }
        }

        return $this;
    }

    public function getTransportModeName(): ?string
    {
        return $this->transportModeName;
    }

    public function setTransportModeName(?string $transportModeName): static
    {
        $this->transportModeName = $transportModeName;

        return $this;
    }
}
