<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\AddressRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AddressRepository::class)]
#[ApiResource]
class Address
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([
        'read:Address:autocompleteStreet',
        'read:Address:autocompleteZipcode',
        'read:Address:autocompleteCity'
    ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['read:StopArea:stationWithAddress', 'read:Address:autocompleteStreet'])]
    private ?string $street = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['read:StopArea:stationWithAddress', 'read:Address:autocompleteStreet', 'read:Address:autocompleteCity'])]
    private ?string $municipality = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['read:StopArea:stationWithAddress', 'read:Address:autocompleteStreet', 'read:Address:autocompleteZipcode'])]
    private ?string $zipCode = null;

    #[ORM\OneToMany(mappedBy: 'address', targetEntity: StopArea::class)]
    private Collection $stopAreas;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['read:StopArea:stationWithAddress'])]
    private ?string $latitude = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['read:StopArea:stationWithAddress'])]
    private ?string $longitude = null;

    #[ORM\OneToMany(mappedBy: 'address', targetEntity: VelovStation::class)]
    private Collection $velovStations;

    public function __construct()
    {
        $this->stopAreas = new ArrayCollection();
        $this->velovStations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): static
    {
        $this->street = $street;

        return $this;
    }

    public function getMunicipality(): ?string
    {
        return $this->municipality;
    }

    public function setMunicipality(string $municipality): static
    {
        $this->municipality = $municipality;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): static
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * @return Collection<int, StopArea>
     */
    public function getStopAreas(): Collection
    {
        return $this->stopAreas;
    }

    public function addStopArea(StopArea $stopArea): static
    {
        if (!$this->stopAreas->contains($stopArea)) {
            $this->stopAreas->add($stopArea);
            $stopArea->setAddress($this);
        }

        return $this;
    }

    public function removeStopArea(StopArea $stopArea): static
    {
        if ($this->stopAreas->removeElement($stopArea)) {
            // set the owning side to null (unless already changed)
            if ($stopArea->getAddress() === $this) {
                $stopArea->setAddress(null);
            }
        }

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): static
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): static
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return Collection<int, VelovStation>
     */
    public function getVelovStations(): Collection
    {
        return $this->velovStations;
    }

    public function addVelovStation(VelovStation $velovStation): static
    {
        if (!$this->velovStations->contains($velovStation)) {
            $this->velovStations->add($velovStation);
            $velovStation->setAddress($this);
        }

        return $this;
    }

    public function removeVelovStation(VelovStation $velovStation): static
    {
        if ($this->velovStations->removeElement($velovStation)) {
            // set the owning side to null (unless already changed)
            if ($velovStation->getAddress() === $this) {
                $velovStation->setAddress(null);
            }
        }

        return $this;
    }
}
