<?php

namespace App\Repository;

use App\Entity\Transports;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Parameter;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Transports>
 *
 * @method Transports|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transports|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transports[]    findAll()
 * @method Transports[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransportsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transports::class);
    }

    /**
     * Recherche les transports commençant par le mot clé renseigné.
     *
     * @param string $keyword Le mot-clé recherché.
     * @return array La liste des transports commençant par le mot-clé.
     */
    public function findTransportsNameWhichStartWithGivenKeyword(string $keyword): array
    {
        return $this->createQueryBuilder('t')
            ->andWhere('LOWER(t.lineName) LIKE :keyword')
            ->setParameter('keyword', strtolower($keyword) . '%')
            ->getQuery()
            ->getResult();
    }

    /**
     * Recherche un transport via son nom et son type de transport.
     *
     * @param string $transportName Le nom du transport recherché.
     * @param array $idTypeTransport L'id du type de transport du transport recherché.
     * @return array La liste des transports trouvés.
     */
    public function searchTransportWithNameAndTypeTransport(string $transportName, array $idTypeTransport): array
    {
        $queryBuilder = $this->createQueryBuilder('t');

        if ($transportName !== '') {
            $queryBuilder->andWhere('t.lineName = :transportName')
                ->setParameter('transportName', $transportName);
        }
        if (count($idTypeTransport) > 0) {
            $queryBuilder->andWhere('tt.id IN (:idTypeTransports)')
                ->setParameter('idTypeTransports', $idTypeTransport);
        }

        return $queryBuilder->innerJoin('App\Entity\TypeTransports', 'tt', 'WITH', 't.type = tt.id')
            ->getQuery()
            ->getResult();
    }

//    /**
//     * @return Transports[] Returns an array of Transports objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Transports
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
