<?php

namespace App\Repository;

use App\Entity\Perturbation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Perturbation>
 *
 * @method Perturbation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Perturbation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Perturbation[]    findAll()
 * @method Perturbation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PerturbationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Perturbation::class);
    }

//    /**
//     * @return Perturbation[] Returns an array of Perturbation objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Perturbation
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
