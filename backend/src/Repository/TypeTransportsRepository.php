<?php

namespace App\Repository;

use App\Entity\TypeTransports;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TypeTransports>
 *
 * @method TypeTransports|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeTransports|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeTransports[]    findAll()
 * @method TypeTransports[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeTransportsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeTransports::class);
    }

//    /**
//     * @return TypeTransports[] Returns an array of TypeTransports objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?TypeTransports
//    {
//        return $this->createQueryBuilder('t')
//            ->andWhere('t.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
