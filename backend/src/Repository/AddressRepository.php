<?php

namespace App\Repository;

use App\Entity\Address;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Address>
 *
 * @method Address|null find($id, $lockMode = null, $lockVersion = null)
 * @method Address|null findOneBy(array $criteria, array $orderBy = null)
 * @method Address[]    findAll()
 * @method Address[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AddressRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Address::class);
    }

    /**
     * Recherche une adresse postale.
     *
     * @param $street : Le numéro de rue et le nom de la rue de l'adresse recherchée.
     * @param $zipCode : Le code postale de l'adresse recherchée.
     * @param $Municipality : Le nom de la ville de l'adresse recherchée.
     * @return array les adresses trouvées
     */
    public function findAddressWithStreetZipCodeMunicipality($street, $zipCode, $Municipality): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.street = :street')
            ->andWhere('a.municipality = :municipality')
            ->andWhere('a.zipCode = :zipCode')
            ->setParameters(new ArrayCollection(array(
                new Parameter('street', $street),
                new Parameter('municipality', $Municipality),
                new Parameter('zipCode', $zipCode)
            )))
            ->getQuery()
            ->getResult();
    }

    /**
     * Recherche une adresse avec des coordonnées GPS.
     *
     * @param $longitude
     * @param $latitude
     * @return array La liste des adresses trouvées.
     */
    public function findAddressWithLongitudeLatitude($longitude, $latitude): array
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.longitude = :longitude')
            ->andWhere('a.latitude = :latitude')
            ->setParameters(new ArrayCollection(array(
                new Parameter('longitude', $longitude),
                new Parameter('latitude', $latitude)
            )))
            ->getQuery()
            ->getResult();
    }

    /**
     * Recherche tous les noms de rue qui contiennent par le mot-clé passé en paramètre.
     *
     * @param string $keyword Le mot-clé recherché
     * @return array La liste des noms de rue contenant la chaine renseignée en paramètre.
     */
    public function findStreetWhichStartWithGivenKeyword(string $keyword): array {
        return $this->createQueryBuilder('a')
            ->andWhere('LOWER(a.street) LIKE :keyword')
            ->setParameter('keyword', '%' . strtolower($keyword) . '%')
            ->groupBy('a.street')
            ->getQuery()
            ->getResult();
    }

    /**
     * Recherche tous les codes postaux qui commencent par le mot-clé passé en paramètre.
     *
     * @param string $keyword Le mot-clé recherché
     * @return array La liste des codes postaux contenant la chaine renseignée en paramètre.
     */
    public function findZipcodeWhichStartWithGivenKeyword(string $keyword): array {
        return $this->createQueryBuilder('a')
            ->andWhere('LOWER(a.zipCode) LIKE :keyword')
            ->setParameter('keyword', strtolower($keyword) . '%')
            ->groupBy('a.zipCode')
            ->getQuery()
            ->getResult();
    }

    /**
     * Recherche toutes les villes qui commencent par le mot-clé passé en paramètre.
     *
     * @param string $keyword Le mot-clé recherché
     * @return array La liste des villes contenant la chaine renseignée en paramètre.
     */
    public function findCityWhichStartWithGivenKeyword(string $keyword): array {
        return $this->createQueryBuilder('a')
            ->andWhere('LOWER(a.municipality) LIKE :keyword')
            ->setParameter('keyword', strtolower($keyword) . '%')
            ->groupBy('a.municipality')
            ->getQuery()
            ->getResult();
    }

//    /**
//     * @return Address[] Returns an array of Address objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Address
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
