<?php

namespace App\Repository;

use App\Entity\Address;
use App\Entity\StopArea;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<StopArea>
 *
 * @method StopArea|null find($id, $lockMode = null, $lockVersion = null)
 * @method StopArea|null findOneBy(array $criteria, array $orderBy = null)
 * @method StopArea[]    findAll()
 * @method StopArea[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StopAreaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StopArea::class);
    }

//    /**
//     * @return StopArea[] Returns an array of StopArea objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?StopArea
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
