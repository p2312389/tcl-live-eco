<?php

namespace App\Repository;

use App\Entity\VelovStation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<VelovStation>
 *
 * @method VelovStation|null find($id, $lockMode = null, $lockVersion = null)
 * @method VelovStation|null findOneBy(array $criteria, array $orderBy = null)
 * @method VelovStation[]    findAll()
 * @method VelovStation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VelovStationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VelovStation::class);
    }

//    /**
//     * @return VelovStation[] Returns an array of VelovStation objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('v.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?VelovStation
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
