<?php

namespace App\Controller;

use App\Entity\Transports;
use App\Services\TransportsServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Contrôleur permettant de renvoyer les informations relatives aux transports TCL
 */
#[Route('api/transport')]
class TransportsController extends AbstractController
{
    private TransportsServices $transportsServices;

    public function __construct(TransportsServices $transportsServices)
    {
        $this->transportsServices = $transportsServices;
    }

    #[Route(name: 'getAllTransports', methods: 'GET')]
    public function getAllTransports(): JsonResponse
    {
        return $this->transportsServices->getAllTransports();

    }

    #[Route('/type-transport', name: 'getAllTransportFromTypeTransport', methods: 'GET')]
    public function getAllTransportsFromTypeTransport(): JsonResponse
    {
        return $this->transportsServices->getAllTransportsFromTypeTransport();
    }

    #[Route('/search-transport', name: 'searchTransport', methods: 'GET')]
    public function searchTransport(Request $request): JsonResponse
    {
        $transportName = $request->get('transport-name', '');
        $idTypeTransport = $request->get('idTypeTransport', []);
        return $this->transportsServices->searchTransport($transportName, $idTypeTransport);
    }

    /**
     * Récupère la liste des transports dont le nom de la ligne commence par le mot-clé renseigné.
     *
     * @param Request $request Requête HTTP pour récupérer le paramètre keyword.
     * @return JsonResponse JSON avec la liste des transports dont le nom de la ligne commence par le mot-clé renseingé.
     */
    #[Route('/autocomplete', name: 'transports-autocomplete', methods: 'GET')]
    public function autoCompleteTransportSearch(Request $request): JsonResponse
    {
        $keyword = $request->get('keyword', '');
        return $this->transportsServices->getTransportsFromKeyword($keyword);
    }

    /**
     * Récupère tout le détail d'un transport dont l'id est renseigné.
     *
     * @param Transports $transport Transport correspondant à l'id.
     * @return JsonResponse JSON avec toutes les informations relatives au transport.
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route('/{id}', name: 'detailTransport', methods: 'GET')]
    public function getTransportDetail(Transports $transport): JsonResponse
    {
        return $this->transportsServices->getTransportDetails($transport);
    }
}