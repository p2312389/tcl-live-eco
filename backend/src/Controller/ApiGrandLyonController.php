<?php

namespace App\Controller;

use App\Exception\ApiGrandLyonException;
use App\Services\ConverterAddressService;
use App\Services\ApiGrandLyonService;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Contrôleur permettant de récupérer des informations via l'API du grand lyon.
 */
class ApiGrandLyonController extends AbstractController
{
    private SerializerInterface $serializer;
    private ApiGrandLyonService $apiGrandLyonService;

    public function __construct(SerializerInterface $serializer, ApiGrandLyonService $apiGrandLyonService)
    {
        $this->serializer = $serializer;
        $this->apiGrandLyonService = $apiGrandLyonService;
    }

    /**
     * Récupère tous les horaires en temps réel mis à disposition par la métropole du grand Lyon.
     *
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route('api/next-schedule', name: 'getRealTimeScheduleFromTCL', methods: 'GET')]
    public function getRealTimeScheduleFromTCL(): JsonResponse
    {
        $response = $this->apiGrandLyonService->getScheduleFromGrandLyon();

        return new jsonResponse(
            $this->serializer->serialize($response->toArray(), 'json'),
            $response->getStatusCode(),
            [],
            true);
    }

    /**
     * Récupère toutes les perturbations connues sur le réseau TCL.
     *
     * @return JsonResponse JSON avec les perturbations connues sur le réseau TCL.
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ApiGrandLyonException Si l'appel API au grand Lyon rencontre une erreur alors une exception est renvoyée
     */
    #[Route('api/disturbance', name: 'getDisturbanceFromTCL', methods: 'GET')]
    public function getDisturbance(): JsonResponse
    {
        $disturbanceVOs = $this->apiGrandLyonService->getDisturbanceFromGrandLyon(true);

        $jsonDisturbance = $this->serializer->serialize($disturbanceVOs, 'json');
        return new JsonResponse($jsonDisturbance, Response::HTTP_OK, [], true);
    }

    #[Route('api/availability-velov', name: 'getRealTimeVelovStation', methods: 'GET')]
    public function getAvailabilityVelovStation(): JsonResponse
    {
        $availabilityVelovVOs = $this->apiGrandLyonService->getAvailabilityVelovStation(true);

        $jsonAvailabilityVelov = $this->serializer->serialize($availabilityVelovVOs, 'json');
        return new JsonResponse($jsonAvailabilityVelov, Response::HTTP_OK, [], true);
    }

}