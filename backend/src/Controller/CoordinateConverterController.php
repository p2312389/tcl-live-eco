<?php

namespace App\Controller;

use App\Services\ConverterAddressService;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Contrôleur pour convertir des coordonnées GPS en adresse postale ou inverserment.
 */
#[Route('api/converter')]
class CoordinateConverterController extends AbstractController
{

    private SerializerInterface $serializer;
    private ConverterAddressService $converterAddressService;

    public function __construct(SerializerInterface $serializer, ConverterAddressService $converterAddressService)
    {
        $this->serializer = $serializer;
        $this->converterAddressService = $converterAddressService;
    }

    /**
     * Convertit l'adresse renseignée en paramètre en coordonnée GPS (latitude & longitude).
     *
     * @param Request $request Requête HTTP pour récupérer les paramètres.
     * @return JsonResponse Un objet de type Address contenant les coordonées GPS.
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route('/address-to-coordinate', name: 'app-address-to-coordinate', methods: 'GET')]
    public function convertAddressToCoordinate(Request $request): JsonResponse
    {
        $address = $request->get('address', "");
        $city = $request->get('city', "");

        if ($address === "" || $city === "") {
            $error = [
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'L\'adresse et la ville doivent être renseigné afin de les convertir en coordonnées GPS'
            ];

            return new JsonResponse($this->serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);
        }

        return $this->converterAddressService->callConverterAddressToCoordinate($address, $city);
    }

    /**
     * Convertit les coordonnées GPS renseignées en paramètre en adresse postale.
     *
     * @param Request $request Requête HTTP pour récupérer les paramètres.
     * @return JsonResponse Un objet de type Address contenant l'adresse postale.
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route('/coordinate-to-address', name: 'app-coordinate-to-address', methods: 'GET')]
    public function convertCoordinateToAddress(Request $request): JsonResponse
    {
        $latitude = $request->get('lat', "");
        $longitude = $request->get('lon', "");

        if ($latitude === "" || $longitude === "") {
            $error = [
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'L\'adresse et la ville doivent être renseigné afin de les convertir en coordonnées GPS'
            ];

            return new JsonResponse($this->serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);
        }

        return $this->converterAddressService->convertCoordinateToAddress($latitude, $longitude);
    }
}
