<?php

namespace App\Controller;

use App\Services\StopAreaServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Routes;
use App\Entity\StopArea;
use App\Entity\Transports;


#[Route('api')]
class StopAreaController extends AbstractController
{
    #[Route('/stop-area-proches', name: 'stopAreaProches', methods: 'GET')]
    public function getStopAreaProches(Request $request, StopAreaServices $stopAreaServices, SerializerInterface $serializer): JsonResponse
    {

        $streetRequest = $request->get('street', '');
        $municipalityRequest = $request->get('municipality', '');
        $zipCodeRequest = $request->get('zipcode', '');
        $distanceRequest = $request->get('dist', '');
        $idTypeTransportsRequest = $request->get('idTransport', []);

        if ($streetRequest == '' || $municipalityRequest == '' || $distanceRequest == '') {
            $error = [
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'Les paramètres street, municipality et dist doivent être renseignées. '
                    . 'Street : ' . $streetRequest
                    . ', municipality : ' . $municipalityRequest
                    . ', distance : ' . $distanceRequest
            ];
            return new JsonResponse($serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);
        }

        return $stopAreaServices->getStopAreaProches($streetRequest, $municipalityRequest, $zipCodeRequest, $distanceRequest, $idTypeTransportsRequest);
    }

    #[Route('/detail-station', name: 'getDetailStopArea2', methods: 'GET')]
    public function getDetailStopArea2(Request $request, StopAreaServices $stopAreaServices): JsonResponse
    {
        $idStopArea = $request->get('id-stop-area', 0);
        $idTransport = $request->get('id-transport', 0);
        $idRoutes = $request->get('id-routes', 0);
        return $stopAreaServices->getDetailStopArea($idStopArea, $idTransport, $idRoutes);
    }

}