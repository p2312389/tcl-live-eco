<?php

namespace App\Controller;

use App\Repository\StopAreaRepository;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Contrôleur pour récupérer des informations relatives aux stations TCL
 */
#[Route('api')]
class StationsController extends AbstractController
{

    private StopAreaRepository $stopAreaRepository;
    private SerializerInterface $serializer;

    public function __construct(StopAreaRepository $stopAreaRepository, SerializerInterface $serializer)
    {
        $this->stopAreaRepository = $stopAreaRepository;
        $this->serializer = $serializer;
    }

    /**
     * Récupère toutes les stations en base possédant des adresses
     *
     * @return JsonResponse La liste des stations avec leur adresse.
     */
    #[Route('/stations', name:'stations-with-address', methods: 'GET')]
    public function getAllSationsWithAddress(): JsonResponse
    {
        $stationsWithAddress = $this->stopAreaRepository->findAll();

        $context = SerializationContext::create()->setGroups(['read:StopArea:stationWithAddress']);
        $jsonStations = $this->serializer->serialize($stationsWithAddress, 'json', $context);
        return new JsonResponse($jsonStations, Response::HTTP_OK, [], true);
    }
}