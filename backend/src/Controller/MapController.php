<?php

namespace App\Controller;

use App\Models\Location;
use App\Services\MapServices;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Contrôleur pour récupérer des informations pour la carte de la page d'accueil
 */
#[Route('api/map')]
class MapController extends AbstractController
{
    private MapServices $mapServices;

    public function __construct(MapServices $mapServices)
    {
        $this->mapServices = $mapServices;
    }

    /**
     * Récupère toutes les stations en fonction des coordonnées GPS.
     *
     * @return JsonResponse La liste des stations avec leur adresse.
     */
    #[Route('/get-distance-between-coordinate', name:'getDistanceBetweenTwoCoordinatePoint', methods: 'GET')]
    public function getAllSationsWithAddress(): JsonResponse
    {

        $fromLocation = new Location(45.756697, 4.585222);
        $toLocation = new Location(45.757097, 4.856011);
        $result = $this->mapServices->getDistanceBetweenGPSCoordinate($fromLocation, $toLocation);

        return new JsonResponse($result, Response::HTTP_OK, [], false);
    }
}