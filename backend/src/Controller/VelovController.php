<?php

namespace App\Controller;

use App\Services\VelovSationService;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Contrôleur relatif aux stations de velo'v.
 */
#[Route('/api')]
class VelovController extends AbstractController
{

    private SerializerInterface $serializer;
    private VelovSationService $velovSationService;
    public function __construct(SerializerInterface $serializer, VelovSationService $velovSationService)
    {
        $this->serializer = $serializer;
        $this->velovSationService = $velovSationService;
    }

    /**
     * Récupère les stations de vélo'v à proximité de l'adresse renseignée.
     *
     * @param Request $request Requete HTTP pour récupérer les paramètres.
     * @return JsonResponse Les stations de vélo'v à proximité de l'adresse renseignée.
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route('/near-velov-station', name: 'getVelovStationNearPostion', methods: 'GET')]
    public function getVelovStationNearToPosition(Request $request): JsonResponse
    {
        $streetRequest = $request->get('street', '');
        $municipalityRequest = $request->get('municipality', '');
        $zipCodeRequest = $request->get('zipcode', '');
        $distanceRequest = $request->get('dist', '');

        if ($streetRequest == '' || $municipalityRequest == '' || $distanceRequest == '') {
            $error = [
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'Les paramètres street, municipality et dist doivent être renseignées. '
                    . 'Street : ' . $streetRequest
                    . ', municipality : ' . $municipalityRequest
                    . ', distance : ' . $distanceRequest
            ];
            return new JsonResponse($this->serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);
        }

        return $this->velovSationService->getStationVelovNearToPostion($streetRequest, $zipCodeRequest, $municipalityRequest, $distanceRequest);
    }
}
