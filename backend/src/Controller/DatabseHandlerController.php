<?php

namespace App\Controller;

use App\Entity\TypeTransports;
use App\Repository\TypeTransportsRepository;
use App\Services\DatabaseHandlerServices;
use App\Services\FileManagerServices;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[Route('api/database-handler')]
class DatabseHandlerController extends AbstractController
{
    private string $directory = "/filesForDatabase";
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;
    private TypeTransportsRepository $typeTransportRepository;
    private DatabaseHandlerServices $databaseHandlerServices;
    private FileManagerServices $fileManagerServices;

    public function __construct(EntityManagerInterface   $entityManager,
                                SerializerInterface      $serializer,
                                TypeTransportsRepository $typeTransportRepository,
                                DatabaseHandlerServices  $databaseHandlerServices,
                                FileManagerServices      $fileManagerServices)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
        $this->typeTransportRepository = $typeTransportRepository;
        $this->databaseHandlerServices = $databaseHandlerServices;
        $this->fileManagerServices = $fileManagerServices;
    }

    #[Route('/get-files', name: 'app-get-files', methods: 'GET')]
    public function getListOfFile()
    {
        return $this->fileManagerServices->getFileInformation($this->getFilesDirectory());
    }

    #[Route('/create-type-transports', name: 'app-create-type-transports', methods: 'GET')]
    public function uploadTypeTransportsDataToDatabase(): JsonResponse
    {
        //test si type_transports est deja dans BD
        //si pas présent -> ajoute
        $resultTypeMetro = $this->typeTransportRepository->findOneBy(['label' => 'Metro']);
        if (is_Null($resultTypeMetro)) {
            $objetTypeTransport = new TypeTransports();
            $objetTypeTransport->setLabel("Metro");
            $this->entityManager->persist($objetTypeTransport); //insertion
        }

        $resultTypeBus = $this->typeTransportRepository->findOneBy(['label' => 'Bus']);
        if (is_Null($resultTypeBus)) {
            $objetTypeTransport = new TypeTransports();
            $objetTypeTransport->setLabel("Bus");
            $this->entityManager->persist($objetTypeTransport); //insertion
        }

        $resultTypeTram = $this->typeTransportRepository->findOneBy(['label' => 'Tram']);
        if (is_Null($resultTypeTram)) {
            $objetTypeTransport = new TypeTransports();
            $objetTypeTransport->setLabel("Tram");
            $this->entityManager->persist($objetTypeTransport); //insertion
        }

        $this->entityManager->flush();

        $arrayToSend["message"] = "insertion type transports ok";

        return new JsonResponse(json_encode($arrayToSend), Response::HTTP_CREATED, [], true);
    }

    #[Route('/create-bus', name: 'app-create-bus', methods: 'GET')]
    public function uploadBusDataToDatabase(Request $request): JsonResponse
    {
        $startIndex = intval($request->get('start-index', -1));
        $endIndex = intval($request->get('end-index', -1));

        $jsonResponseReturnToCheckIndex = $this->handleBadIndex($startIndex, $endIndex, $this->serializer);
        if ($jsonResponseReturnToCheckIndex->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseReturnToCheckIndex;

        $filePath = $this->getFilesDirectory() . "/transports-bus.csv";
        return $this->databaseHandlerServices->transportHandler("Bus", $filePath, $startIndex, $endIndex);
    }

    #[Route('/create-metro', name: 'app-create-metro', methods: 'GET')]
    public function uploadMetroDataToDatabase(Request $request): JsonResponse
    {
        $startIndex = intval($request->get('start-index', -1));
        $endIndex = intval($request->get('end-index', -1));

        $jsonResponseReturnToCheckIndex = $this->handleBadIndex($startIndex, $endIndex, $this->serializer);
        if ($jsonResponseReturnToCheckIndex->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseReturnToCheckIndex;

        $filePath = $this->getFilesDirectory() . "/transports-metro.csv";
        return $this->databaseHandlerServices->transportHandler("Metro", $filePath, $startIndex, $endIndex);
    }

    #[Route('/create-tramway', name: 'app-create-tramway', methods: 'GET')]
    public function uploadTramwayDataToDatabase(Request $request): JsonResponse
    {
        $startIndex = intval($request->get('start-index', -1));
        $endIndex = intval($request->get('end-index', -1));

        $jsonResponseReturnToCheckIndex = $this->handleBadIndex($startIndex, $endIndex, $this->serializer);
        if ($jsonResponseReturnToCheckIndex->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseReturnToCheckIndex;

        $filePath = $this->getFilesDirectory() . "/transports-tramway.csv";
        return $this->databaseHandlerServices->transportHandler("Tram", $filePath, $startIndex, $endIndex);
    }

    #[Route('/create-stop-area', name: 'app-create-stop-area', methods: 'GET')]
    public function uploadStopAreaDataToDatabase(Request $request): JsonResponse
    {
        $startIndex = intval($request->get('start-index', -1));
        $endIndex = intval($request->get('end-index', -1));

        $jsonResponseReturnToCheckIndex = $this->handleBadIndex($startIndex, $endIndex, $this->serializer);
        if ($jsonResponseReturnToCheckIndex->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseReturnToCheckIndex;

        $filePath = $this->getFilesDirectory() . "/stop-area.csv";
        return $this->databaseHandlerServices->stopAreaHandler($filePath, $startIndex, $endIndex);
    }

    private function handleBadIndex(int $startIndex, int $endIndex): JsonResponse
    {

        if ($startIndex <= 0 || $endIndex === -1 || $startIndex > $endIndex) {
            $error = [
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => "Les balises de parcours de début et de fin sont incorrectes. " .
                    "Balise début renvoyée : " . $startIndex . " " .
                    "balise fin renvoyée : " . $endIndex
            ];

            return new JsonResponse($this->serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);
        }

        return new JsonResponse($this->serializer->serialize([], 'json'), Response::HTTP_OK, [], true);
    }

    private function getFilesDirectory(): string
    {
        return $this->getParameter('kernel.project_dir') . $this->directory . "/";
    }

    // Timetable
    #[Route('/create-timetable', name: 'app-create-timetable', methods: 'GET')]
    public function uploadTimetable(Request $request): JsonResponse
    {
        $fileName = $request->get('file-name', '');
        $jsonResponseReturnToCheckFileName = $this->handleBadFileName($fileName, $this->serializer);
        if ($jsonResponseReturnToCheckFileName->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseReturnToCheckFileName;

        // On envoie juste le nom du fichier sans l'extension comme ca je suis sur de l'extension du fichier
        // Ca limite les erreurs
        $filePath = $this->getFilesDirectory() . $fileName . ".xml";
        return $this->databaseHandlerServices->timetableHandler($filePath);
    }

    // StopArea & address
    #[Route('/create-stop-area-address-xml', name: 'app-create-stop-area-address-xml', methods: 'GET')]
    public function uploadTStopAreaAndAddressXml(Request $request): JsonResponse
    {
        $fileName = $request->get('file-name', '');
        $jsonResponseReturnToCheckFileName = $this->handleBadFileName($fileName, $this->serializer);
        if ($jsonResponseReturnToCheckFileName->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseReturnToCheckFileName;

        $filePath = $this->getFilesDirectory() . $fileName . ".xml";
        return $this->databaseHandlerServices->stopAreaAddressFromXmlHandler($filePath);
    }

    // Routes
    #[Route('/create-routes', name: 'app-create-routes', methods: 'GET')]
    public function uploadRoutes(Request $request): JsonResponse
    {
        $fileName = $request->get('file-name', '');
        $jsonResponseReturnToCheckFileName = $this->handleBadFileName($fileName, $this->serializer);
        if ($jsonResponseReturnToCheckFileName->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseReturnToCheckFileName;

        $filePath = $this->getFilesDirectory() . $fileName . ".xml";
        return $this->databaseHandlerServices->routesHandler($filePath);
    }

    // RouteDetail
    #[Route('/create-route-detail', name: 'app-create-route-detail', methods: 'GET')]
    public function uploadRouteDetail(Request $request): JsonResponse
    {
        $fileName = $request->get('file-name', '');
        $jsonResponseReturnToCheckFileName = $this->handleBadFileName($fileName, $this->serializer);
        if ($jsonResponseReturnToCheckFileName->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseReturnToCheckFileName;

        $filePath = $this->getFilesDirectory() . $fileName . ".xml";
        return $this->databaseHandlerServices->routeDetailHandler($filePath);
    }

    // Schedule
    #[Route('/create-schedule', name: 'app-create-schedule', methods: 'GET')]
    public function uploadSchedule(Request $request): JsonResponse
    {
        $startIndex = intval($request->get('start-index', -1));
        $endIndex = intval($request->get('end-index', -1));

        $jsonResponseReturnToCheckIndex = $this->handleBadIndex($startIndex, $endIndex, $this->serializer);
        if ($jsonResponseReturnToCheckIndex->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseReturnToCheckIndex;

        if (($endIndex - $startIndex) > 1600) {
            $error = [
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => "Le pas est trop haut (balise de fin - balise début), il ne doit pas dépasser :  1600."
            ];

            return new JsonResponse($this->serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);
        }

        $fileName = $request->get('file-name', '');
        $jsonResponseReturnToCheckFileName = $this->handleBadFileName($fileName, $this->serializer);
        if ($jsonResponseReturnToCheckFileName->getStatusCode() === Response::HTTP_BAD_REQUEST) return $jsonResponseReturnToCheckFileName;

        $filePath = $this->getFilesDirectory() . $fileName . ".xml";
        return $this->databaseHandlerServices->scheduleHandler($filePath, $startIndex, $endIndex);
    }

    #[Route('/create-velov', name: 'createVelov', methods: 'GET')]
    public function createVelov(): JsonResponse
    {
        $filePath = $this->getFilesDirectory() . "velov.json";
        return $this->databaseHandlerServices->insertVelovStationInDatabase($filePath);
    }

    private function handleBadFileName(string $fileName): JsonResponse
    {

        if ($fileName === "") {
            $error = [
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => "Le nom de fichier : '" . $fileName . "' est incorrect."
            ];

            return new JsonResponse($this->serializer->serialize($error, 'json'), Response::HTTP_BAD_REQUEST, [], true);
        }

        return new JsonResponse($this->serializer->serialize([], 'json'), Response::HTTP_OK, [], true);
    }
}