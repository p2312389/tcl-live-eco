<?php

namespace App\Controller;

use App\Services\TypeTransportServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Contrôleur permettant de renvoyer les informations relatives aux Type de transport.
 */
#[Route('api')]
class TypeTransportController extends AbstractController
{
    private TypeTransportServices $typeTransportServices;

    public function __construct(TypeTransportServices $typeTransportServices)
    {
        $this->typeTransportServices = $typeTransportServices;
    }

    /**
     * Récupère tous les types de transport.
     *
     * @return JsonResponse JSON contenant tous les types de transport.
     */
    #[Route('/type-transport', name: 'getTypeTransports', methods: 'GET')]
    public function getTypeTransports(): JsonResponse
    {
        return $this->typeTransportServices->getTypeTransport();
    }
}