<?php

namespace App\Controller;

use App\Services\AddressService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Contrôleur pour transmettre les informations relatives aux adresses
 */
#[Route('api/address')]
class AddressController extends AbstractController
{

    private AddressService $addressService;

    public function __construct(AddressService $addressService)
    {
        $this->addressService = $addressService;
    }

    /**
     * Récupère les adresses qui ont une correspondance avec la chaine de caractère récupéré en paramètre
     *
     * @param Request $request Requète HTTP pour récupérer le paramètre keyword.
     * @return Response La liste des adresses correpondant à la chaine passée en paramètre
     */
    #[Route('/autocomplete-street', name: 'getAutocompleteAddress', methods: 'GET')]
    public function autocompleteStreetSearch(Request $request): Response
    {
        $keyword = $request->get('keyword', '');
        return $this->addressService->getStreetFromKeyword($keyword);
    }

    /**
     * @param Request $request Requète HTTP pour récupérer le paramètre keyword.
     * @return Response La liste des code postaux correpondant à la chaine passée en paramètre
     */
    #[Route('/autocomplete-zipcode', name: 'getAutocompleteZipcode', methods: 'GET')]
    public function autocompleteZipcodeSearch(Request $request): Response
    {
        $keyword = $request->get('keyword', '');
        return $this->addressService->getZipcodeFromKeyword($keyword);
    }

    /**
     * Récupère les villes qui ont une correspondance avec la chaine de caractère récupéré en paramètre
     *
     * @param Request $request Requète HTTP pour récupérer le paramètre keyword.
     * @return JsonResponse La liste des villes correpondant à la chaine passée en paramètre
     */
    #[Route('/autocomplete-city', name: 'getAutocompleteCity', methods: 'GET')]
    public function autocompleteCitySearch(Request $request): JsonResponse
    {
        $keyword = $request->get('keyword', '');
        return $this->addressService->getCityFromKeyword($keyword);
    }
}
