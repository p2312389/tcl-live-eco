```mermaid

%% extension VD code pour la visualisation : Markdown Preview Mermaid Support

gantt
    title        TCL Live Eco+
    dateFormat   YYYY-MM-DD
    tickInterval 1week
    weekday      monday
    axisFormat   %W

    section Préparation
        Brainstorming                      : p1,  2023-09-04, 2w
        Cahier des charges                 : p2,  2023-09-04, 2w
        Apprentissage frameworks           : p3,  2023-09-11, 3w
        Préparation de l'oral              : p4,  after b11,  1w
        Rapport de projet                  : p5,  after b11,  1w

    section Frontend
        Paramétrage projet                 : f1,  after p3,   1d
        Maquette du site                   : f2,  after f1,   6d
        Version statique                   : f3,  after f2,   5w
        Version dynamique                  : f4,  after f3,   8w
        Amélioration graphique             : f5,  after f3,   8w
        Documentation                      : f6,  after f4,   1w

    section Backend 
        Paramétrage projet                 : b1,  after p3,   1d
        Conversion adresses en coordonnées : b2,  after f2,   1w
        Fonctions de peuplement            : b3,  after d3,   1w
        Informations sur les transports    : b4,  after b3,   2w
        Filtres de recherche de transports : b5,  after b4,   1w
        Recherche géographique des stations: b6,  after b5,   1w
        Horaires en temps réel             : b7,  after b6,   1w
        Détails des stations               : b8,  after b7,   3w
        Détails des transports             : b9,  after b7,   3w
        Velo'v                             : b10, after b9,   2w
        Perturbations du réseau            : b11, after b9,   2w
        Documentation                      : b12, after b10,  1w

    section Database
        Inventaire des données             : d1,  after p3,   6d
        Modélisation BDD                   : d2,  after d1,   1d
        création BDD                       : d3,  after d2,   2w
        peuplement BDD                     : d4,  after d3,   1w
        automatisation peuplement          : d5,  after d4,   2w
```